package br.ufes.nemo.seon.generator.ontologymodel.to.django.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalOM2DLexer extends Lexer {
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int RULE_ID=4;
    public static final int RULE_DATATYPE=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=8;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_BOOLEAN_VALUE=6;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators

    public InternalOM2DLexer() {;} 
    public InternalOM2DLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalOM2DLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalOM2D.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:11:7: ( 'Get' )
            // InternalOM2D.g:11:9: 'Get'
            {
            match("Get"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:12:7: ( 'Post' )
            // InternalOM2D.g:12:9: 'Post'
            {
            match("Post"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:13:7: ( 'Delete' )
            // InternalOM2D.g:13:9: 'Delete'
            {
            match("Delete"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:14:7: ( 'Put' )
            // InternalOM2D.g:14:9: 'Put'
            {
            match("Put"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:15:7: ( 'Configuration' )
            // InternalOM2D.g:15:9: 'Configuration'
            {
            match("Configuration"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:16:7: ( '{' )
            // InternalOM2D.g:16:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:17:7: ( '}' )
            // InternalOM2D.g:17:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:18:7: ( 'author:' )
            // InternalOM2D.g:18:9: 'author:'
            {
            match("author:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:19:7: ( 'author_email:' )
            // InternalOM2D.g:19:9: 'author_email:'
            {
            match("author_email:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:20:7: ( 'repository:' )
            // InternalOM2D.g:20:9: 'repository:'
            {
            match("repository:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:21:7: ( 'software:' )
            // InternalOM2D.g:21:9: 'software:'
            {
            match("software:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:22:7: ( 'about:' )
            // InternalOM2D.g:22:9: 'about:'
            {
            match("about:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:23:7: ( '#' )
            // InternalOM2D.g:23:9: '#'
            {
            match('#'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:24:7: ( 'module' )
            // InternalOM2D.g:24:9: 'module'
            {
            match("module"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:25:7: ( 'imported_name' )
            // InternalOM2D.g:25:9: 'imported_name'
            {
            match("imported_name"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:26:7: ( ':' )
            // InternalOM2D.g:26:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:27:7: ( 'actor' )
            // InternalOM2D.g:27:9: 'actor'
            {
            match("actor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:28:7: ( 'extends' )
            // InternalOM2D.g:28:9: 'extends'
            {
            match("extends"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:29:7: ( 'usecase' )
            // InternalOM2D.g:29:9: 'usecase'
            {
            match("usecase"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:30:7: ( 'fullName' )
            // InternalOM2D.g:30:9: 'fullName'
            {
            match("fullName"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:31:7: ( 'manage' )
            // InternalOM2D.g:31:9: 'manage'
            {
            match("manage"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:32:7: ( 'perfomed' )
            // InternalOM2D.g:32:9: 'perfomed'
            {
            match("perfomed"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:33:7: ( 'by' )
            // InternalOM2D.g:33:9: 'by'
            {
            match("by"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:34:7: ( 'model' )
            // InternalOM2D.g:34:9: 'model'
            {
            match("model"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:35:7: ( '.' )
            // InternalOM2D.g:35:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:36:7: ( 'import' )
            // InternalOM2D.g:36:9: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:37:7: ( '.*' )
            // InternalOM2D.g:37:9: '.*'
            {
            match(".*"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:38:7: ( 'enum' )
            // InternalOM2D.g:38:9: 'enum'
            {
            match("enum"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:39:7: ( ',' )
            // InternalOM2D.g:39:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:40:7: ( 'dao' )
            // InternalOM2D.g:40:9: 'dao'
            {
            match("dao"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:41:7: ( 'entity' )
            // InternalOM2D.g:41:9: 'entity'
            {
            match("entity"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:42:7: ( 'webservice' )
            // InternalOM2D.g:42:9: 'webservice'
            {
            match("webservice"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:43:7: ( 'service' )
            // InternalOM2D.g:43:9: 'service'
            {
            match("service"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:44:7: ( '(' )
            // InternalOM2D.g:44:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:45:7: ( ')' )
            // InternalOM2D.g:45:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:46:7: ( 'NotRequired' )
            // InternalOM2D.g:46:9: 'NotRequired'
            {
            match("NotRequired"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:47:7: ( 'uses' )
            // InternalOM2D.g:47:9: 'uses'
            {
            match("uses"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:48:7: ( 'OneToOne' )
            // InternalOM2D.g:48:9: 'OneToOne'
            {
            match("OneToOne"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:49:7: ( 'ManyToMany' )
            // InternalOM2D.g:49:9: 'ManyToMany'
            {
            match("ManyToMany"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:50:7: ( 'OneToMany' )
            // InternalOM2D.g:50:9: 'OneToMany'
            {
            match("OneToMany"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:51:7: ( 'ManyToOne' )
            // InternalOM2D.g:51:9: 'ManyToOne'
            {
            match("ManyToOne"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "RULE_BOOLEAN_VALUE"
    public final void mRULE_BOOLEAN_VALUE() throws RecognitionException {
        try {
            int _type = RULE_BOOLEAN_VALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6046:20: ( ( 'true' | 'false' ) )
            // InternalOM2D.g:6046:22: ( 'true' | 'false' )
            {
            // InternalOM2D.g:6046:22: ( 'true' | 'false' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='t') ) {
                alt1=1;
            }
            else if ( (LA1_0=='f') ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalOM2D.g:6046:23: 'true'
                    {
                    match("true"); 


                    }
                    break;
                case 2 :
                    // InternalOM2D.g:6046:30: 'false'
                    {
                    match("false"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_BOOLEAN_VALUE"

    // $ANTLR start "RULE_DATATYPE"
    public final void mRULE_DATATYPE() throws RecognitionException {
        try {
            int _type = RULE_DATATYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6048:15: ( ( 'string' | 'integer' | 'decimal' | 'datetime' | 'date' | 'boolean' | 'char' | 'file' | 'cpf' | 'cnpj' | 'celular' | 'telefone' | 'url' | 'email' | 'user' | 'uuid' ) )
            // InternalOM2D.g:6048:17: ( 'string' | 'integer' | 'decimal' | 'datetime' | 'date' | 'boolean' | 'char' | 'file' | 'cpf' | 'cnpj' | 'celular' | 'telefone' | 'url' | 'email' | 'user' | 'uuid' )
            {
            // InternalOM2D.g:6048:17: ( 'string' | 'integer' | 'decimal' | 'datetime' | 'date' | 'boolean' | 'char' | 'file' | 'cpf' | 'cnpj' | 'celular' | 'telefone' | 'url' | 'email' | 'user' | 'uuid' )
            int alt2=16;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // InternalOM2D.g:6048:18: 'string'
                    {
                    match("string"); 


                    }
                    break;
                case 2 :
                    // InternalOM2D.g:6048:27: 'integer'
                    {
                    match("integer"); 


                    }
                    break;
                case 3 :
                    // InternalOM2D.g:6048:37: 'decimal'
                    {
                    match("decimal"); 


                    }
                    break;
                case 4 :
                    // InternalOM2D.g:6048:47: 'datetime'
                    {
                    match("datetime"); 


                    }
                    break;
                case 5 :
                    // InternalOM2D.g:6048:58: 'date'
                    {
                    match("date"); 


                    }
                    break;
                case 6 :
                    // InternalOM2D.g:6048:65: 'boolean'
                    {
                    match("boolean"); 


                    }
                    break;
                case 7 :
                    // InternalOM2D.g:6048:75: 'char'
                    {
                    match("char"); 


                    }
                    break;
                case 8 :
                    // InternalOM2D.g:6048:82: 'file'
                    {
                    match("file"); 


                    }
                    break;
                case 9 :
                    // InternalOM2D.g:6048:89: 'cpf'
                    {
                    match("cpf"); 


                    }
                    break;
                case 10 :
                    // InternalOM2D.g:6048:95: 'cnpj'
                    {
                    match("cnpj"); 


                    }
                    break;
                case 11 :
                    // InternalOM2D.g:6048:102: 'celular'
                    {
                    match("celular"); 


                    }
                    break;
                case 12 :
                    // InternalOM2D.g:6048:112: 'telefone'
                    {
                    match("telefone"); 


                    }
                    break;
                case 13 :
                    // InternalOM2D.g:6048:123: 'url'
                    {
                    match("url"); 


                    }
                    break;
                case 14 :
                    // InternalOM2D.g:6048:129: 'email'
                    {
                    match("email"); 


                    }
                    break;
                case 15 :
                    // InternalOM2D.g:6048:137: 'user'
                    {
                    match("user"); 


                    }
                    break;
                case 16 :
                    // InternalOM2D.g:6048:144: 'uuid'
                    {
                    match("uuid"); 


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DATATYPE"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6050:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalOM2D.g:6050:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalOM2D.g:6050:11: ( '^' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='^') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalOM2D.g:6050:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalOM2D.g:6050:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')||(LA4_0>='A' && LA4_0<='Z')||LA4_0=='_'||(LA4_0>='a' && LA4_0<='z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalOM2D.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6052:10: ( ( '0' .. '9' )+ )
            // InternalOM2D.g:6052:12: ( '0' .. '9' )+
            {
            // InternalOM2D.g:6052:12: ( '0' .. '9' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalOM2D.g:6052:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6054:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalOM2D.g:6054:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalOM2D.g:6054:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\"') ) {
                alt8=1;
            }
            else if ( (LA8_0=='\'') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalOM2D.g:6054:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalOM2D.g:6054:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='!')||(LA6_0>='#' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalOM2D.g:6054:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOM2D.g:6054:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalOM2D.g:6054:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalOM2D.g:6054:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0=='\\') ) {
                            alt7=1;
                        }
                        else if ( ((LA7_0>='\u0000' && LA7_0<='&')||(LA7_0>='(' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='\uFFFF')) ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalOM2D.g:6054:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOM2D.g:6054:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6056:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalOM2D.g:6056:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalOM2D.g:6056:24: ( options {greedy=false; } : . )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='*') ) {
                    int LA9_1 = input.LA(2);

                    if ( (LA9_1=='/') ) {
                        alt9=2;
                    }
                    else if ( ((LA9_1>='\u0000' && LA9_1<='.')||(LA9_1>='0' && LA9_1<='\uFFFF')) ) {
                        alt9=1;
                    }


                }
                else if ( ((LA9_0>='\u0000' && LA9_0<=')')||(LA9_0>='+' && LA9_0<='\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalOM2D.g:6056:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6058:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalOM2D.g:6058:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalOM2D.g:6058:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='\u0000' && LA10_0<='\t')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalOM2D.g:6058:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalOM2D.g:6058:40: ( ( '\\r' )? '\\n' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='\n'||LA12_0=='\r') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalOM2D.g:6058:41: ( '\\r' )? '\\n'
                    {
                    // InternalOM2D.g:6058:41: ( '\\r' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0=='\r') ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // InternalOM2D.g:6058:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6060:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalOM2D.g:6060:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalOM2D.g:6060:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalOM2D.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOM2D.g:6062:16: ( . )
            // InternalOM2D.g:6062:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalOM2D.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | RULE_BOOLEAN_VALUE | RULE_DATATYPE | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt14=50;
        alt14 = dfa14.predict(input);
        switch (alt14) {
            case 1 :
                // InternalOM2D.g:1:10: T__13
                {
                mT__13(); 

                }
                break;
            case 2 :
                // InternalOM2D.g:1:16: T__14
                {
                mT__14(); 

                }
                break;
            case 3 :
                // InternalOM2D.g:1:22: T__15
                {
                mT__15(); 

                }
                break;
            case 4 :
                // InternalOM2D.g:1:28: T__16
                {
                mT__16(); 

                }
                break;
            case 5 :
                // InternalOM2D.g:1:34: T__17
                {
                mT__17(); 

                }
                break;
            case 6 :
                // InternalOM2D.g:1:40: T__18
                {
                mT__18(); 

                }
                break;
            case 7 :
                // InternalOM2D.g:1:46: T__19
                {
                mT__19(); 

                }
                break;
            case 8 :
                // InternalOM2D.g:1:52: T__20
                {
                mT__20(); 

                }
                break;
            case 9 :
                // InternalOM2D.g:1:58: T__21
                {
                mT__21(); 

                }
                break;
            case 10 :
                // InternalOM2D.g:1:64: T__22
                {
                mT__22(); 

                }
                break;
            case 11 :
                // InternalOM2D.g:1:70: T__23
                {
                mT__23(); 

                }
                break;
            case 12 :
                // InternalOM2D.g:1:76: T__24
                {
                mT__24(); 

                }
                break;
            case 13 :
                // InternalOM2D.g:1:82: T__25
                {
                mT__25(); 

                }
                break;
            case 14 :
                // InternalOM2D.g:1:88: T__26
                {
                mT__26(); 

                }
                break;
            case 15 :
                // InternalOM2D.g:1:94: T__27
                {
                mT__27(); 

                }
                break;
            case 16 :
                // InternalOM2D.g:1:100: T__28
                {
                mT__28(); 

                }
                break;
            case 17 :
                // InternalOM2D.g:1:106: T__29
                {
                mT__29(); 

                }
                break;
            case 18 :
                // InternalOM2D.g:1:112: T__30
                {
                mT__30(); 

                }
                break;
            case 19 :
                // InternalOM2D.g:1:118: T__31
                {
                mT__31(); 

                }
                break;
            case 20 :
                // InternalOM2D.g:1:124: T__32
                {
                mT__32(); 

                }
                break;
            case 21 :
                // InternalOM2D.g:1:130: T__33
                {
                mT__33(); 

                }
                break;
            case 22 :
                // InternalOM2D.g:1:136: T__34
                {
                mT__34(); 

                }
                break;
            case 23 :
                // InternalOM2D.g:1:142: T__35
                {
                mT__35(); 

                }
                break;
            case 24 :
                // InternalOM2D.g:1:148: T__36
                {
                mT__36(); 

                }
                break;
            case 25 :
                // InternalOM2D.g:1:154: T__37
                {
                mT__37(); 

                }
                break;
            case 26 :
                // InternalOM2D.g:1:160: T__38
                {
                mT__38(); 

                }
                break;
            case 27 :
                // InternalOM2D.g:1:166: T__39
                {
                mT__39(); 

                }
                break;
            case 28 :
                // InternalOM2D.g:1:172: T__40
                {
                mT__40(); 

                }
                break;
            case 29 :
                // InternalOM2D.g:1:178: T__41
                {
                mT__41(); 

                }
                break;
            case 30 :
                // InternalOM2D.g:1:184: T__42
                {
                mT__42(); 

                }
                break;
            case 31 :
                // InternalOM2D.g:1:190: T__43
                {
                mT__43(); 

                }
                break;
            case 32 :
                // InternalOM2D.g:1:196: T__44
                {
                mT__44(); 

                }
                break;
            case 33 :
                // InternalOM2D.g:1:202: T__45
                {
                mT__45(); 

                }
                break;
            case 34 :
                // InternalOM2D.g:1:208: T__46
                {
                mT__46(); 

                }
                break;
            case 35 :
                // InternalOM2D.g:1:214: T__47
                {
                mT__47(); 

                }
                break;
            case 36 :
                // InternalOM2D.g:1:220: T__48
                {
                mT__48(); 

                }
                break;
            case 37 :
                // InternalOM2D.g:1:226: T__49
                {
                mT__49(); 

                }
                break;
            case 38 :
                // InternalOM2D.g:1:232: T__50
                {
                mT__50(); 

                }
                break;
            case 39 :
                // InternalOM2D.g:1:238: T__51
                {
                mT__51(); 

                }
                break;
            case 40 :
                // InternalOM2D.g:1:244: T__52
                {
                mT__52(); 

                }
                break;
            case 41 :
                // InternalOM2D.g:1:250: T__53
                {
                mT__53(); 

                }
                break;
            case 42 :
                // InternalOM2D.g:1:256: RULE_BOOLEAN_VALUE
                {
                mRULE_BOOLEAN_VALUE(); 

                }
                break;
            case 43 :
                // InternalOM2D.g:1:275: RULE_DATATYPE
                {
                mRULE_DATATYPE(); 

                }
                break;
            case 44 :
                // InternalOM2D.g:1:289: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 45 :
                // InternalOM2D.g:1:297: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 46 :
                // InternalOM2D.g:1:306: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 47 :
                // InternalOM2D.g:1:318: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 48 :
                // InternalOM2D.g:1:334: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 49 :
                // InternalOM2D.g:1:350: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 50 :
                // InternalOM2D.g:1:358: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA2_eotS =
        "\24\uffff\1\26\2\uffff";
    static final String DFA2_eofS =
        "\27\uffff";
    static final String DFA2_minS =
        "\1\142\2\uffff\1\141\1\uffff\1\145\2\uffff\1\162\2\uffff\1\164\7\uffff\1\145\1\164\2\uffff";
    static final String DFA2_maxS =
        "\1\165\2\uffff\1\145\1\uffff\1\160\2\uffff\1\165\2\uffff\1\164\7\uffff\1\145\1\164\2\uffff";
    static final String DFA2_acceptS =
        "\1\uffff\1\1\1\2\1\uffff\1\6\1\uffff\1\10\1\14\1\uffff\1\16\1\3\1\uffff\1\7\1\11\1\12\1\13\1\15\1\17\1\20\2\uffff\1\4\1\5";
    static final String DFA2_specialS =
        "\27\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\4\1\5\1\3\1\11\1\6\2\uffff\1\2\11\uffff\1\1\1\7\1\10",
            "",
            "",
            "\1\13\3\uffff\1\12",
            "",
            "\1\17\2\uffff\1\14\5\uffff\1\16\1\uffff\1\15",
            "",
            "",
            "\1\20\1\21\1\uffff\1\22",
            "",
            "",
            "\1\23",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\24",
            "\1\25",
            "",
            ""
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "6048:17: ( 'string' | 'integer' | 'decimal' | 'datetime' | 'date' | 'boolean' | 'char' | 'file' | 'cpf' | 'cnpj' | 'celular' | 'telefone' | 'url' | 'email' | 'user' | 'uuid' )";
        }
    }
    static final String DFA14_eotS =
        "\1\uffff\4\47\2\uffff\3\47\1\uffff\2\47\1\uffff\5\47\1\110\1\uffff\2\47\2\uffff\5\47\1\45\2\uffff\3\45\2\uffff\1\47\1\uffff\4\47\2\uffff\7\47\1\uffff\4\47\1\uffff\12\47\1\170\1\47\3\uffff\3\47\2\uffff\11\47\5\uffff\1\u0087\1\47\1\u0089\22\47\1\u009f\5\47\1\uffff\1\47\1\u00a6\11\47\1\u009f\2\47\1\uffff\1\u00b2\1\uffff\17\47\1\u00c2\3\47\1\u00c6\1\u009f\1\uffff\1\u009f\2\47\1\u009f\2\47\1\uffff\1\u009f\5\47\1\u00d1\1\47\2\u009f\1\47\1\uffff\4\47\1\u00d8\5\47\1\u00de\4\47\1\uffff\1\47\1\u009f\1\47\1\uffff\1\47\1\u00d1\10\47\1\uffff\2\47\1\u00f1\2\47\2\uffff\3\47\1\u009f\1\u00f8\1\uffff\1\u00f9\1\u00fb\2\47\1\u00fe\15\47\1\uffff\1\47\1\uffff\3\47\1\u0111\2\uffff\1\47\1\uffff\1\u009f\1\u0113\1\uffff\1\u0114\2\47\1\u009f\1\47\1\u009f\7\47\1\u009f\4\47\1\uffff\1\47\2\uffff\1\u0124\1\u0125\1\u009f\2\47\1\u0128\3\47\1\u009f\3\47\1\uffff\1\47\2\uffff\2\47\1\uffff\1\u0132\1\47\1\u0134\4\47\1\u0139\1\47\1\uffff\1\u013b\1\uffff\2\47\1\uffff\1\47\1\uffff\1\u013f\1\uffff\3\47\1\uffff\1\u0143\1\uffff\1\u0144\2\uffff";
    static final String DFA14_eofS =
        "\u0145\uffff";
    static final String DFA14_minS =
        "\1\0\1\145\1\157\1\145\1\157\2\uffff\1\142\2\145\1\uffff\1\141\1\155\1\uffff\1\155\1\162\1\141\1\145\1\157\1\52\1\uffff\1\141\1\145\2\uffff\1\157\1\156\1\141\2\145\1\101\2\uffff\2\0\1\52\2\uffff\1\164\1\uffff\1\163\1\164\1\154\1\156\2\uffff\1\164\1\157\1\164\1\160\1\146\2\162\1\uffff\1\144\1\156\1\160\1\164\1\uffff\2\164\1\141\1\145\1\154\1\151\3\154\1\162\1\60\1\157\3\uffff\1\157\1\143\1\142\2\uffff\1\164\1\145\1\156\1\165\1\154\1\141\1\146\1\160\1\154\5\uffff\1\60\1\164\1\60\1\145\1\146\1\150\1\165\2\157\1\164\1\166\1\151\1\145\1\141\1\157\2\145\1\155\2\151\1\143\1\60\1\144\1\154\1\163\1\145\1\146\1\uffff\1\154\1\60\1\145\1\151\1\163\1\122\1\124\1\171\2\145\1\162\1\60\1\152\1\165\1\uffff\1\60\1\uffff\1\164\1\151\1\157\1\164\1\162\1\163\1\167\1\151\1\156\2\154\1\147\1\162\1\147\1\156\1\60\1\164\1\154\1\141\2\60\1\uffff\1\60\1\116\1\145\1\60\1\157\1\145\1\uffff\1\60\1\155\2\145\1\157\1\124\1\60\1\146\2\60\1\154\1\uffff\1\145\1\147\1\162\1\72\1\60\1\151\1\141\1\143\1\147\1\145\1\60\1\145\1\164\1\145\1\144\1\uffff\1\171\1\60\1\163\1\uffff\1\141\1\60\1\155\1\141\1\151\1\141\1\162\1\161\1\115\1\157\1\uffff\1\157\1\141\1\60\1\165\1\72\2\uffff\1\164\1\162\1\145\2\60\1\uffff\2\60\1\162\1\163\1\60\1\145\1\155\1\145\1\156\1\155\1\154\1\166\1\165\1\156\1\141\1\115\1\156\1\162\1\uffff\1\162\1\uffff\1\145\1\157\1\145\1\60\2\uffff\1\144\1\uffff\2\60\1\uffff\1\60\1\145\1\144\1\60\1\145\1\60\2\151\1\145\1\156\1\141\1\156\1\145\1\60\1\141\1\155\1\162\1\72\1\uffff\1\137\2\uffff\3\60\1\143\1\162\1\60\1\171\1\156\1\145\1\60\1\164\1\141\1\171\1\uffff\1\156\2\uffff\2\145\1\uffff\1\60\1\171\1\60\2\151\1\72\1\141\1\60\1\144\1\uffff\1\60\1\uffff\1\157\1\154\1\uffff\1\155\1\uffff\1\60\1\uffff\1\156\1\72\1\145\1\uffff\1\60\1\uffff\1\60\2\uffff";
    static final String DFA14_maxS =
        "\1\uffff\1\145\1\165\1\145\1\157\2\uffff\1\165\1\145\1\164\1\uffff\1\157\1\156\1\uffff\1\170\2\165\1\145\1\171\1\52\1\uffff\2\145\2\uffff\1\157\1\156\1\141\1\162\1\160\1\172\2\uffff\2\uffff\1\57\2\uffff\1\164\1\uffff\1\163\1\164\1\154\1\156\2\uffff\1\164\1\157\1\164\1\160\1\146\2\162\1\uffff\1\144\1\156\1\160\1\164\1\uffff\1\164\1\165\1\141\1\145\1\154\1\151\3\154\1\162\1\172\1\157\3\uffff\1\164\1\143\1\142\2\uffff\1\164\1\145\1\156\1\165\1\154\1\141\1\146\1\160\1\154\5\uffff\1\172\1\164\1\172\1\145\1\146\1\150\1\165\2\157\1\164\1\166\1\151\1\165\1\141\1\157\2\145\1\155\2\151\1\163\1\172\1\144\1\154\1\163\1\145\1\146\1\uffff\1\154\1\172\1\145\1\151\1\163\1\122\1\124\1\171\2\145\1\162\1\172\1\152\1\165\1\uffff\1\172\1\uffff\1\164\1\151\1\157\1\164\1\162\1\163\1\167\1\151\1\156\2\154\1\147\1\162\1\147\1\156\1\172\1\164\1\154\1\141\2\172\1\uffff\1\172\1\116\1\145\1\172\1\157\1\145\1\uffff\1\172\1\155\2\145\1\157\1\124\1\172\1\146\2\172\1\154\1\uffff\1\145\1\147\1\162\1\72\1\172\1\151\1\141\1\143\1\147\1\145\1\172\1\145\1\164\1\145\1\144\1\uffff\1\171\1\172\1\163\1\uffff\1\141\1\172\1\155\1\141\1\151\1\141\1\162\1\161\1\117\1\157\1\uffff\1\157\1\141\1\172\1\165\1\137\2\uffff\1\164\1\162\1\145\2\172\1\uffff\2\172\1\162\1\163\1\172\1\145\1\155\1\145\1\156\1\155\1\154\1\166\1\165\1\156\1\141\1\117\1\156\1\162\1\uffff\1\162\1\uffff\1\145\1\157\1\145\1\172\2\uffff\1\144\1\uffff\2\172\1\uffff\1\172\1\145\1\144\1\172\1\145\1\172\2\151\1\145\1\156\1\141\1\156\1\145\1\172\1\141\1\155\1\162\1\72\1\uffff\1\137\2\uffff\3\172\1\143\1\162\1\172\1\171\1\156\1\145\1\172\1\164\1\141\1\171\1\uffff\1\156\2\uffff\2\145\1\uffff\1\172\1\171\1\172\2\151\1\72\1\141\1\172\1\144\1\uffff\1\172\1\uffff\1\157\1\154\1\uffff\1\155\1\uffff\1\172\1\uffff\1\156\1\72\1\145\1\uffff\1\172\1\uffff\1\172\2\uffff";
    static final String DFA14_acceptS =
        "\5\uffff\1\6\1\7\3\uffff\1\15\2\uffff\1\20\6\uffff\1\35\2\uffff\1\42\1\43\6\uffff\1\54\1\55\3\uffff\1\61\1\62\1\uffff\1\54\4\uffff\1\6\1\7\7\uffff\1\15\4\uffff\1\20\14\uffff\1\33\1\31\1\35\3\uffff\1\42\1\43\11\uffff\1\55\1\56\1\57\1\60\1\61\33\uffff\1\27\16\uffff\1\1\1\uffff\1\4\25\uffff\1\53\6\uffff\1\36\13\uffff\1\2\17\uffff\1\34\3\uffff\1\45\12\uffff\1\52\5\uffff\1\14\1\21\5\uffff\1\30\22\uffff\1\3\1\uffff\1\10\4\uffff\1\16\1\25\1\uffff\1\32\2\uffff\1\37\22\uffff\1\41\1\uffff\1\22\1\23\15\uffff\1\13\1\uffff\1\24\1\26\2\uffff\1\46\11\uffff\1\50\1\uffff\1\51\2\uffff\1\12\1\uffff\1\40\1\uffff\1\47\3\uffff\1\44\1\uffff\1\11\1\uffff\1\5\1\17";
    static final String DFA14_specialS =
        "\1\1\40\uffff\1\0\1\2\u0122\uffff}>";
    static final String[] DFA14_transitionS = {
            "\11\45\2\44\2\45\1\44\22\45\1\44\1\45\1\41\1\12\3\45\1\42\1\27\1\30\2\45\1\24\1\45\1\23\1\43\12\40\1\15\6\45\2\37\1\4\1\3\2\37\1\1\5\37\1\33\1\31\1\32\1\2\12\37\3\45\1\36\1\37\1\45\1\7\1\22\1\35\1\25\1\16\1\20\2\37\1\14\3\37\1\13\2\37\1\21\1\37\1\10\1\11\1\34\1\17\1\37\1\26\3\37\1\5\1\45\1\6\uff82\45",
            "\1\46",
            "\1\50\5\uffff\1\51",
            "\1\52",
            "\1\53",
            "",
            "",
            "\1\57\1\60\21\uffff\1\56",
            "\1\61",
            "\1\63\11\uffff\1\62\4\uffff\1\64",
            "",
            "\1\67\15\uffff\1\66",
            "\1\70\1\71",
            "",
            "\1\75\1\74\11\uffff\1\73",
            "\1\77\1\76\1\uffff\1\100",
            "\1\102\7\uffff\1\103\13\uffff\1\101",
            "\1\104",
            "\1\106\11\uffff\1\105",
            "\1\107",
            "",
            "\1\112\3\uffff\1\113",
            "\1\114",
            "",
            "",
            "\1\117",
            "\1\120",
            "\1\121",
            "\1\123\14\uffff\1\122",
            "\1\127\2\uffff\1\124\5\uffff\1\126\1\uffff\1\125",
            "\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "",
            "\0\131",
            "\0\131",
            "\1\132\4\uffff\1\133",
            "",
            "",
            "\1\135",
            "",
            "\1\136",
            "\1\137",
            "\1\140",
            "\1\141",
            "",
            "",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "",
            "\1\151",
            "\1\152",
            "\1\153",
            "\1\154",
            "",
            "\1\155",
            "\1\157\1\156",
            "\1\160",
            "\1\161",
            "\1\162",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\167",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\171",
            "",
            "",
            "",
            "\1\172\4\uffff\1\173",
            "\1\174",
            "\1\175",
            "",
            "",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "",
            "",
            "",
            "",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0088",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "\1\u008e",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0094\17\uffff\1\u0093",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c\16\uffff\1\u009e\1\u009d",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "",
            "\1\u00a5",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\u00af",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00b0",
            "\1\u00b1",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u00b3",
            "\1\u00b4",
            "\1\u00b5",
            "\1\u00b6",
            "\1\u00b7",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00c3",
            "\1\u00c4",
            "\1\u00c5",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00c7",
            "\1\u00c8",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00c9",
            "\1\u00ca",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\23\47\1\u00cb\6\47",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "\1\u00cf",
            "\1\u00d0",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00d2",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00d3",
            "",
            "\1\u00d4",
            "\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\1\u00dc",
            "\1\u00dd",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00df",
            "\1\u00e0",
            "\1\u00e1",
            "\1\u00e2",
            "",
            "\1\u00e3",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00e4",
            "",
            "\1\u00e5",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00e6",
            "\1\u00e7",
            "\1\u00e8",
            "\1\u00e9",
            "\1\u00ea",
            "\1\u00eb",
            "\1\u00ed\1\uffff\1\u00ec",
            "\1\u00ee",
            "",
            "\1\u00ef",
            "\1\u00f0",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00f2",
            "\1\u00f3\44\uffff\1\u00f4",
            "",
            "",
            "\1\u00f5",
            "\1\u00f6",
            "\1\u00f7",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\4\47\1\u00fa\25\47",
            "\1\u00fc",
            "\1\u00fd",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00ff",
            "\1\u0100",
            "\1\u0101",
            "\1\u0102",
            "\1\u0103",
            "\1\u0104",
            "\1\u0105",
            "\1\u0106",
            "\1\u0107",
            "\1\u0108",
            "\1\u0109\1\uffff\1\u010a",
            "\1\u010b",
            "\1\u010c",
            "",
            "\1\u010d",
            "",
            "\1\u010e",
            "\1\u010f",
            "\1\u0110",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "",
            "\1\u0112",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0115",
            "\1\u0116",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0117",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0118",
            "\1\u0119",
            "\1\u011a",
            "\1\u011b",
            "\1\u011c",
            "\1\u011d",
            "\1\u011e",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u011f",
            "\1\u0120",
            "\1\u0121",
            "\1\u0122",
            "",
            "\1\u0123",
            "",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0126",
            "\1\u0127",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0129",
            "\1\u012a",
            "\1\u012b",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u012c",
            "\1\u012d",
            "\1\u012e",
            "",
            "\1\u012f",
            "",
            "",
            "\1\u0130",
            "\1\u0131",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0133",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0135",
            "\1\u0136",
            "\1\u0137",
            "\1\u0138",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u013a",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u013c",
            "\1\u013d",
            "",
            "\1\u013e",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u0140",
            "\1\u0141",
            "\1\u0142",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | RULE_BOOLEAN_VALUE | RULE_DATATYPE | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_33 = input.LA(1);

                        s = -1;
                        if ( ((LA14_33>='\u0000' && LA14_33<='\uFFFF')) ) {s = 89;}

                        else s = 37;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA14_0 = input.LA(1);

                        s = -1;
                        if ( (LA14_0=='G') ) {s = 1;}

                        else if ( (LA14_0=='P') ) {s = 2;}

                        else if ( (LA14_0=='D') ) {s = 3;}

                        else if ( (LA14_0=='C') ) {s = 4;}

                        else if ( (LA14_0=='{') ) {s = 5;}

                        else if ( (LA14_0=='}') ) {s = 6;}

                        else if ( (LA14_0=='a') ) {s = 7;}

                        else if ( (LA14_0=='r') ) {s = 8;}

                        else if ( (LA14_0=='s') ) {s = 9;}

                        else if ( (LA14_0=='#') ) {s = 10;}

                        else if ( (LA14_0=='m') ) {s = 11;}

                        else if ( (LA14_0=='i') ) {s = 12;}

                        else if ( (LA14_0==':') ) {s = 13;}

                        else if ( (LA14_0=='e') ) {s = 14;}

                        else if ( (LA14_0=='u') ) {s = 15;}

                        else if ( (LA14_0=='f') ) {s = 16;}

                        else if ( (LA14_0=='p') ) {s = 17;}

                        else if ( (LA14_0=='b') ) {s = 18;}

                        else if ( (LA14_0=='.') ) {s = 19;}

                        else if ( (LA14_0==',') ) {s = 20;}

                        else if ( (LA14_0=='d') ) {s = 21;}

                        else if ( (LA14_0=='w') ) {s = 22;}

                        else if ( (LA14_0=='(') ) {s = 23;}

                        else if ( (LA14_0==')') ) {s = 24;}

                        else if ( (LA14_0=='N') ) {s = 25;}

                        else if ( (LA14_0=='O') ) {s = 26;}

                        else if ( (LA14_0=='M') ) {s = 27;}

                        else if ( (LA14_0=='t') ) {s = 28;}

                        else if ( (LA14_0=='c') ) {s = 29;}

                        else if ( (LA14_0=='^') ) {s = 30;}

                        else if ( ((LA14_0>='A' && LA14_0<='B')||(LA14_0>='E' && LA14_0<='F')||(LA14_0>='H' && LA14_0<='L')||(LA14_0>='Q' && LA14_0<='Z')||LA14_0=='_'||(LA14_0>='g' && LA14_0<='h')||(LA14_0>='j' && LA14_0<='l')||(LA14_0>='n' && LA14_0<='o')||LA14_0=='q'||LA14_0=='v'||(LA14_0>='x' && LA14_0<='z')) ) {s = 31;}

                        else if ( ((LA14_0>='0' && LA14_0<='9')) ) {s = 32;}

                        else if ( (LA14_0=='\"') ) {s = 33;}

                        else if ( (LA14_0=='\'') ) {s = 34;}

                        else if ( (LA14_0=='/') ) {s = 35;}

                        else if ( ((LA14_0>='\t' && LA14_0<='\n')||LA14_0=='\r'||LA14_0==' ') ) {s = 36;}

                        else if ( ((LA14_0>='\u0000' && LA14_0<='\b')||(LA14_0>='\u000B' && LA14_0<='\f')||(LA14_0>='\u000E' && LA14_0<='\u001F')||LA14_0=='!'||(LA14_0>='$' && LA14_0<='&')||(LA14_0>='*' && LA14_0<='+')||LA14_0=='-'||(LA14_0>=';' && LA14_0<='@')||(LA14_0>='[' && LA14_0<=']')||LA14_0=='`'||LA14_0=='|'||(LA14_0>='~' && LA14_0<='\uFFFF')) ) {s = 37;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA14_34 = input.LA(1);

                        s = -1;
                        if ( ((LA14_34>='\u0000' && LA14_34<='\uFFFF')) ) {s = 89;}

                        else s = 37;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AttributeEnum;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Description;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumX;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ManyToMany;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OneToMany;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OneToOne;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Relation;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class DocumentationGenerator extends AbstractGenerator {
  private String PATH_LIB_DOCUMENTATION = "/solution/";

  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;

  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    for (final Configuration c : _filter) {
      fsa.generateFile((this.PATH_LIB_DOCUMENTATION + "README.md"), this.compile(c));
    }
    Iterable<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module> modules = Iterables.<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module.class);
    fsa.generateFile((this.PATH_LIB_DOCUMENTATION + "docs/README.md"), this.compile(modules));
    modules = Iterables.<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module.class);
    fsa.generateFile((this.PATH_LIB_DOCUMENTATION + "docs/packagediagram.puml"), this.createPackageDiagram(modules));
    Iterable<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module> _filter_1 = Iterables.<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module.class);
    for (final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m : _filter_1) {
      {
        String _lowerCase = m.getName().toLowerCase();
        String _plus = ((this.PATH_LIB_DOCUMENTATION + "docs/") + _lowerCase);
        String _plus_1 = (_plus + "/README.md");
        fsa.generateFile(_plus_1, this.compile(m));
        String _lowerCase_1 = m.getName().toLowerCase();
        String _plus_2 = ((this.PATH_LIB_DOCUMENTATION + "docs/") + _lowerCase_1);
        String _plus_3 = (_plus_2 + "/classdiagram.puml");
        fsa.generateFile(_plus_3, 
          this.createClassDiagram(m));
      }
    }
  }

  public CharSequence createPackageDiagram(final Iterable<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module> modules) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@startuml");
    _builder.newLine();
    {
      for(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m : modules) {
        _builder.append("namespace ");
        String _name = m.getName();
        _builder.append(_name);
        _builder.append("{");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("@enduml");
    _builder.newLine();
    return _builder;
  }

  public CharSequence compile(final Iterable<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module> modules) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# Documentation");
    _builder.newLine();
    _builder.append("## Conceptual Model");
    _builder.newLine();
    _builder.append("![Domain Diagram](packagediagram.png)\t");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.newLine();
    _builder.append("## Modules");
    _builder.newLine();
    {
      for(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m : modules) {
        {
          Description _description = m.getDescription();
          boolean _tripleNotEquals = (_description != null);
          if (_tripleNotEquals) {
            _builder.append("* **[");
            String _name = m.getName();
            _builder.append(_name);
            _builder.append("](./");
            String _lowerCase = m.getName().toLowerCase();
            _builder.append(_lowerCase);
            _builder.append("/)** : ");
            String _textfield = m.getDescription().getTextfield();
            _builder.append(_textfield);
            _builder.append(" ");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
          } else {
            _builder.append("* **[");
            String _name_1 = m.getName();
            _builder.append(_name_1);
            _builder.append("](./");
            String _name_2 = m.getName();
            _builder.append(_name_2);
            _builder.append("/)** : - ");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    return _builder;
  }

  public CharSequence createClassDiagram(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m) {
    CharSequence _xblockexpression = null;
    {
      final List<Entity> entities = new ArrayList<Entity>();
      final List<EnumX> enuns = new ArrayList<EnumX>();
      EList<AbstractElement> _elements = m.getElements();
      for (final AbstractElement e : _elements) {
        Class<?> _instanceClass = e.eClass().getInstanceClass();
        boolean _equals = Objects.equal(_instanceClass, Entity.class);
        if (_equals) {
          final Entity entity = ((Entity) e);
          entities.add(entity);
        } else {
          Class<?> _instanceClass_1 = e.eClass().getInstanceClass();
          boolean _equals_1 = Objects.equal(_instanceClass_1, EnumX.class);
          if (_equals_1) {
            final EnumX enumx = ((EnumX) e);
            enuns.add(enumx);
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _createClassDiagram = this.createClassDiagram(entities, m, enuns);
      _builder.append(_createClassDiagram);
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }

  public CharSequence createClassDiagram(final List<Entity> entities, final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m, final List<EnumX> enuns) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@startuml");
    _builder.newLine();
    _builder.newLine();
    {
      int _size = enuns.size();
      boolean _greaterThan = (_size > 0);
      if (_greaterThan) {
        {
          for(final EnumX e : enuns) {
            _builder.append("enum ");
            String _name = e.getName();
            _builder.append(_name);
            _builder.append("{");
            _builder.newLineIfNotEmpty();
            {
              EList<AttributeEnum> _attributes = e.getAttributes();
              for(final AttributeEnum a : _attributes) {
                _builder.append("\t");
                String _name_1 = a.getName();
                _builder.append(_name_1, "\t");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("}");
            _builder.newLine();
          }
        }
        _builder.newLine();
      }
    }
    _builder.newLine();
    {
      int _size_1 = entities.size();
      boolean _greaterThan_1 = (_size_1 > 0);
      if (_greaterThan_1) {
        {
          for(final Entity e_1 : entities) {
            _builder.append("class ");
            String _name_2 = e_1.getName();
            _builder.append(_name_2);
            _builder.append("{");
            _builder.newLineIfNotEmpty();
            {
              EList<Attribute> _attributes_1 = e_1.getAttributes();
              for(final Attribute a_1 : _attributes_1) {
                _builder.append("\t");
                String _type = a_1.getType();
                _builder.append(_type, "\t");
                _builder.append(":");
                String _name_3 = a_1.getName();
                _builder.append(_name_3, "\t");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("\t");
            _builder.newLine();
            {
              EList<Relation> _relations = e_1.getRelations();
              for(final Relation r : _relations) {
                {
                  Class<?> _instanceClass = r.eClass().getInstanceClass();
                  boolean _notEquals = (!Objects.equal(_instanceClass, ManyToMany.class));
                  if (_notEquals) {
                    _builder.append("\t");
                    String _name_4 = r.getType().getName();
                    _builder.append(_name_4, "\t");
                    _builder.append(" : ");
                    String _lowerCase = r.getName().toLowerCase();
                    _builder.append(_lowerCase, "\t");
                    _builder.append(" ");
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
            _builder.append("\t");
            _builder.newLine();
            _builder.append("}");
            _builder.newLine();
            {
              Entity _superType = e_1.getSuperType();
              boolean _tripleNotEquals = (_superType != null);
              if (_tripleNotEquals) {
                String _name_5 = e_1.getSuperType().getName();
                _builder.append(_name_5);
                _builder.append(" <|-- ");
                String _name_6 = e_1.getName();
                _builder.append(_name_6);
              }
            }
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            {
              EList<EnumEntityAtribute> _enumentityatributes = e_1.getEnumentityatributes();
              for(final EnumEntityAtribute a_2 : _enumentityatributes) {
                String _name_7 = e_1.getName();
                _builder.append(_name_7);
                _builder.append(" \"1\" -- \"1\" ");
                String _name_8 = a_2.getType().getName();
                _builder.append(_name_8);
                _builder.append(" : ");
                String _lowerCase_1 = a_2.getName().toLowerCase();
                _builder.append(_lowerCase_1);
                _builder.append(" >");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.newLine();
            {
              EList<Relation> _relations_1 = e_1.getRelations();
              for(final Relation r_1 : _relations_1) {
                {
                  Class<?> _instanceClass_1 = r_1.eClass().getInstanceClass();
                  boolean _equals = Objects.equal(_instanceClass_1, OneToOne.class);
                  if (_equals) {
                    _builder.newLine();
                    {
                      String _lowerCase_2 = this._iQualifiedNameProvider.getFullyQualifiedName(r_1.getType().eContainer()).toString().toLowerCase();
                      String _lowerCase_3 = m.getName().toString().toLowerCase();
                      boolean _equals_1 = Objects.equal(_lowerCase_2, _lowerCase_3);
                      if (_equals_1) {
                        _builder.append("\t");
                        String _name_9 = e_1.getName();
                        _builder.append(_name_9, "\t");
                        _builder.append(" \"1\" -- \"1\" ");
                        String _name_10 = r_1.getType().getName();
                        _builder.append(_name_10, "\t");
                        _builder.append(" : ");
                        String _lowerCase_4 = r_1.getName().toLowerCase();
                        _builder.append(_lowerCase_4, "\t");
                        _builder.append(" >");
                        _builder.newLineIfNotEmpty();
                      } else {
                        _builder.append("\t");
                        String _name_11 = e_1.getName();
                        _builder.append(_name_11, "\t");
                        _builder.append(" \"1\" -- \"1\" ");
                        String _string = this._iQualifiedNameProvider.getFullyQualifiedName(r_1.getType().eContainer()).toString();
                        _builder.append(_string, "\t");
                        _builder.append(".");
                        String _name_12 = r_1.getType().getName();
                        _builder.append(_name_12, "\t");
                        _builder.append(" : ");
                        String _lowerCase_5 = r_1.getName().toLowerCase();
                        _builder.append(_lowerCase_5, "\t");
                        _builder.append(" >");
                        _builder.newLineIfNotEmpty();
                      }
                    }
                  }
                }
                _builder.newLine();
                {
                  Class<?> _instanceClass_2 = r_1.eClass().getInstanceClass();
                  boolean _equals_2 = Objects.equal(_instanceClass_2, ManyToMany.class);
                  if (_equals_2) {
                    {
                      String _lowerCase_6 = this._iQualifiedNameProvider.getFullyQualifiedName(r_1.getType().eContainer()).toString().toLowerCase();
                      String _lowerCase_7 = m.getName().toString().toLowerCase();
                      boolean _equals_3 = Objects.equal(_lowerCase_6, _lowerCase_7);
                      if (_equals_3) {
                        String _name_13 = e_1.getName();
                        _builder.append(_name_13);
                        _builder.append(" \"0..*\" -- \"0..*\" ");
                        String _name_14 = r_1.getType().getName();
                        _builder.append(_name_14);
                        _builder.append(" : ");
                        String _lowerCase_8 = r_1.getName().toLowerCase();
                        _builder.append(_lowerCase_8);
                        _builder.append(" >");
                        _builder.newLineIfNotEmpty();
                      } else {
                        String _name_15 = e_1.getName();
                        _builder.append(_name_15);
                        _builder.append(" \"0..*\" -- \"0..*\" ");
                        String _string_1 = this._iQualifiedNameProvider.getFullyQualifiedName(r_1.getType().eContainer()).toString();
                        _builder.append(_string_1);
                        _builder.append(".");
                        String _name_16 = r_1.getType().getName();
                        _builder.append(_name_16);
                        _builder.append(" : ");
                        String _lowerCase_9 = r_1.getName().toLowerCase();
                        _builder.append(_lowerCase_9);
                        _builder.append(" >");
                        _builder.newLineIfNotEmpty();
                      }
                    }
                    _builder.newLine();
                  }
                }
                _builder.newLine();
                {
                  Class<?> _instanceClass_3 = r_1.eClass().getInstanceClass();
                  boolean _equals_4 = Objects.equal(_instanceClass_3, OneToMany.class);
                  if (_equals_4) {
                    {
                      String _lowerCase_10 = this._iQualifiedNameProvider.getFullyQualifiedName(r_1.getType().eContainer()).toString().toLowerCase();
                      String _lowerCase_11 = m.getName().toString().toLowerCase();
                      boolean _equals_5 = Objects.equal(_lowerCase_10, _lowerCase_11);
                      if (_equals_5) {
                        String _name_17 = e_1.getName();
                        _builder.append(_name_17);
                        _builder.append(" \"1\" -- \"0..*\" ");
                        String _name_18 = r_1.getType().getName();
                        _builder.append(_name_18);
                        _builder.append(" : ");
                        String _lowerCase_12 = r_1.getName().toLowerCase();
                        _builder.append(_lowerCase_12);
                        _builder.append(" >");
                        _builder.newLineIfNotEmpty();
                      } else {
                        String _name_19 = e_1.getName();
                        _builder.append(_name_19);
                        _builder.append(" \"1\" -- \"0..*\" ");
                        String _string_2 = this._iQualifiedNameProvider.getFullyQualifiedName(r_1.getType().eContainer()).toString();
                        _builder.append(_string_2);
                        _builder.append(".");
                        String _name_20 = r_1.getType().getName();
                        _builder.append(_name_20);
                        _builder.append(" : ");
                        String _lowerCase_13 = r_1.getName().toLowerCase();
                        _builder.append(_lowerCase_13);
                        _builder.append(" >");
                        _builder.newLineIfNotEmpty();
                      }
                    }
                  }
                }
              }
            }
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("@enduml");
    _builder.newLine();
    return _builder;
  }

  public CharSequence compile(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# Documentation: ");
    String _name = m.getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    {
      Description _description = m.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        String _textfield = m.getDescription().getTextfield();
        _builder.append(_textfield);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("## Application Conceptual Data Model");
    _builder.newLine();
    _builder.append("![Domain Diagram](classdiagram.png)\t");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.append("## Entities");
    _builder.newLine();
    {
      EList<AbstractElement> _elements = m.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _descriptionX = this.descriptionX(entity);
            _builder.append(_descriptionX);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence descriptionX(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Description _description = e.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        _builder.append("* **");
        String _name = e.getName();
        _builder.append(_name);
        _builder.append("** : ");
        String _textfield = e.getDescription().getTextfield();
        _builder.append(_textfield);
      } else {
        _builder.append("* **");
        String _name_1 = e.getName();
        _builder.append(_name_1);
        _builder.append("** : -");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public CharSequence compile(final Configuration e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# ");
    String _firstUpper = StringExtensions.toFirstUpper(e.getSoftware().getName());
    _builder.append(_firstUpper);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("## General Information");
    _builder.newLine();
    _builder.append("* **Software**:");
    String _name = e.getSoftware().getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    _builder.append("* **Author**:");
    String _name_1 = e.getAuthor().getName();
    _builder.append(_name_1);
    _builder.newLineIfNotEmpty();
    _builder.append("* **Author\'s e-mail**:");
    String _name_2 = e.getAuthor_email().getName();
    _builder.append(_name_2);
    _builder.newLineIfNotEmpty();
    _builder.append("* **Source Repository**: [");
    String _name_3 = e.getRepository().getName();
    _builder.append(_name_3);
    _builder.append("](");
    String _name_4 = e.getRepository().getName();
    _builder.append(_name_4);
    _builder.append(")  ");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("## Goal");
    _builder.newLine();
    String _name_5 = e.getAbout().getName();
    _builder.append(_name_5);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("## Documentation");
    _builder.newLine();
    _builder.newLine();
    _builder.append("The Documentation can be found in this [link](./docs/README.md)");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("## Instalation");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
}

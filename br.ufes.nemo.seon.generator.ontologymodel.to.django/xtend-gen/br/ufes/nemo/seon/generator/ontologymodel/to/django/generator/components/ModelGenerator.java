package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AttributeEnum;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Description;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumX;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ManyToMany;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Relation;
import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.HashSet;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class ModelGenerator {
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;

  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# -*- coding: utf-8 -*-");
    _builder.newLine();
    _builder.append("from django.db import models\t");
    _builder.newLine();
    _builder.append("from django.utils.translation import gettext_lazy as _\t");
    _builder.newLine();
    CharSequence _createImport = this.createImport(module);
    _builder.append(_createImport);
    _builder.append("\t\t");
    _builder.newLineIfNotEmpty();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createModel = this.createModel(entity);
            _builder.append(_createModel);
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.newLine();
        {
          Class<?> _instanceClass_1 = e.eClass().getInstanceClass();
          boolean _equals_1 = Objects.equal(_instanceClass_1, EnumX.class);
          if (_equals_1) {
            final EnumX enumX = ((EnumX) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createEnum = this.createEnum(enumX);
            _builder.append(_createEnum);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createImport(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    CharSequence _xblockexpression = null;
    {
      final Set<String> imports = new HashSet<String>();
      EList<AbstractElement> _elements = module.getElements();
      for (final AbstractElement e : _elements) {
        Class<?> _instanceClass = e.eClass().getInstanceClass();
        boolean _equals = Objects.equal(_instanceClass, Entity.class);
        if (_equals) {
          final Entity entity = ((Entity) e);
          EList<Relation> _relations = entity.getRelations();
          for (final Relation relation : _relations) {
            String _lowerCase = this._iQualifiedNameProvider.getFullyQualifiedName(relation.getType().eContainer()).toString().toLowerCase();
            String _lowerCase_1 = module.getName().toString().toLowerCase();
            boolean _notEquals = (!Objects.equal(_lowerCase, _lowerCase_1));
            if (_notEquals) {
              StringConcatenation _builder = new StringConcatenation();
              _builder.append("from apps.");
              QualifiedName _lowerCase_2 = this._iQualifiedNameProvider.getFullyQualifiedName(relation.getType().eContainer()).toLowerCase();
              _builder.append(_lowerCase_2);
              _builder.append(".models import ");
              String _name = relation.getType().getName();
              _builder.append(_name);
              imports.add(_builder.toString());
            }
          }
          EList<Attribute> _attributes = entity.getAttributes();
          for (final Attribute attribute : _attributes) {
            {
              String _lowerCase_3 = attribute.getType().toLowerCase();
              boolean _equals_1 = Objects.equal(_lowerCase_3, "cpf");
              if (_equals_1) {
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append("from django_cpf_cnpj.fields import CPFField");
                imports.add(_builder_1.toString());
              }
              String _lowerCase_4 = attribute.getType().toLowerCase();
              boolean _equals_2 = Objects.equal(_lowerCase_4, "cnpj");
              if (_equals_2) {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("from django_cpf_cnpj.fields import CNPJField");
                imports.add(_builder_2.toString());
              }
              String _lowerCase_5 = attribute.getType().toLowerCase();
              boolean _equals_3 = Objects.equal(_lowerCase_5, "user");
              if (_equals_3) {
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("from django.contrib.auth.models import User");
                imports.add(_builder_3.toString());
              }
            }
          }
        }
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      {
        for(final String element : imports) {
          _builder_1.append(element);
          _builder_1.newLineIfNotEmpty();
        }
      }
      _xblockexpression = _builder_1;
    }
    return _xblockexpression;
  }

  public CharSequence createEnum(final EnumX enumX) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = enumX.getName();
    _builder.append(_name);
    _builder.append("(models.TextChoices):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("\"\"\"");
    {
      Description _description = enumX.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        String _textfield = enumX.getDescription().getTextfield();
        _builder.append(_textfield, "\t");
      }
    }
    _builder.append("\"\"\"");
    _builder.newLineIfNotEmpty();
    {
      EList<AttributeEnum> _attributes = enumX.getAttributes();
      for(final AttributeEnum attribute : _attributes) {
        _builder.append("\t");
        String _name_1 = attribute.getName();
        _builder.append(_name_1, "\t");
        _builder.append(" = \'");
        String _name_2 = attribute.getName();
        _builder.append(_name_2, "\t");
        _builder.append("\', _(\'");
        String _fullName = attribute.getFullName();
        _builder.append(_fullName, "\t");
        _builder.append("\')");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createModel(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("class ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append(" (");
    {
      Entity _superType = entity.getSuperType();
      boolean _tripleNotEquals = (_superType != null);
      if (_tripleNotEquals) {
        String _name_1 = entity.getSuperType().getName();
        _builder.append(_name_1);
      } else {
        _builder.append("models.Model");
      }
    }
    _builder.append("): ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("\"\"\"");
    {
      Description _description = entity.getDescription();
      boolean _tripleNotEquals_1 = (_description != null);
      if (_tripleNotEquals_1) {
        String _textfield = entity.getDescription().getTextfield();
        _builder.append(_textfield, "\t");
      }
    }
    _builder.append("\"\"\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    {
      EList<Attribute> _attributes = entity.getAttributes();
      for(final Attribute attribute : _attributes) {
        _builder.append("\t");
        CharSequence _create = this.create(attribute);
        _builder.append(_create, "\t");
        _builder.append("\t\t\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    {
      EList<EnumEntityAtribute> _enumentityatributes = entity.getEnumentityatributes();
      for(final EnumEntityAtribute enumAttribute : _enumentityatributes) {
        _builder.append("\t");
        CharSequence _create_1 = this.create(enumAttribute);
        _builder.append(_create_1, "\t");
        _builder.append("\t\t\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    {
      EList<Relation> _relations = entity.getRelations();
      for(final Relation relation : _relations) {
        _builder.append("\t");
        CharSequence _create_2 = this.create(relation);
        _builder.append(_create_2, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("class Meta:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("db_table = \'");
    String _lowerCase = entity.getName().toLowerCase();
    _builder.append(_lowerCase, "\t\t");
    _builder.append("\'");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder;
  }

  public CharSequence create(final EnumEntityAtribute enumEntityAtribute) {
    CharSequence _xblockexpression = null;
    {
      String valor = "";
      int tamanho = 0;
      EList<AttributeEnum> _attributes = enumEntityAtribute.getType().getAttributes();
      for (final AttributeEnum attribute : _attributes) {
        {
          boolean _equals = Objects.equal(valor, "");
          if (_equals) {
            valor = attribute.getName();
          }
          int _length = attribute.getName().length();
          boolean _greaterThan = (_length > tamanho);
          if (_greaterThan) {
            tamanho = attribute.getName().length();
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      String _name = enumEntityAtribute.getName();
      _builder.append(_name);
      _builder.append(" = models.CharField(max_length=");
      _builder.append(tamanho);
      _builder.append(",choices=");
      String _name_1 = enumEntityAtribute.getType().getName();
      _builder.append(_name_1);
      _builder.append(".choices, default=");
      String _name_2 = enumEntityAtribute.getType().getName();
      _builder.append(_name_2);
      _builder.append(".");
      _builder.append(valor);
      _builder.append(")");
      _builder.newLineIfNotEmpty();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }

  public CharSequence create(final Attribute attribute) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    {
      String _lowerCase = attribute.getType().toLowerCase();
      boolean _equals = Objects.equal(_lowerCase, "string");
      if (_equals) {
        String _name = attribute.getName();
        _builder.append(_name);
        _builder.append(" = models.CharField(");
        {
          String _fullName = attribute.getFullName();
          boolean _tripleNotEquals = (_fullName != null);
          if (_tripleNotEquals) {
            _builder.append("\"");
            String _fullName_1 = attribute.getFullName();
            _builder.append(_fullName_1);
            _builder.append("\",");
          }
        }
        _builder.append(" max_length=300,null=True, blank=True)");
        _builder.newLineIfNotEmpty();
      } else {
        String _lowerCase_1 = attribute.getType().toLowerCase();
        boolean _equals_1 = Objects.equal(_lowerCase_1, "decimal");
        if (_equals_1) {
          String _name_1 = attribute.getName();
          _builder.append(_name_1);
          _builder.append(" = models.");
          String _firstUpper = StringExtensions.toFirstUpper(attribute.getType());
          _builder.append(_firstUpper);
          _builder.append("Field(");
          {
            String _fullName_2 = attribute.getFullName();
            boolean _tripleNotEquals_1 = (_fullName_2 != null);
            if (_tripleNotEquals_1) {
              _builder.append("\"");
              String _fullName_3 = attribute.getFullName();
              _builder.append(_fullName_3);
              _builder.append("\",");
            }
          }
          _builder.append(" max_digits=14, decimal_places=2,null=True, blank=True)");
          _builder.newLineIfNotEmpty();
        } else {
          String _lowerCase_2 = attribute.getType().toLowerCase();
          boolean _equals_2 = Objects.equal(_lowerCase_2, "cnpj");
          if (_equals_2) {
            String _name_2 = attribute.getName();
            _builder.append(_name_2);
            _builder.append(" = CNPJField(");
            {
              String _fullName_4 = attribute.getFullName();
              boolean _tripleNotEquals_2 = (_fullName_4 != null);
              if (_tripleNotEquals_2) {
                _builder.append("\"");
                String _fullName_5 = attribute.getFullName();
                _builder.append(_fullName_5);
                _builder.append("\",");
              }
            }
            _builder.append(" null=True, blank=True)");
            _builder.newLineIfNotEmpty();
          } else {
            String _lowerCase_3 = attribute.getType().toLowerCase();
            boolean _equals_3 = Objects.equal(_lowerCase_3, "cpf");
            if (_equals_3) {
              String _name_3 = attribute.getName();
              _builder.append(_name_3);
              _builder.append(" = CPFField(");
              {
                String _fullName_6 = attribute.getFullName();
                boolean _tripleNotEquals_3 = (_fullName_6 != null);
                if (_tripleNotEquals_3) {
                  _builder.append("\"");
                  String _fullName_7 = attribute.getFullName();
                  _builder.append(_fullName_7);
                  _builder.append("\",");
                }
              }
              _builder.append(" null=True, blank=True)");
              _builder.newLineIfNotEmpty();
            } else {
              String _lowerCase_4 = attribute.getType().toLowerCase();
              boolean _equals_4 = Objects.equal(_lowerCase_4, "telefone");
              if (_equals_4) {
                String _name_4 = attribute.getName();
                _builder.append(_name_4);
                _builder.append(" = models.CharField(");
                {
                  String _fullName_8 = attribute.getFullName();
                  boolean _tripleNotEquals_4 = (_fullName_8 != null);
                  if (_tripleNotEquals_4) {
                    _builder.append("\"");
                    String _fullName_9 = attribute.getFullName();
                    _builder.append(_fullName_9);
                    _builder.append("\",");
                  }
                }
                _builder.append(" max_length=27,null=True, blank=True)");
                _builder.newLineIfNotEmpty();
              } else {
                String _lowerCase_5 = attribute.getType().toLowerCase();
                boolean _equals_5 = Objects.equal(_lowerCase_5, "celular");
                if (_equals_5) {
                  String _name_5 = attribute.getName();
                  _builder.append(_name_5);
                  _builder.append(" = models.CharField(");
                  {
                    String _fullName_10 = attribute.getFullName();
                    boolean _tripleNotEquals_5 = (_fullName_10 != null);
                    if (_tripleNotEquals_5) {
                      _builder.append("\"");
                      String _fullName_11 = attribute.getFullName();
                      _builder.append(_fullName_11);
                      _builder.append("\",");
                    }
                  }
                  _builder.append(" max_length=11,null=True, blank=True)");
                  _builder.newLineIfNotEmpty();
                } else {
                  String _lowerCase_6 = attribute.getType().toLowerCase();
                  boolean _equals_6 = Objects.equal(_lowerCase_6, "user");
                  if (_equals_6) {
                    String _name_6 = attribute.getName();
                    _builder.append(_name_6);
                    _builder.append(" = models.ForeignKey(User,blank=True,null=True, on_delete=models.CASCADE)");
                    _builder.newLineIfNotEmpty();
                  } else {
                    String _lowerCase_7 = attribute.getType().toLowerCase();
                    boolean _equals_7 = Objects.equal(_lowerCase_7, "datetime");
                    if (_equals_7) {
                      String _name_7 = attribute.getName();
                      _builder.append(_name_7);
                      _builder.append(" = models.DateTimeField(");
                      {
                        String _fullName_12 = attribute.getFullName();
                        boolean _tripleNotEquals_6 = (_fullName_12 != null);
                        if (_tripleNotEquals_6) {
                          _builder.append("\"");
                          String _fullName_13 = attribute.getFullName();
                          _builder.append(_fullName_13);
                          _builder.append("\",");
                        }
                      }
                      _builder.append("blank=True)");
                      _builder.newLineIfNotEmpty();
                    } else {
                      String _lowerCase_8 = attribute.getType().toLowerCase();
                      boolean _equals_8 = Objects.equal(_lowerCase_8, "url");
                      if (_equals_8) {
                        String _name_8 = attribute.getName();
                        _builder.append(_name_8);
                        _builder.append(" = models.URLField(");
                        {
                          String _fullName_14 = attribute.getFullName();
                          boolean _tripleNotEquals_7 = (_fullName_14 != null);
                          if (_tripleNotEquals_7) {
                            _builder.append("\"");
                            String _fullName_15 = attribute.getFullName();
                            _builder.append(_fullName_15);
                            _builder.append("\",");
                          }
                        }
                        _builder.append("blank=True)");
                        _builder.newLineIfNotEmpty();
                      } else {
                        String _name_9 = attribute.getName();
                        _builder.append(_name_9);
                        _builder.append(" = models.");
                        String _firstUpper_1 = StringExtensions.toFirstUpper(attribute.getType());
                        _builder.append(_firstUpper_1);
                        _builder.append("Field(");
                        {
                          String _fullName_16 = attribute.getFullName();
                          boolean _tripleNotEquals_8 = (_fullName_16 != null);
                          if (_tripleNotEquals_8) {
                            _builder.append("\"");
                            String _fullName_17 = attribute.getFullName();
                            _builder.append(_fullName_17);
                            _builder.append("\",");
                          }
                        }
                        _builder.append(" null=True, blank=True)");
                        _builder.newLineIfNotEmpty();
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence create(final Relation relation) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Class<?> _instanceClass = relation.eClass().getInstanceClass();
      boolean _equals = Objects.equal(_instanceClass, ManyToMany.class);
      if (_equals) {
        final ManyToMany m2m = ((ManyToMany) relation);
        _builder.newLineIfNotEmpty();
        String _name = relation.getName();
        _builder.append(_name);
        _builder.append(" = models.ManyToManyField(");
        String _name_1 = relation.getType().getName();
        _builder.append(_name_1);
        _builder.append(" ");
        {
          Entity _by = m2m.getBy();
          boolean _tripleNotEquals = (_by != null);
          if (_tripleNotEquals) {
            _builder.append(",through=\'");
            String _name_2 = m2m.getBy().getName();
            _builder.append(_name_2);
            _builder.append("\'");
          }
        }
        _builder.append(")");
        _builder.newLineIfNotEmpty();
      } else {
        String _name_3 = relation.getName();
        _builder.append(_name_3);
        _builder.append(" = models.ForeignKey(");
        String _name_4 = relation.getType().getName();
        _builder.append(_name_4);
        _builder.append(",blank=True,null=True, on_delete=models.CASCADE, related_name=");
        String _name_5 = relation.getName();
        _builder.append(_name_5);
        _builder.append("_");
        String _name_6 = relation.getType().getName();
        _builder.append(_name_6);
        _builder.append(")");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    return _builder;
  }
}

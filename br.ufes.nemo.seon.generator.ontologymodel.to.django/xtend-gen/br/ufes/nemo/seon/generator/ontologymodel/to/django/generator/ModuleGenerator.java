package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.AdminGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.DAOGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.FormGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.ModelGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.SerializerGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.TemplateGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.URLGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.UTILGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.ViewGenerator;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ModuleImport;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class ModuleGenerator extends AbstractGenerator {
  @Inject
  private ModelGenerator modelGenerator;

  @Inject
  private AdminGenerator adminGenerator;

  @Inject
  private URLGenerator urlGenerator;

  @Inject
  private ViewGenerator viewGenerator;

  @Inject
  private FormGenerator formGenerator;

  @Inject
  private TemplateGenerator templateGenerator;

  @Inject
  private SerializerGenerator serializerGenerator;

  @Inject
  private DAOGenerator daoGenerator;

  @Inject
  private UTILGenerator utilGenerator;

  private String PATH_LIB_ENTITY = "solution/apps/";

  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module> _filter = Iterables.<br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module.class);
    for (final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module m : _filter) {
      ModuleImport _moduleimported = m.getModuleimported();
      boolean _equals = Objects.equal(_moduleimported, null);
      if (_equals) {
        String _lowerCase = m.getName().toLowerCase();
        String _plus = (this.PATH_LIB_ENTITY + _lowerCase);
        String _plus_1 = (_plus + "/__init__.py");
        fsa.generateFile(_plus_1, this.create_init(m));
        String _lowerCase_1 = m.getName().toLowerCase();
        String _plus_2 = (this.PATH_LIB_ENTITY + _lowerCase_1);
        String _plus_3 = (_plus_2 + "/migrations/__init__.py");
        fsa.generateFile(_plus_3, "");
        String _lowerCase_2 = m.getName().toLowerCase();
        String _plus_4 = (this.PATH_LIB_ENTITY + _lowerCase_2);
        String _plus_5 = (_plus_4 + "/admin.py");
        fsa.generateFile(_plus_5, this.adminGenerator.create(m));
        String _lowerCase_3 = m.getName().toLowerCase();
        String _plus_6 = (this.PATH_LIB_ENTITY + _lowerCase_3);
        String _plus_7 = (_plus_6 + "/templates/");
        String _lowerCase_4 = m.getName().toLowerCase();
        String _plus_8 = (_plus_7 + _lowerCase_4);
        String _plus_9 = (_plus_8 + "/__init__.py");
        fsa.generateFile(_plus_9, 
          "");
        String _lowerCase_5 = m.getName().toLowerCase();
        String _plus_10 = (this.PATH_LIB_ENTITY + _lowerCase_5);
        String _plus_11 = (_plus_10 + "/models.py");
        fsa.generateFile(_plus_11, this.modelGenerator.create(m));
        String _lowerCase_6 = m.getName().toLowerCase();
        String _plus_12 = (this.PATH_LIB_ENTITY + _lowerCase_6);
        String _plus_13 = (_plus_12 + "/factory.py");
        fsa.generateFile(_plus_13, this.createFactory(m));
        String _lowerCase_7 = m.getName().toLowerCase();
        String _plus_14 = (this.PATH_LIB_ENTITY + _lowerCase_7);
        String _plus_15 = (_plus_14 + "/apps.py");
        fsa.generateFile(_plus_15, this.createApps(m));
        this.templateGenerator.create(m, fsa, this.PATH_LIB_ENTITY);
        String _lowerCase_8 = m.getName().toLowerCase();
        String _plus_16 = (this.PATH_LIB_ENTITY + _lowerCase_8);
        String _plus_17 = (_plus_16 + "/web_urls.py");
        fsa.generateFile(_plus_17, this.urlGenerator.createURLWeb(m));
        String _lowerCase_9 = m.getName().toLowerCase();
        String _plus_18 = (this.PATH_LIB_ENTITY + _lowerCase_9);
        String _plus_19 = (_plus_18 + "/api_urls.py");
        fsa.generateFile(_plus_19, this.urlGenerator.createURLAPI(m));
        String _lowerCase_10 = m.getName().toLowerCase();
        String _plus_20 = (this.PATH_LIB_ENTITY + _lowerCase_10);
        String _plus_21 = (_plus_20 + "/dao.py");
        fsa.generateFile(_plus_21, this.daoGenerator.create(m));
        String _lowerCase_11 = m.getName().toLowerCase();
        String _plus_22 = (this.PATH_LIB_ENTITY + _lowerCase_11);
        String _plus_23 = (_plus_22 + "/web_urls.py");
        fsa.generateFile(_plus_23, this.urlGenerator.createURLWeb(m));
        String _lowerCase_12 = m.getName().toLowerCase();
        String _plus_24 = (this.PATH_LIB_ENTITY + _lowerCase_12);
        String _plus_25 = (_plus_24 + "/api_urls.py");
        fsa.generateFile(_plus_25, this.urlGenerator.createURLAPI(m));
        String _lowerCase_13 = m.getName().toLowerCase();
        String _plus_26 = (this.PATH_LIB_ENTITY + _lowerCase_13);
        String _plus_27 = (_plus_26 + "/web_views.py");
        fsa.generateFile(_plus_27, this.viewGenerator.create(m));
        String _lowerCase_14 = m.getName().toLowerCase();
        String _plus_28 = (this.PATH_LIB_ENTITY + _lowerCase_14);
        String _plus_29 = (_plus_28 + "/api_views.py");
        fsa.generateFile(_plus_29, this.viewGenerator.createAPIView(m));
        String _lowerCase_15 = m.getName().toLowerCase();
        String _plus_30 = (this.PATH_LIB_ENTITY + _lowerCase_15);
        String _plus_31 = (_plus_30 + "/forms.py");
        fsa.generateFile(_plus_31, this.formGenerator.create(m));
        String _lowerCase_16 = m.getName().toLowerCase();
        String _plus_32 = (this.PATH_LIB_ENTITY + _lowerCase_16);
        String _plus_33 = (_plus_32 + "/validators.py");
        fsa.generateFile(_plus_33, "");
        String _lowerCase_17 = m.getName().toLowerCase();
        String _plus_34 = (this.PATH_LIB_ENTITY + _lowerCase_17);
        String _plus_35 = (_plus_34 + "/messages.py");
        fsa.generateFile(_plus_35, "");
        String _lowerCase_18 = m.getName().toLowerCase();
        String _plus_36 = (this.PATH_LIB_ENTITY + _lowerCase_18);
        String _plus_37 = (_plus_36 + "/exception.py");
        fsa.generateFile(_plus_37, "");
        EList<AbstractElement> _elements = m.getElements();
        for (final AbstractElement e : _elements) {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals_1 = Objects.equal(_instanceClass, Entity.class);
          if (_equals_1) {
            final Entity entity = ((Entity) e);
            String _lowerCase_19 = m.getName().toLowerCase();
            String _plus_38 = (this.PATH_LIB_ENTITY + _lowerCase_19);
            String _plus_39 = (_plus_38 + "/test/unit/");
            String _lowerCase_20 = entity.getName().toLowerCase();
            String _plus_40 = (_plus_39 + _lowerCase_20);
            String _plus_41 = (_plus_40 + "_tests.py");
            fsa.generateFile(_plus_41, this.createclasstest(entity, m));
          }
        }
        String _lowerCase_21 = m.getName().toLowerCase();
        String _plus_42 = (this.PATH_LIB_ENTITY + _lowerCase_21);
        String _plus_43 = (_plus_42 + "/services.py");
        fsa.generateFile(_plus_43, this.createServices(m));
        String _lowerCase_22 = m.getName().toLowerCase();
        String _plus_44 = (this.PATH_LIB_ENTITY + _lowerCase_22);
        String _plus_45 = (_plus_44 + "/signals.py");
        fsa.generateFile(_plus_45, this.createSignals(m));
        String _lowerCase_23 = m.getName().toLowerCase();
        String _plus_46 = (this.PATH_LIB_ENTITY + _lowerCase_23);
        String _plus_47 = (_plus_46 + "/serializers.py");
        fsa.generateFile(_plus_47, this.serializerGenerator.create(m));
        String _lowerCase_24 = m.getName().toLowerCase();
        String _plus_48 = (this.PATH_LIB_ENTITY + _lowerCase_24);
        String _plus_49 = (_plus_48 + "/utils.py");
        fsa.generateFile(_plus_49, this.utilGenerator.createUtil());
        String _lowerCase_25 = m.getName().toLowerCase();
        String _plus_50 = (this.PATH_LIB_ENTITY + _lowerCase_25);
        String _plus_51 = (_plus_50 + "/pagination.py");
        fsa.generateFile(_plus_51, this.pagination());
      }
    }
  }

  public CharSequence pagination() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from rest_framework import pagination");
    _builder.newLine();
    _builder.append("from rest_framework.response import Response");
    _builder.newLine();
    _builder.newLine();
    _builder.append("class CustomPagination(pagination.PageNumberPagination):");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("page_size = 10");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("page_size_query_param = \'page_size\'");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("max_page_size = 1000");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("def get_paginated_response(self, data):");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("return Response({");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("\'meta\': {");
    _builder.newLine();
    _builder.append("                ");
    _builder.append("\'current_page\': self.page.number,");
    _builder.newLine();
    _builder.append("                ");
    _builder.append("\'per_page\': self.page.paginator.per_page,");
    _builder.newLine();
    _builder.append("                ");
    _builder.append("\'max_per_page\': self.max_page_size,");
    _builder.newLine();
    _builder.append("                ");
    _builder.append("\'total\': self.page.paginator.count");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("},");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("\'data\': data");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("})");
    _builder.newLine();
    return _builder;
  }

  public CharSequence create_init(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("default_app_config = \'apps.");
    String _lowerCase = module.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".apps.");
    String _firstUpper = StringExtensions.toFirstUpper(module.getName());
    _builder.append(_firstUpper);
    _builder.append("Config\'");
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public CharSequence createApps(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.apps import AppConfig");
    _builder.newLine();
    _builder.append("from django.utils.translation import ugettext_lazy as _");
    _builder.newLine();
    _builder.newLine();
    _builder.append("class ");
    String _firstUpper = StringExtensions.toFirstUpper(module.getName());
    _builder.append(_firstUpper);
    _builder.append("Config(AppConfig):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("name  = \'apps.");
    String _lowerCase = module.getName().toLowerCase();
    _builder.append(_lowerCase, "\t");
    _builder.append("\'");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("label = \'apps_");
    String _lowerCase_1 = module.getName().toLowerCase();
    _builder.append(_lowerCase_1, "\t");
    _builder.append("\'");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def ready(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("import apps.");
    String _lowerCase_2 = module.getName().toLowerCase();
    _builder.append(_lowerCase_2, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder;
  }

  public CharSequence createFactory(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            _builder.newLine();
            _builder.append("class ");
            String _name = entity.getName();
            _builder.append(_name);
            _builder.append("Factory(factory.django.DjangoModelFactory):");
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            _builder.append("class Meta:");
            _builder.newLine();
            _builder.append("\t\t");
            _builder.append("model = ");
            String _lowerCase = module.getName().toLowerCase();
            _builder.append(_lowerCase, "\t\t");
            _builder.append(".");
            String _name_1 = entity.getName();
            _builder.append(_name_1, "\t\t");
            _builder.newLineIfNotEmpty();
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createattributeJsontest(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Attribute> _attributes = entity.getAttributes();
      for(final Attribute attribute : _attributes) {
        _builder.append("\'");
        String _name = attribute.getName();
        _builder.append(_name);
        _builder.append("\' : ");
        CharSequence _createAtrributeValuesTest = this.createAtrributeValuesTest(attribute);
        _builder.append(_createAtrributeValuesTest);
        _builder.append(",");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public CharSequence createattributetest(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Attribute> _attributes = entity.getAttributes();
      for(final Attribute attribute : _attributes) {
        String _name = attribute.getName();
        _builder.append(_name);
        _builder.append(" = ");
        CharSequence _createAtrributeValuesTest = this.createAtrributeValuesTest(attribute);
        _builder.append(_createAtrributeValuesTest);
        _builder.append(",");
        _builder.newLineIfNotEmpty();
      }
    }
    return _builder;
  }

  public CharSequence createAtrributeValuesTest(final Attribute attribute) {
    StringConcatenation _builder = new StringConcatenation();
    {
      String _lowerCase = attribute.getType().toLowerCase();
      boolean _equals = Objects.equal(_lowerCase, "string");
      if (_equals) {
        _builder.append(" self.faker.first_name()");
        _builder.newLineIfNotEmpty();
      } else {
        String _lowerCase_1 = attribute.getType().toLowerCase();
        boolean _equals_1 = Objects.equal(_lowerCase_1, "decimal");
        if (_equals_1) {
          _builder.append(" random.uniform(0.00, 100.5)");
          _builder.newLineIfNotEmpty();
        } else {
          String _lowerCase_2 = attribute.getType().toLowerCase();
          boolean _equals_2 = Objects.equal(_lowerCase_2, "cnpj");
          if (_equals_2) {
            _builder.append(" self.faker.cnpj()");
            _builder.newLineIfNotEmpty();
          } else {
            String _lowerCase_3 = attribute.getType().toLowerCase();
            boolean _equals_3 = Objects.equal(_lowerCase_3, "cpf");
            if (_equals_3) {
              _builder.append(" self.faker.cpf()");
              _builder.newLineIfNotEmpty();
            } else {
              String _lowerCase_4 = attribute.getType().toLowerCase();
              boolean _equals_4 = Objects.equal(_lowerCase_4, "telefone");
              if (_equals_4) {
                _builder.append(" self.faker.phone_number()");
                _builder.newLineIfNotEmpty();
              } else {
                String _lowerCase_5 = attribute.getType().toLowerCase();
                boolean _equals_5 = Objects.equal(_lowerCase_5, "celular");
                if (_equals_5) {
                  _builder.append(" self.faker.phone_number()");
                  _builder.newLineIfNotEmpty();
                } else {
                  String _lowerCase_6 = attribute.getType().toLowerCase();
                  boolean _equals_6 = Objects.equal(_lowerCase_6, "datetime");
                  if (_equals_6) {
                    _builder.append(" self.faker.date()");
                    _builder.newLineIfNotEmpty();
                  } else {
                    String _lowerCase_7 = attribute.getType().toLowerCase();
                    boolean _equals_7 = Objects.equal(_lowerCase_7, "url");
                    if (_equals_7) {
                      _builder.append(" \'http://\'+ self.fake.domain_name()");
                      _builder.newLineIfNotEmpty();
                    } else {
                      String _lowerCase_8 = attribute.getType().toLowerCase();
                      boolean _equals_8 = Objects.equal(_lowerCase_8, "date");
                      if (_equals_8) {
                        _builder.append(" self.faker.date()");
                        _builder.newLineIfNotEmpty();
                      } else {
                        String _lowerCase_9 = attribute.getType().toLowerCase();
                        boolean _equals_9 = Objects.equal(_lowerCase_9, "email");
                        if (_equals_9) {
                          _builder.append(" self.faker.ascii_company_email()");
                          _builder.newLineIfNotEmpty();
                        } else {
                          String _lowerCase_10 = attribute.getType().toLowerCase();
                          boolean _equals_10 = Objects.equal(_lowerCase_10, "integer");
                          if (_equals_10) {
                            _builder.append(" random.uniform(0, 100)");
                            _builder.newLineIfNotEmpty();
                          } else {
                            _builder.append(" \'TO DO\'\t\t");
                            _builder.newLineIfNotEmpty();
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createclasstest(final Entity entity, final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import json");
    _builder.newLine();
    _builder.append("from rest_framework import status");
    _builder.newLine();
    _builder.append("from django.test import TestCase, Client");
    _builder.newLine();
    _builder.append("from django.urls import reverse");
    _builder.newLine();
    _builder.append("from ");
    String _lowerCase = module.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".models import ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.append("from ");
    String _lowerCase_1 = module.getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append(".serializers import ");
    String _name_1 = entity.getName();
    _builder.append(_name_1);
    _builder.append("Serializer");
    _builder.newLineIfNotEmpty();
    _builder.append("from faker import Faker");
    _builder.newLine();
    _builder.append("import random");
    _builder.newLine();
    _builder.newLine();
    _builder.append("class ");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append("Tests(TestCase):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def setUp(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.faker = Faker(\'pt_BR\')");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.client = Client()");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.");
    String _lowerCase_2 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_2, "\t\t");
    _builder.append("_1 = ");
    String _name_3 = entity.getName();
    _builder.append(_name_3, "\t\t");
    _builder.append(".objects.create(");
    CharSequence _createattributetest = this.createattributetest(entity);
    _builder.append(_createattributetest, "\t\t");
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("self.");
    String _lowerCase_3 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_3, "\t\t");
    _builder.append("_2 = ");
    String _name_4 = entity.getName();
    _builder.append(_name_4, "\t\t");
    _builder.append(".objects.create(");
    CharSequence _createattributetest_1 = this.createattributetest(entity);
    _builder.append(_createattributetest_1, "\t\t");
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("self.");
    String _lowerCase_4 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_4, "\t\t");
    _builder.append("_3 = ");
    String _name_5 = entity.getName();
    _builder.append(_name_5, "\t\t");
    _builder.append(".objects.create(");
    CharSequence _createattributetest_2 = this.createattributetest(entity);
    _builder.append(_createattributetest_2, "\t\t");
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.valid_payload = {");
    _builder.newLine();
    _builder.append("\t\t\t");
    CharSequence _createattributeJsontest = this.createattributeJsontest(entity);
    _builder.append(_createattributeJsontest, "\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.invalid_payload = {");
    _builder.newLine();
    _builder.append("\t\t\t");
    CharSequence _createattributeJsontest_1 = this.createattributeJsontest(entity);
    _builder.append(_createattributeJsontest_1, "\t\t\t");
    _builder.append("    ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_valid_create(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.post(reverse(\'");
    String _lowerCase_5 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_5, "\t\t");
    _builder.append("-api-list\'), ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("data=json.dumps(self.valid_payload),");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("content_type=\'application/json\')");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_201_CREATED)\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_invalid_create(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.post(");
    _builder.newLine();
    _builder.append("\t            ");
    _builder.append("reverse(\'");
    String _lowerCase_6 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_6, "\t            ");
    _builder.append("-api-list\'),");
    _builder.newLineIfNotEmpty();
    _builder.append("\t            ");
    _builder.append("data=json.dumps(self.invalid_payload),");
    _builder.newLine();
    _builder.append("\t            ");
    _builder.append("content_type=\'application/json\'");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append(")");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_valid_upload(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.put(reverse(\'");
    String _lowerCase_7 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_7, "\t\t");
    _builder.append("-detail\',kwargs={\'pk\': self.");
    String _lowerCase_8 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_8, "\t\t");
    _builder.append("_1.id}),");
    _builder.newLineIfNotEmpty();
    _builder.append("\t            ");
    _builder.append("data=json.dumps(self.valid_payload), content_type=\'application/json\')");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_200_OK)");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_invalid_upload(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.put(reverse(\'");
    String _lowerCase_9 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_9, "\t\t");
    _builder.append("-detail\',kwargs={\'pk\': self.");
    String _lowerCase_10 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_10, "\t\t");
    _builder.append("_1.id}),");
    _builder.newLineIfNotEmpty();
    _builder.append("\t     ");
    _builder.append("data=json.dumps(self.invalid_payload), content_type=\'application/json\')");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("# retornando todos os elementos\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_retrieve_all(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.get(reverse(\'");
    String _lowerCase_11 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_11, "\t\t");
    _builder.append("-api-list\'))");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("data = ");
    String _name_6 = entity.getName();
    _builder.append(_name_6, "\t\t");
    _builder.append(".objects.all()");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("serializer = ");
    String _name_7 = entity.getName();
    _builder.append(_name_7, "\t\t");
    _builder.append("Serializer(data, context={\'request\': None}, many=True)");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("#Aqui deve comparar todos os compos do objeto com serialização");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.data, serializer.data)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertIsNotNone(response.data)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_200_OK)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("# retornando um elemento");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_valid_get_element(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.get(reverse(\'");
    String _lowerCase_12 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_12, "\t\t");
    _builder.append("-detail\',kwargs={\'pk\': self.");
    String _lowerCase_13 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_13, "\t\t");
    _builder.append("_1.id}))");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("data = ");
    String _name_8 = entity.getName();
    _builder.append(_name_8, "\t\t");
    _builder.append(".objects.get(pk=self.condicao_1.id)");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("#Aqui deve comparar todos os compos do objeto com serialização");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(str(data.uuid),response.data[\'uuid\'])");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertIsNotNone(response.data)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_200_OK)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("# erro ao retornar um elemento invalido");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_invalid_get_element(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.get(reverse(\'");
    String _lowerCase_14 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_14, "\t\t");
    _builder.append("-detail\',kwargs={\'pk\': 666}))");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("# Delete um elemento valido");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_valid_delete(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.delete(reverse(\'");
    String _lowerCase_15 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_15, "\t\t");
    _builder.append("-detail\',kwargs={\'pk\': self.");
    String _lowerCase_16 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_16, "\t\t");
    _builder.append("_1.id}))");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("# Delete um elemento valido");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def test_invalid_delete(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("response = self.client.delete(reverse(\'");
    String _lowerCase_17 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_17, "\t\t");
    _builder.append("-detail\',kwargs={\'pk\': 666}))");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createServices(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from .models import *");
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createService = this.createService(entity);
            _builder.append(_createService);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createService(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append("Service():");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("\"\"\" Service of ");
    String _name_1 = entity.getName();
    _builder.append(_name_1, "\t");
    _builder.append("\"\"\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createSignals(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("from django.db.models.signals import *");
    _builder.newLine();
    _builder.append("from django.dispatch import receiver");
    _builder.newLine();
    _builder.append("from .services import *");
    _builder.newLine();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            _builder.append("## signals from ");
            String _name = entity.getName();
            _builder.append(_name);
            _builder.append(" ");
            _builder.newLineIfNotEmpty();
            CharSequence _createsignal = this.createsignal(entity);
            _builder.append(_createsignal);
            _builder.newLineIfNotEmpty();
            _builder.append("#######");
            _builder.newLine();
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    return _builder;
  }

  public CharSequence createsignal(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@receiver(pre_init, sender=");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def pre_init_");
    String _lowerCase = entity.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("(sender, *args, **kwargs):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("@receiver(post_init, sender=");
    String _name_1 = entity.getName();
    _builder.append(_name_1);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def post_init_");
    String _lowerCase_1 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append("(sender, instance):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(pre_save, sender=");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def pre_save_");
    String _lowerCase_2 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_2);
    _builder.append("(sender, instance, raw, using, update_fields):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(post_save, sender=");
    String _name_3 = entity.getName();
    _builder.append(_name_3);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def post_save_");
    String _lowerCase_3 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_3);
    _builder.append("(sender, instance, created, raw, using, update_fields):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("@receiver(pre_delete, sender=");
    String _name_4 = entity.getName();
    _builder.append(_name_4);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def pre_delete_");
    String _lowerCase_4 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_4);
    _builder.append("(sender, instance, using):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("@receiver(post_delete, sender=");
    String _name_5 = entity.getName();
    _builder.append(_name_5);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def post_delete_");
    String _lowerCase_5 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_5);
    _builder.append("(sender, instance, using):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("@receiver(m2m_changed, sender=");
    String _name_6 = entity.getName();
    _builder.append(_name_6);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def m2m_changed_");
    String _lowerCase_6 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_6);
    _builder.append("(sender, instance, action, reverse, model, pk_set, using):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    return _builder;
  }
}

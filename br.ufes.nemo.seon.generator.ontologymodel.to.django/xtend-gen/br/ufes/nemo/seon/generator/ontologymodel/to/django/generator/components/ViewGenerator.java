package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class ViewGenerator {
  public CharSequence createAux(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module, final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append("ListView(LoginRequiredMixin,ListView):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("model = ");
    String _name_1 = entity.getName();
    _builder.append(_name_1, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("template_name  = \'");
    String _lowerCase = module.getName().toLowerCase();
    _builder.append(_lowerCase, "\t");
    _builder.append("/");
    String _lowerCase_1 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_1, "\t");
    _builder.append("/");
    String _lowerCase_2 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_2, "\t");
    _builder.append("_list.html\'");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("class ");
    String _name_2 = entity.getName();
    _builder.append(_name_2, "\t");
    _builder.append("CreateView(LoginRequiredMixin,SuccessMessageMixin,CreateView):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("model = ");
    String _name_3 = entity.getName();
    _builder.append(_name_3, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("form_class = ");
    String _name_4 = entity.getName();
    _builder.append(_name_4, "\t\t");
    _builder.append("Form");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("template_name  = \'");
    String _lowerCase_3 = module.getName().toLowerCase();
    _builder.append(_lowerCase_3, "\t\t");
    _builder.append("/");
    String _lowerCase_4 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_4, "\t\t");
    _builder.append("/");
    String _lowerCase_5 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_5, "\t\t");
    _builder.append("_form.html\'");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("success_url = reverse_lazy(\'");
    String _lowerCase_6 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_6, "\t\t");
    _builder.append("-web-list\')");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("success_message = \'Cadastro realizado com sucesso!\'");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("class ");
    String _name_5 = entity.getName();
    _builder.append(_name_5, "\t");
    _builder.append("UpdateView(LoginRequiredMixin,SuccessMessageMixin,UpdateView):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("model = ");
    String _name_6 = entity.getName();
    _builder.append(_name_6, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("form_class = ");
    String _name_7 = entity.getName();
    _builder.append(_name_7, "\t\t");
    _builder.append("Form");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("template_name  = \'");
    String _lowerCase_7 = module.getName().toLowerCase();
    _builder.append(_lowerCase_7, "\t\t");
    _builder.append("/");
    String _lowerCase_8 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_8, "\t\t");
    _builder.append("/");
    String _lowerCase_9 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_9, "\t\t");
    _builder.append("_form.html\'");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("success_url = reverse_lazy(\'");
    String _lowerCase_10 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_10, "\t\t");
    _builder.append("-web-list\')");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("success_message = \'Dados atualizados com sucesso!\'");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("class ");
    String _name_8 = entity.getName();
    _builder.append(_name_8, "\t");
    _builder.append("DeleteView(LoginRequiredMixin,SuccessMessageMixin,DeleteView):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("model = ");
    String _name_9 = entity.getName();
    _builder.append(_name_9, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("form_class = ");
    String _name_10 = entity.getName();
    _builder.append(_name_10, "\t\t");
    _builder.append("Form");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("template_name  = \'");
    String _lowerCase_11 = module.getName().toLowerCase();
    _builder.append(_lowerCase_11, "\t\t");
    _builder.append("/");
    String _lowerCase_12 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_12, "\t\t");
    _builder.append("/");
    String _lowerCase_13 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_13, "\t\t");
    _builder.append("_confirm_delete.html\'");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("success_url = reverse_lazy(\'");
    String _lowerCase_14 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_14, "\t\t");
    _builder.append("-web-list\')");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("def delete(self, request, *args, **kwargs):");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("response = super().delete(request, *args, **kwargs)");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("messages.success(self.request, \'Exclusão realizada com sucesso!\')");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return response");
    _builder.newLine();
    return _builder;
  }

  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module, final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.shortcuts import render");
    _builder.newLine();
    _builder.append("from django.urls import reverse_lazy");
    _builder.newLine();
    _builder.append("from django.views.generic.list import ListView");
    _builder.newLine();
    _builder.append("from django.views.generic.edit import CreateView,UpdateView, DeleteView");
    _builder.newLine();
    _builder.append("from django.contrib.auth.mixins import LoginRequiredMixin");
    _builder.newLine();
    _builder.append("from django.contrib.messages.views import SuccessMessageMixin");
    _builder.newLine();
    _builder.append("from django.contrib import messages");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("from .forms import *\t");
    _builder.newLine();
    _builder.newLine();
    CharSequence _createAux = this.createAux(module, entity);
    _builder.append(_createAux);
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.shortcuts import render");
    _builder.newLine();
    _builder.append("from django.urls import reverse_lazy");
    _builder.newLine();
    _builder.append("from django.views.generic.list import ListView");
    _builder.newLine();
    _builder.append("from django.views.generic.edit import CreateView,UpdateView, DeleteView");
    _builder.newLine();
    _builder.append("from django.contrib.auth.mixins import LoginRequiredMixin");
    _builder.newLine();
    _builder.append("from django.contrib.messages.views import SuccessMessageMixin");
    _builder.newLine();
    _builder.append("from django.contrib import messages");
    _builder.newLine();
    _builder.newLine();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("from .forms import *\t");
    _builder.newLine();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createAux = this.createAux(module, entity);
            _builder.append(_createAux);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }

  public CharSequence searchfields(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Attribute> _attributes = entity.getAttributes();
      for(final Attribute a : _attributes) {
        {
          String _type = a.getType();
          boolean _notEquals = (!Objects.equal(_type, "file"));
          if (_notEquals) {
            _builder.append("\'");
            String _name = a.getName();
            _builder.append(_name);
            _builder.append("\',");
          }
        }
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public CharSequence createAPIViewAux(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module Module, final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append("ViewSet(ModelViewSet):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("queryset = ");
    String _name_1 = entity.getName();
    _builder.append(_name_1, "\t");
    _builder.append(".objects.all()");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pagination_class = CustomPagination");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("authentication_classes = [OAuth2Authentication, SessionAuthentication]");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("filter_backends = (filters.SearchFilter, ");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.append("filters.OrderingFilter, ");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.append("django_filters.rest_framework.DjangoFilterBackend)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("filterset_fields = \'__all__\'\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("search_fields = [");
    CharSequence _searchfields = this.searchfields(entity);
    _builder.append(_searchfields, "\t");
    _builder.append("]");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("ordering_fields = \'__all__\'\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("def get_serializer_class(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if self.request.method in [\'GET\']:");
    _builder.newLine();
    _builder.append("\t    \t");
    _builder.append("return ");
    String _name_2 = entity.getName();
    _builder.append(_name_2, "\t    \t");
    _builder.append("ReadSerializer");
    _builder.newLineIfNotEmpty();
    _builder.append("\t    ");
    _builder.append("return ");
    String _name_3 = entity.getName();
    _builder.append(_name_3, "\t    ");
    _builder.append("WriteSerializer");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder;
  }

  /**
   * def createAPIViewAux(Module Module, Entity entity)'''
   * class «entity.name»APIList(generics.ListCreateAPIView):
   * queryset = «entity.name».objects.all()
   * serializer_class = «entity.name»Serializer
   * pagination_class = CustomPagination
   * authentication_classes = [OAuth2Authentication, SessionAuthentication]
   * permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
   * filter_backends = (filters.SearchFilter, filters.OrderingFilter, django_filters.rest_framework.DjangoFilterBackend)
   * filterset_fields = '__all__'
   * search_fields = [«entity.searchfields»]
   * ordering_fields = '__all__'
   * 
   * class «entity.name»APIDetail(generics.RetrieveUpdateDestroyAPIView):
   * queryset = «entity.name».objects.all()
   * serializer_class = «entity.name»Serializer
   * pagination_class = CustomPagination
   * authentication_classes = [OAuth2Authentication, SessionAuthentication]
   * permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
   * filter_backends = (filters.SearchFilter, filters.OrderingFilter, django_filters.rest_framework.DjangoFilterBackend)
   * filterset_fields = '__all__'
   * search_fields = [«entity.searchfields»]
   * ordering_fields = '__all__'
   * '''
   */
  public CharSequence createAPIView(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module, final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from rest_framework import generics");
    _builder.newLine();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("from .serializers import *");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("from django_filters import rest_framework as filters");
    _builder.newLine();
    _builder.append("from rest_framework.permissions import IsAdminUser");
    _builder.newLine();
    _builder.append("from rest_condition import Or");
    _builder.newLine();
    _builder.append("from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication");
    _builder.newLine();
    _builder.append("from rest_framework.authentication import SessionAuthentication");
    _builder.newLine();
    _builder.append("from .pagination import CustomPagination");
    _builder.newLine();
    _builder.append("from rest_framework import generics");
    _builder.newLine();
    _builder.append("from rest_framework import filters");
    _builder.newLine();
    _builder.append("import django_filters.rest_framework");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    CharSequence _createAPIViewAux = this.createAPIViewAux(module, entity);
    _builder.append(_createAPIViewAux);
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public CharSequence createImport(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            String _name = entity.getName();
            _builder.append(_name);
            _builder.append(",");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
      }
    }
    return _builder;
  }

  public CharSequence createSerializar(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            String _name = entity.getName();
            _builder.append(_name);
            _builder.append("ReadSerializer,");
            String _name_1 = entity.getName();
            _builder.append(_name_1);
            _builder.append("WriteSerializer,");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t");
      }
    }
    return _builder;
  }

  public CharSequence createAPIView(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from .models import (");
    CharSequence _createImport = this.createImport(module);
    _builder.append(_createImport);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("from .serializers import (");
    CharSequence _createSerializar = this.createSerializar(module);
    _builder.append(_createSerializar);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("from rest_framework.viewsets import ModelViewSet");
    _builder.newLine();
    _builder.append("from django_filters import rest_framework as filters");
    _builder.newLine();
    _builder.append("from rest_framework.permissions import IsAdminUser");
    _builder.newLine();
    _builder.append("from rest_condition import Or");
    _builder.newLine();
    _builder.append("from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication");
    _builder.newLine();
    _builder.append("from rest_framework.authentication import SessionAuthentication");
    _builder.newLine();
    _builder.append("from .pagination import CustomPagination");
    _builder.newLine();
    _builder.append("from rest_framework import generics");
    _builder.newLine();
    _builder.append("from rest_framework import filters");
    _builder.newLine();
    _builder.append("import django_filters.rest_framework");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createAPIViewAux = this.createAPIViewAux(module, entity);
            _builder.append(_createAPIViewAux);
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            _builder.newLine();
          }
        }
      }
    }
    _builder.newLine();
    return _builder;
  }
}

package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration;
import com.google.common.collect.Iterables;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class ScriptGenerator extends AbstractGenerator {
  private String PATH_LIB = "/solution/";

  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    for (final Configuration configuration : _filter) {
      {
        final String softwareName = configuration.getSoftware().getName();
        String _lowerCase = configuration.getSoftware().getName().toLowerCase();
        String _plus = ((this.PATH_LIB + "/script_deploy/gunicorn/") + _lowerCase);
        String _plus_1 = (_plus + ".socket");
        fsa.generateFile(_plus_1, this.createApplicationSocket(softwareName));
        String _lowerCase_1 = configuration.getSoftware().getName().toLowerCase();
        String _plus_2 = ((this.PATH_LIB + "/script_deploy/gunicorn/") + _lowerCase_1);
        String _plus_3 = (_plus_2 + ".service");
        fsa.generateFile(_plus_3, this.createApplicationService(softwareName));
        String _lowerCase_2 = softwareName.toLowerCase();
        String _plus_4 = ((this.PATH_LIB + "/script_deploy/nginx/") + _lowerCase_2);
        fsa.generateFile(_plus_4, this.createNginixfile(softwareName));
        fsa.generateFile((this.PATH_LIB + "/script_deploy/db/sqlmigrate.py"), this.createSQLmigratePython());
        fsa.generateFile((this.PATH_LIB + "/sqlmigrate.sh"), this.createSQLMigrateSH());
        fsa.generateFile((this.PATH_LIB + "/run-linux.sh"), this.createRunLinuxSH(softwareName));
      }
    }
  }

  public CharSequence createApplicationSocket(final String softwareName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[Unit]");
    _builder.newLine();
    _builder.append("Description=");
    _builder.append(softwareName);
    _builder.append(" socket");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("[Socket]");
    _builder.newLine();
    _builder.append("ListenStream=/run/");
    String _lowerCase = softwareName.toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".socket");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("[Install]");
    _builder.newLine();
    _builder.append("WantedBy=sockets.target");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createApplicationService(final String softwareName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[Unit]");
    _builder.newLine();
    _builder.append("Description=");
    _builder.append(softwareName);
    _builder.append(" service");
    _builder.newLineIfNotEmpty();
    _builder.append("Requires=");
    String _lowerCase = softwareName.toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".socket");
    _builder.newLineIfNotEmpty();
    _builder.append("After=network.target");
    _builder.newLine();
    _builder.newLine();
    _builder.append("[Service]");
    _builder.newLine();
    _builder.append("User=root");
    _builder.newLine();
    _builder.append("Group=www-data");
    _builder.newLine();
    _builder.append("WorkingDirectory=/xxxxx/");
    String _lowerCase_1 = softwareName.toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.newLineIfNotEmpty();
    _builder.append("ExecStart=/xxxxx/");
    String _lowerCase_2 = softwareName.toLowerCase();
    _builder.append(_lowerCase_2);
    _builder.append("/.venv/bin/gunicorn \\");
    _builder.newLineIfNotEmpty();
    _builder.append("          ");
    _builder.append("--access-logfile - \\");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("--workers 3 \\");
    _builder.newLine();
    _builder.append("          ");
    _builder.append("--bind unix:/run/factor.sock \\");
    _builder.newLine();
    _builder.append("          ");
    String _lowerCase_3 = softwareName.toLowerCase();
    _builder.append(_lowerCase_3, "          ");
    _builder.append(".wsgi:application");
    _builder.newLineIfNotEmpty();
    _builder.append("          ");
    _builder.newLine();
    _builder.append("[Install]");
    _builder.newLine();
    _builder.append("WantedBy=multi-user.target");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createNginixfile(final String softwareName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("server {");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("listen xxxxxx;");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("server_name xxxxx;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("location = /favicon.ico { access_log off; log_not_found off; }");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("location /staticfiles/ {");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("root /xxxx/xxx/");
    String _lowerCase = softwareName.toLowerCase();
    _builder.append(_lowerCase, "        ");
    _builder.append("/");
    String _lowerCase_1 = softwareName.toLowerCase();
    _builder.append(_lowerCase_1, "        ");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("location / {");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("include proxy_params;");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("proxy_pass http://unix:/run/");
    String _lowerCase_2 = softwareName.toLowerCase();
    _builder.append(_lowerCase_2, "        ");
    _builder.append(".sock;");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createSQLmigratePython() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import string");
    _builder.newLine();
    _builder.append("f = open(\"migrationsplan.txt\", \"r\")");
    _builder.newLine();
    _builder.append("count  = 0");
    _builder.newLine();
    _builder.append("commands = []");
    _builder.newLine();
    _builder.append("for x in f:");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("#separando o nome do app com do arquivo  ");
    _builder.newLine();
    _builder.append("  ");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("apps = x.rsplit( \".\", 1 )[ 0 ]");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("#Pegando apenas o nome do app");
    _builder.newLine();
    _builder.newLine();
    _builder.append("  ");
    _builder.append("apps = apps.split()[2]");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("apps = apps.translate({ord(c): None for c in string.whitespace}) ");
    _builder.newLine();
    _builder.append("  ");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("#pegando o nome do migrate");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("migrate = x.rsplit( \".\", 1 )[ 1 ]");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("migrate = migrate.strip()");
    _builder.newLine();
    _builder.append("  ");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("migrate = migrate.translate({ord(c): None for c in string.whitespace})");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("command = \"\"");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("command = \"python manage.py sqlmigrate \"+apps+\" \"+migrate+\' > ./script_deploy/db/sql/\'+str(count)+\'_\'+apps+\'_\'+migrate+\'.sql\' ");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("commands.append (command)");
    _builder.newLine();
    _builder.append("  ");
    _builder.append("count = count +1");
    _builder.newLine();
    _builder.newLine();
    _builder.append("file = open(\'sql_command_migrates.sh\',\'w\')");
    _builder.newLine();
    _builder.append("for command in commands:");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("file.write(command+\'\\n\')");
    _builder.newLine();
    _builder.append("file.close()");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createSQLMigrateSH() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("python manage.py makemigrations");
    _builder.newLine();
    _builder.append("python manage.py showmigrations --plan > migrationsplan.txt");
    _builder.newLine();
    _builder.append("python script_deploy/db/sqlmigrate.py ");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createRunLinuxSH(final String softwareName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("gunicorn ");
    String _lowerCase = softwareName.toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".wsgi:application --bind 0.0.0.0:8000 \\");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("--error-logfile error.log \\");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("--access-logfile access.log");
    _builder.newLine();
    return _builder;
  }
}

package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class SerializerGenerator {
  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from rest_framework import serializers");
    _builder.newLine();
    _builder.append("from .models import (");
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        _builder.newLineIfNotEmpty();
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            String _name = entity.getName();
            _builder.append(_name, "\t");
            _builder.append(",");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t\t");
      }
    }
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<AbstractElement> _elements_1 = module.getElements();
      for(final AbstractElement e_1 : _elements_1) {
        {
          Class<?> _instanceClass_1 = e_1.eClass().getInstanceClass();
          boolean _equals_1 = Objects.equal(_instanceClass_1, Entity.class);
          if (_equals_1) {
            final Entity entity_1 = ((Entity) e_1);
            _builder.newLineIfNotEmpty();
            CharSequence _createSerializer = this.createSerializer(entity_1);
            _builder.append(_createSerializer);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createSerializer(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append("WriteSerializer (serializers.ModelSerializer):");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("class Meta:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("model = ");
    String _name_1 = entity.getName();
    _builder.append(_name_1, "\t\t");
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("fields = \'__all__\'");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("exclude = (\"polymorphic_ctype\",)");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.newLine();
    _builder.newLine();
    _builder.append("class ");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append("ReadSerializer (serializers.ModelSerializer):");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("class Meta:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("model = ");
    String _name_3 = entity.getName();
    _builder.append(_name_3, "\t\t");
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("fields = \'__all__\'");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("depth = 1");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("exclude = (\"polymorphic_ctype\",)");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    return _builder;
  }
}

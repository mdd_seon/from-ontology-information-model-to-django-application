package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class UTILGenerator {
  public CharSequence createUtil() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from hashids import Hashids");
    _builder.newLine();
    _builder.append("from django.conf import settings");
    _builder.newLine();
    _builder.newLine();
    _builder.append("hashids = Hashids(settings.HASHIDS_SALT, min_length=8)");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("def h_encode(id):");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("return hashids.encode(id)");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("def h_decode(h):");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("z = hashids.decode(h)");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("if z:");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("return z[0]");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("class HashIdConverter:");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("regex = \'[a-zA-Z0-9]{8,}\'");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("def to_python(self, value):");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("return h_decode(value)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("    ");
    _builder.append("def to_url(self, value):");
    _builder.newLine();
    _builder.append("        ");
    _builder.append("return h_encode(value)");
    _builder.newLine();
    return _builder;
  }
}

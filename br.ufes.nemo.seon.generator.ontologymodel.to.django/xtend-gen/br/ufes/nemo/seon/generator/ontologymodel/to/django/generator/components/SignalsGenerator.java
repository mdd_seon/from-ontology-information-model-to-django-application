package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class SignalsGenerator {
  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.db.models.signals import pre_save, pre_init, post_init, post_save,m2m_changed,pre_delete, post_delete");
    _builder.newLine();
    _builder.append("from django.dispatch import receiver");
    _builder.newLine();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createSignal = this.createSignal(entity);
            _builder.append(_createSignal);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createSignal(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("@receiver(pre_init, sender=");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def pre_init_");
    String _lowerCase = entity.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("(sender, *args, **kwargs):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(post_init, sender=");
    String _name_1 = entity.getName();
    _builder.append(_name_1);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def post_init_");
    String _lowerCase_1 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append("(sender, instance):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("@receiver(pre_save, sender=");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def pre_save_");
    String _lowerCase_2 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_2);
    _builder.append("(sender, instance,raw, using, update_fields):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(post_save, sender=");
    String _name_3 = entity.getName();
    _builder.append(_name_3);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def post_save_");
    String _lowerCase_3 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_3);
    _builder.append("(sender, instance, created, raw, using, update_fields):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(m2m_changed, sender=");
    String _name_4 = entity.getName();
    _builder.append(_name_4);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def m2m_changed_");
    String _lowerCase_4 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_4);
    _builder.append("(sender, instance, action, reverse, model, pk_set, using):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass\t");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(pre_delete, sender=");
    String _name_5 = entity.getName();
    _builder.append(_name_5);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def delete_pre_");
    String _lowerCase_5 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_5);
    _builder.append("(sender, instance, using):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@receiver(post_delete, sender=");
    String _name_6 = entity.getName();
    _builder.append(_name_6);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("def delete_post_");
    String _lowerCase_6 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_6);
    _builder.append("(sender, instance, using):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("pass");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
}

package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator;

import com.google.common.collect.Iterables;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class TestDGenerator extends AbstractGenerator {
  private String PATH_LIB_ENTITY = "solution/apps/";

  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<Module> _filter = Iterables.<Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Module.class);
    for (final Module m : _filter) {
      String _lowerCase = m.getName().toLowerCase();
      String _plus = (this.PATH_LIB_ENTITY + _lowerCase);
      String _plus_1 = (_plus + "/tests.py");
      fsa.generateFile(_plus_1, this.createTests(m));
    }
  }

  public CharSequence createTests(final Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.test import TestCase");
    _builder.newLine();
    return _builder;
  }
}

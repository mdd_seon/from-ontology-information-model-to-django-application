package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.DAO;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Description;
import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class DAOGenerator {
  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# -*- coding: utf-8 -*-");
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, DAO.class);
          if (_equals) {
            final DAO dao = ((DAO) e);
            _builder.newLineIfNotEmpty();
            CharSequence _create = this.create(dao);
            _builder.append(_create);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence create(final DAO dao) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = dao.getName();
    _builder.append(_name);
    _builder.append("():");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("\"\"\"");
    {
      Description _description = dao.getDescription();
      boolean _tripleNotEquals = (_description != null);
      if (_tripleNotEquals) {
        String _textfield = dao.getDescription().getTextfield();
        _builder.append(_textfield, "\t");
      }
    }
    _builder.append("\"\"\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def __init__():");
    _builder.newLine();
    {
      EList<Attribute> _attributes = dao.getAttributes();
      for(final Attribute attribute : _attributes) {
        _builder.append("\t");
        _builder.append("self.");
        String _name_1 = attribute.getName();
        _builder.append(_name_1, "\t");
        _builder.append(" = None");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
}

package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import com.google.common.collect.Iterables;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class BDDGenerator extends AbstractGenerator {
  private String PATH = "solution/";

  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    fsa.generateFile((this.PATH + "features/steps/README.md"), "");
    Iterable<Entity> _filter = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
    for (final Entity entity : _filter) {
      String _name = entity.getName();
      String _plus = ((this.PATH + "features/") + _name);
      String _plus_1 = (_plus + ".feature");
      fsa.generateFile(_plus_1, this.create_feature_file(entity));
    }
  }

  private CharSequence create_feature_file(final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("Feature: Manager to ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("Scenarios to manager an instance of ");
    String _name_1 = entity.getName();
    _builder.append(_name_1);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("Background:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("Give I am Authenticated User");
    _builder.newLine();
    _builder.newLine();
    _builder.append("Scenario Outline: Manager an instance of ");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append(" ");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("When I want to \"<action>\" a ");
    String _name_3 = entity.getName();
    _builder.append(_name_3, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("Then the system respoonde \"<response>\" with \"<type\"");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("Examples:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|action |type   |response|");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|Create |success|201     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|Create |fail   |400     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|Delete |success|204     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|Delete |fail   |404     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|ReadOne|success|200     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|ReadOne|fail   |404     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|ReadAll|success|200     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|ReadAll|fail   |400     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|Update |success|200     |");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("|Update |fail   |400     |");
    _builder.newLine();
    return _builder;
  }
}

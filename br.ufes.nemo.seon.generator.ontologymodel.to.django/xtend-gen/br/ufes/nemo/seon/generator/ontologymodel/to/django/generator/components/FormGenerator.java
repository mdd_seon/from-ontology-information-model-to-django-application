package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class FormGenerator {
  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module, final Entity entity) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django import forms");
    _builder.newLine();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("from django.utils.translation import ugettext_lazy as _");
    _builder.newLine();
    _builder.newLine();
    CharSequence _createForm = this.createForm(entity);
    _builder.append(_createForm);
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public CharSequence create(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django import forms");
    _builder.newLine();
    _builder.append("from .models import *");
    _builder.newLine();
    _builder.append("from django.utils.translation import ugettext_lazy as _\t");
    _builder.newLine();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createForm = this.createForm(entity);
            _builder.append(_createForm);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createForm(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _name = e.getName();
    _builder.append(_name);
    _builder.append("Form(forms.ModelForm):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("class Meta:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("model =");
    String _name_1 = e.getName();
    _builder.append(_name_1, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("fields = [");
    _builder.newLine();
    {
      EList<Attribute> _attributes = e.getAttributes();
      for(final Attribute attribute : _attributes) {
        _builder.append("\t\t");
        _builder.append("\'");
        String _name_2 = attribute.getName();
        _builder.append(_name_2, "\t\t");
        _builder.append("\',\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("] ");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("labels = {");
    _builder.newLine();
    {
      EList<Attribute> _attributes_1 = e.getAttributes();
      for(final Attribute attribute_1 : _attributes_1) {
        _builder.append("\t\t");
        _builder.append("\'");
        String _name_3 = attribute_1.getName();
        _builder.append(_name_3, "\t\t");
        _builder.append("\': _(\'");
        {
          String _fullName = attribute_1.getFullName();
          boolean _tripleNotEquals = (_fullName != null);
          if (_tripleNotEquals) {
            String _fullName_1 = attribute_1.getFullName();
            _builder.append(_fullName_1, "\t\t");
          } else {
            String _firstLower = StringExtensions.toFirstLower(attribute_1.getName());
            _builder.append(_firstLower, "\t\t");
          }
        }
        _builder.append("\'),\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("widgets = {}");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
}

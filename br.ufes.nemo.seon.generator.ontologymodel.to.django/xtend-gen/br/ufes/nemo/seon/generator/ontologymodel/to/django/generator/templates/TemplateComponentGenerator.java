package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.templates;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class TemplateComponentGenerator extends AbstractGenerator {
  private String PATH_LIB_ENTITY = "solution/templates";

  @Inject
  private TemplateComponent templateGenerator;

  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    for (final Configuration configuration : _filter) {
      {
        fsa.generateFile((this.PATH_LIB_ENTITY + "/base.html"), this.templateGenerator.createBase());
        fsa.generateFile((this.PATH_LIB_ENTITY + "/components/menu/menu_lateral.html"), this.templateGenerator.create_menu_lateral(resource));
        fsa.generateFile((this.PATH_LIB_ENTITY + "/components/menu/menu_topo.html"), this.templateGenerator.create_menu_top());
      }
    }
  }
}

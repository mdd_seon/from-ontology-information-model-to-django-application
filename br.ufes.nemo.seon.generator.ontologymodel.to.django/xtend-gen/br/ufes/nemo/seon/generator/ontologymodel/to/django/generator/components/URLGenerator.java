package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.AbstractElement;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity;
import com.google.common.base.Objects;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class URLGenerator {
  public CharSequence createURLWeb(final Entity entity, final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.urls import path, register_converter");
    _builder.newLine();
    _builder.append("from .web_views import *");
    _builder.newLine();
    _builder.append("from .utils import *");
    _builder.newLine();
    _builder.append("register_converter(HashIdConverter, \"hashid\")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("urlpatterns = [");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _createURLWebAux = this.createURLWebAux(entity, module);
    _builder.append(_createURLWebAux, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("]");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createURLWebAux(final Entity entity, final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("#Web");
    _builder.newLine();
    _builder.append("path(\'");
    String _lowerCase = module.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("/");
    String _lowerCase_1 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append("/\',");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append("ListView.as_view(), name=\'");
    String _lowerCase_2 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_2);
    _builder.append("-web-list\'),");
    _builder.newLineIfNotEmpty();
    _builder.append("path(\'");
    String _lowerCase_3 = module.getName().toLowerCase();
    _builder.append(_lowerCase_3);
    _builder.append("/");
    String _lowerCase_4 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_4);
    _builder.append("/add\',");
    String _name_1 = entity.getName();
    _builder.append(_name_1);
    _builder.append("CreateView.as_view(), name=\'");
    String _lowerCase_5 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_5);
    _builder.append("-web-create\'),");
    _builder.newLineIfNotEmpty();
    _builder.append("path(\'");
    String _lowerCase_6 = module.getName().toLowerCase();
    _builder.append(_lowerCase_6);
    _builder.append("/");
    String _lowerCase_7 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_7);
    _builder.append("/<hashid:pk>\',");
    String _name_2 = entity.getName();
    _builder.append(_name_2);
    _builder.append("UpdateView.as_view(), name=\'");
    String _lowerCase_8 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_8);
    _builder.append("-web-detail\'),");
    _builder.newLineIfNotEmpty();
    _builder.append("path(\'");
    String _lowerCase_9 = module.getName().toLowerCase();
    _builder.append(_lowerCase_9);
    _builder.append("/");
    String _lowerCase_10 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_10);
    _builder.append("/<hashid:pk>/delete\',");
    String _name_3 = entity.getName();
    _builder.append(_name_3);
    _builder.append("DeleteView.as_view(), name=\'");
    String _lowerCase_11 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_11);
    _builder.append("-web-delete\'),\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createURLWeb(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.urls import path, register_converter");
    _builder.newLine();
    _builder.append("from .web_views import *");
    _builder.newLine();
    _builder.append("from .utils import *");
    _builder.newLine();
    _builder.append("register_converter(HashIdConverter, \"hashid\")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("urlpatterns = [");
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createURLWebAux = this.createURLWebAux(entity, module);
            _builder.append(_createURLWebAux);
            _builder.append("\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("]");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createURLAPI(final Entity entity, final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from django.urls import path, register_converter");
    _builder.newLine();
    _builder.append("from .api_views import *");
    _builder.newLine();
    _builder.append("from .utils import *");
    _builder.newLine();
    _builder.append("register_converter(HashIdConverter, \"hashid\")");
    _builder.newLine();
    _builder.newLine();
    _builder.append("urlpatterns = [");
    _builder.newLine();
    _builder.append("\t");
    CharSequence _createURLAPIAux = this.createURLAPIAux(entity, module);
    _builder.append(_createURLAPIAux, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("]");
    _builder.newLine();
    return _builder;
  }

  public CharSequence createURLAPIAux(final Entity entity, final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("router.register(r\'");
    String _lowerCase = entity.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("\', ");
    String _name = entity.getName();
    _builder.append(_name);
    _builder.append("ViewSet, basename=\'");
    String _lowerCase_1 = entity.getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append("\')");
    _builder.newLineIfNotEmpty();
    return _builder;
  }

  public CharSequence createModelImport(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            _builder.append("\t");
            String _name = entity.getName();
            _builder.append(_name, "\t");
            _builder.append("ViewSet,");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }

  public CharSequence createURLAPI(final br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module module) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("from rest_framework import routers");
    _builder.newLine();
    _builder.append("from django.urls import include, path");
    _builder.newLine();
    _builder.append("from .api_views import (");
    CharSequence _createModelImport = this.createModelImport(module);
    _builder.append(_createModelImport);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("router = routers.DefaultRouter()");
    _builder.newLine();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = module.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _createURLAPIAux = this.createURLAPIAux(entity, module);
            _builder.append(_createURLAPIAux);
            _builder.append("\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.newLine();
    _builder.append("urlpatterns = [");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("path(\'");
    String _lowerCase = module.getName().toLowerCase();
    _builder.append(_lowerCase, "    ");
    _builder.append("/\', include(router.urls)),");
    _builder.newLineIfNotEmpty();
    _builder.append("]");
    _builder.newLine();
    return _builder;
  }
}

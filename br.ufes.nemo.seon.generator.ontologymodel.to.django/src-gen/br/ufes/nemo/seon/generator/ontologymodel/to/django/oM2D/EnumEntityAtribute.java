/**
 * generated by Xtext 2.27.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Entity Atribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute#getDescription <em>Description</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute#getName <em>Name</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getEnumEntityAtribute()
 * @model
 * @generated
 */
public interface EnumEntityAtribute extends EObject
{
  /**
   * Returns the value of the '<em><b>Description</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' containment reference.
   * @see #setDescription(Description)
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getEnumEntityAtribute_Description()
   * @model containment="true"
   * @generated
   */
  Description getDescription();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute#getDescription <em>Description</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' containment reference.
   * @see #getDescription()
   * @generated
   */
  void setDescription(Description value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getEnumEntityAtribute_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' reference.
   * @see #setType(EnumX)
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getEnumEntityAtribute_Type()
   * @model
   * @generated
   */
  EnumX getType();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute#getType <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' reference.
   * @see #getType()
   * @generated
   */
  void setType(EnumX value);

} // EnumEntityAtribute

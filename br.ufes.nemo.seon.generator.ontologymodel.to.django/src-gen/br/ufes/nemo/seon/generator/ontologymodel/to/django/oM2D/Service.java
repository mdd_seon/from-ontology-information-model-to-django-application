/**
 * generated by Xtext 2.27.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getVerb <em>Verb</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getName <em>Name</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getParams <em>Params</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getService()
 * @model
 * @generated
 */
public interface Service extends EObject
{
  /**
   * Returns the value of the '<em><b>Verb</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Verb</em>' attribute.
   * @see #setVerb(String)
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getService_Verb()
   * @model
   * @generated
   */
  String getVerb();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getVerb <em>Verb</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Verb</em>' attribute.
   * @see #getVerb()
   * @generated
   */
  void setVerb(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getService_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Params</b></em>' containment reference list.
   * The list contents are of type {@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Params}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Params</em>' containment reference list.
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getService_Params()
   * @model containment="true"
   * @generated
   */
  EList<Params> getParams();

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(Params)
   * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getService_Type()
   * @model containment="true"
   * @generated
   */
  Params getType();

  /**
   * Sets the value of the '{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Service#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(Params value);

} // Service

package br.ufes.nemo.seon.generator.ontologymodel.to.django.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.services.OM2DGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalOM2DParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_BOOLEAN_VALUE", "RULE_DATATYPE", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "'{'", "'}'", "'author:'", "'author_email:'", "'repository:'", "'software:'", "'about:'", "'#'", "'module'", "'imported_name'", "':'", "'actor'", "'extends'", "'usecase'", "'fullName'", "'manage'", "'perfomed'", "'by'", "'model'", "'.'", "'import'", "'.*'", "'enum'", "','", "'dao'", "'entity'", "'webservice'", "'Get'", "'Post'", "'Delete'", "'Put'", "'service'", "'('", "')'", "'NotRequired'", "'uses'", "'OneToOne'", "'ManyToMany'", "'OneToMany'", "'ManyToOne'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int RULE_ID=5;
    public static final int RULE_DATATYPE=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=8;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_BOOLEAN_VALUE=6;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalOM2DParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalOM2DParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalOM2DParser.tokenNames; }
    public String getGrammarFileName() { return "InternalOM2D.g"; }



     	private OM2DGrammarAccess grammarAccess;

        public InternalOM2DParser(TokenStream input, OM2DGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Application";
       	}

       	@Override
       	protected OM2DGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleApplication"
    // InternalOM2D.g:64:1: entryRuleApplication returns [EObject current=null] : iv_ruleApplication= ruleApplication EOF ;
    public final EObject entryRuleApplication() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleApplication = null;


        try {
            // InternalOM2D.g:64:52: (iv_ruleApplication= ruleApplication EOF )
            // InternalOM2D.g:65:2: iv_ruleApplication= ruleApplication EOF
            {
             newCompositeNode(grammarAccess.getApplicationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleApplication=ruleApplication();

            state._fsp--;

             current =iv_ruleApplication; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleApplication"


    // $ANTLR start "ruleApplication"
    // InternalOM2D.g:71:1: ruleApplication returns [EObject current=null] : ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* ) ;
    public final EObject ruleApplication() throws RecognitionException {
        EObject current = null;

        EObject lv_configuration_0_0 = null;

        EObject lv_abstractElements_1_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:77:2: ( ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* ) )
            // InternalOM2D.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* )
            {
            // InternalOM2D.g:78:2: ( ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )* )
            // InternalOM2D.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )? ( (lv_abstractElements_1_0= ruleAbstractElement ) )*
            {
            // InternalOM2D.g:79:3: ( (lv_configuration_0_0= ruleConfiguration ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalOM2D.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    {
                    // InternalOM2D.g:80:4: (lv_configuration_0_0= ruleConfiguration )
                    // InternalOM2D.g:81:5: lv_configuration_0_0= ruleConfiguration
                    {

                    					newCompositeNode(grammarAccess.getApplicationAccess().getConfigurationConfigurationParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_configuration_0_0=ruleConfiguration();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getApplicationRule());
                    					}
                    					set(
                    						current,
                    						"configuration",
                    						lv_configuration_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Configuration");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOM2D.g:98:3: ( (lv_abstractElements_1_0= ruleAbstractElement ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=21 && LA2_0<=22)||LA2_0==25||LA2_0==27||LA2_0==34||LA2_0==36||(LA2_0>=38 && LA2_0<=40)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalOM2D.g:99:4: (lv_abstractElements_1_0= ruleAbstractElement )
            	    {
            	    // InternalOM2D.g:99:4: (lv_abstractElements_1_0= ruleAbstractElement )
            	    // InternalOM2D.g:100:5: lv_abstractElements_1_0= ruleAbstractElement
            	    {

            	    					newCompositeNode(grammarAccess.getApplicationAccess().getAbstractElementsAbstractElementParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_abstractElements_1_0=ruleAbstractElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getApplicationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"abstractElements",
            	    						lv_abstractElements_1_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.AbstractElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleApplication"


    // $ANTLR start "entryRuleConfiguration"
    // InternalOM2D.g:121:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalOM2D.g:121:54: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalOM2D.g:122:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalOM2D.g:128:1: ruleConfiguration returns [EObject current=null] : (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_about_2_0= ruleAbout ) ) ( (lv_software_3_0= ruleSoftware ) ) ( (lv_author_4_0= ruleAuthor ) ) ( (lv_author_email_5_0= ruleAuthor_Email ) ) ( (lv_repository_6_0= ruleRepository ) ) otherlv_7= '}' ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_7=null;
        EObject lv_about_2_0 = null;

        EObject lv_software_3_0 = null;

        EObject lv_author_4_0 = null;

        EObject lv_author_email_5_0 = null;

        EObject lv_repository_6_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:134:2: ( (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_about_2_0= ruleAbout ) ) ( (lv_software_3_0= ruleSoftware ) ) ( (lv_author_4_0= ruleAuthor ) ) ( (lv_author_email_5_0= ruleAuthor_Email ) ) ( (lv_repository_6_0= ruleRepository ) ) otherlv_7= '}' ) )
            // InternalOM2D.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_about_2_0= ruleAbout ) ) ( (lv_software_3_0= ruleSoftware ) ) ( (lv_author_4_0= ruleAuthor ) ) ( (lv_author_email_5_0= ruleAuthor_Email ) ) ( (lv_repository_6_0= ruleRepository ) ) otherlv_7= '}' )
            {
            // InternalOM2D.g:135:2: (otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_about_2_0= ruleAbout ) ) ( (lv_software_3_0= ruleSoftware ) ) ( (lv_author_4_0= ruleAuthor ) ) ( (lv_author_email_5_0= ruleAuthor_Email ) ) ( (lv_repository_6_0= ruleRepository ) ) otherlv_7= '}' )
            // InternalOM2D.g:136:3: otherlv_0= 'Configuration' otherlv_1= '{' ( (lv_about_2_0= ruleAbout ) ) ( (lv_software_3_0= ruleSoftware ) ) ( (lv_author_4_0= ruleAuthor ) ) ( (lv_author_email_5_0= ruleAuthor_Email ) ) ( (lv_repository_6_0= ruleRepository ) ) otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getConfigurationAccess().getConfigurationKeyword_0());
            		
            otherlv_1=(Token)match(input,14,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalOM2D.g:144:3: ( (lv_about_2_0= ruleAbout ) )
            // InternalOM2D.g:145:4: (lv_about_2_0= ruleAbout )
            {
            // InternalOM2D.g:145:4: (lv_about_2_0= ruleAbout )
            // InternalOM2D.g:146:5: lv_about_2_0= ruleAbout
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_about_2_0=ruleAbout();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"about",
            						lv_about_2_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.About");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOM2D.g:163:3: ( (lv_software_3_0= ruleSoftware ) )
            // InternalOM2D.g:164:4: (lv_software_3_0= ruleSoftware )
            {
            // InternalOM2D.g:164:4: (lv_software_3_0= ruleSoftware )
            // InternalOM2D.g:165:5: lv_software_3_0= ruleSoftware
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_7);
            lv_software_3_0=ruleSoftware();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"software",
            						lv_software_3_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Software");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOM2D.g:182:3: ( (lv_author_4_0= ruleAuthor ) )
            // InternalOM2D.g:183:4: (lv_author_4_0= ruleAuthor )
            {
            // InternalOM2D.g:183:4: (lv_author_4_0= ruleAuthor )
            // InternalOM2D.g:184:5: lv_author_4_0= ruleAuthor
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_author_4_0=ruleAuthor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author",
            						lv_author_4_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Author");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOM2D.g:201:3: ( (lv_author_email_5_0= ruleAuthor_Email ) )
            // InternalOM2D.g:202:4: (lv_author_email_5_0= ruleAuthor_Email )
            {
            // InternalOM2D.g:202:4: (lv_author_email_5_0= ruleAuthor_Email )
            // InternalOM2D.g:203:5: lv_author_email_5_0= ruleAuthor_Email
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_9);
            lv_author_email_5_0=ruleAuthor_Email();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"author_email",
            						lv_author_email_5_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Author_Email");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOM2D.g:220:3: ( (lv_repository_6_0= ruleRepository ) )
            // InternalOM2D.g:221:4: (lv_repository_6_0= ruleRepository )
            {
            // InternalOM2D.g:221:4: (lv_repository_6_0= ruleRepository )
            // InternalOM2D.g:222:5: lv_repository_6_0= ruleRepository
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_10);
            lv_repository_6_0=ruleRepository();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"repository",
            						lv_repository_6_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Repository");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getConfigurationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleAuthor"
    // InternalOM2D.g:247:1: entryRuleAuthor returns [EObject current=null] : iv_ruleAuthor= ruleAuthor EOF ;
    public final EObject entryRuleAuthor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor = null;


        try {
            // InternalOM2D.g:247:47: (iv_ruleAuthor= ruleAuthor EOF )
            // InternalOM2D.g:248:2: iv_ruleAuthor= ruleAuthor EOF
            {
             newCompositeNode(grammarAccess.getAuthorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor=ruleAuthor();

            state._fsp--;

             current =iv_ruleAuthor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor"


    // $ANTLR start "ruleAuthor"
    // InternalOM2D.g:254:1: ruleAuthor returns [EObject current=null] : (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:260:2: ( (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOM2D.g:261:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOM2D.g:261:2: (otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOM2D.g:262:3: otherlv_0= 'author:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthorAccess().getAuthorKeyword_0());
            		
            // InternalOM2D.g:266:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOM2D.g:267:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOM2D.g:267:4: (lv_name_1_0= RULE_STRING )
            // InternalOM2D.g:268:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor"


    // $ANTLR start "entryRuleAuthor_Email"
    // InternalOM2D.g:288:1: entryRuleAuthor_Email returns [EObject current=null] : iv_ruleAuthor_Email= ruleAuthor_Email EOF ;
    public final EObject entryRuleAuthor_Email() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAuthor_Email = null;


        try {
            // InternalOM2D.g:288:53: (iv_ruleAuthor_Email= ruleAuthor_Email EOF )
            // InternalOM2D.g:289:2: iv_ruleAuthor_Email= ruleAuthor_Email EOF
            {
             newCompositeNode(grammarAccess.getAuthor_EmailRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAuthor_Email=ruleAuthor_Email();

            state._fsp--;

             current =iv_ruleAuthor_Email; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAuthor_Email"


    // $ANTLR start "ruleAuthor_Email"
    // InternalOM2D.g:295:1: ruleAuthor_Email returns [EObject current=null] : (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAuthor_Email() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:301:2: ( (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOM2D.g:302:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOM2D.g:302:2: (otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOM2D.g:303:3: otherlv_0= 'author_email:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getAuthor_EmailAccess().getAuthor_emailKeyword_0());
            		
            // InternalOM2D.g:307:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOM2D.g:308:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOM2D.g:308:4: (lv_name_1_0= RULE_STRING )
            // InternalOM2D.g:309:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAuthor_EmailRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAuthor_Email"


    // $ANTLR start "entryRuleRepository"
    // InternalOM2D.g:329:1: entryRuleRepository returns [EObject current=null] : iv_ruleRepository= ruleRepository EOF ;
    public final EObject entryRuleRepository() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepository = null;


        try {
            // InternalOM2D.g:329:51: (iv_ruleRepository= ruleRepository EOF )
            // InternalOM2D.g:330:2: iv_ruleRepository= ruleRepository EOF
            {
             newCompositeNode(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRepository=ruleRepository();

            state._fsp--;

             current =iv_ruleRepository; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalOM2D.g:336:1: ruleRepository returns [EObject current=null] : (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleRepository() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:342:2: ( (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOM2D.g:343:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOM2D.g:343:2: (otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOM2D.g:344:3: otherlv_0= 'repository:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getRepositoryAccess().getRepositoryKeyword_0());
            		
            // InternalOM2D.g:348:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOM2D.g:349:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOM2D.g:349:4: (lv_name_1_0= RULE_STRING )
            // InternalOM2D.g:350:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRepositoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleSoftware"
    // InternalOM2D.g:370:1: entryRuleSoftware returns [EObject current=null] : iv_ruleSoftware= ruleSoftware EOF ;
    public final EObject entryRuleSoftware() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSoftware = null;


        try {
            // InternalOM2D.g:370:49: (iv_ruleSoftware= ruleSoftware EOF )
            // InternalOM2D.g:371:2: iv_ruleSoftware= ruleSoftware EOF
            {
             newCompositeNode(grammarAccess.getSoftwareRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSoftware=ruleSoftware();

            state._fsp--;

             current =iv_ruleSoftware; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSoftware"


    // $ANTLR start "ruleSoftware"
    // InternalOM2D.g:377:1: ruleSoftware returns [EObject current=null] : (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleSoftware() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:383:2: ( (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOM2D.g:384:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOM2D.g:384:2: (otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOM2D.g:385:3: otherlv_0= 'software:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getSoftwareAccess().getSoftwareKeyword_0());
            		
            // InternalOM2D.g:389:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOM2D.g:390:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOM2D.g:390:4: (lv_name_1_0= RULE_STRING )
            // InternalOM2D.g:391:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSoftwareRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSoftware"


    // $ANTLR start "entryRuleAbout"
    // InternalOM2D.g:411:1: entryRuleAbout returns [EObject current=null] : iv_ruleAbout= ruleAbout EOF ;
    public final EObject entryRuleAbout() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbout = null;


        try {
            // InternalOM2D.g:411:46: (iv_ruleAbout= ruleAbout EOF )
            // InternalOM2D.g:412:2: iv_ruleAbout= ruleAbout EOF
            {
             newCompositeNode(grammarAccess.getAboutRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbout=ruleAbout();

            state._fsp--;

             current =iv_ruleAbout; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbout"


    // $ANTLR start "ruleAbout"
    // InternalOM2D.g:418:1: ruleAbout returns [EObject current=null] : (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleAbout() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:424:2: ( (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) ) )
            // InternalOM2D.g:425:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            {
            // InternalOM2D.g:425:2: (otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) ) )
            // InternalOM2D.g:426:3: otherlv_0= 'about:' ( (lv_name_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getAboutAccess().getAboutKeyword_0());
            		
            // InternalOM2D.g:430:3: ( (lv_name_1_0= RULE_STRING ) )
            // InternalOM2D.g:431:4: (lv_name_1_0= RULE_STRING )
            {
            // InternalOM2D.g:431:4: (lv_name_1_0= RULE_STRING )
            // InternalOM2D.g:432:5: lv_name_1_0= RULE_STRING
            {
            lv_name_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAboutRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbout"


    // $ANTLR start "entryRuleDescription"
    // InternalOM2D.g:452:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // InternalOM2D.g:452:52: (iv_ruleDescription= ruleDescription EOF )
            // InternalOM2D.g:453:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // InternalOM2D.g:459:1: ruleDescription returns [EObject current=null] : (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_textfield_1_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:465:2: ( (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) ) )
            // InternalOM2D.g:466:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            {
            // InternalOM2D.g:466:2: (otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) ) )
            // InternalOM2D.g:467:3: otherlv_0= '#' ( (lv_textfield_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getDescriptionAccess().getNumberSignKeyword_0());
            		
            // InternalOM2D.g:471:3: ( (lv_textfield_1_0= RULE_STRING ) )
            // InternalOM2D.g:472:4: (lv_textfield_1_0= RULE_STRING )
            {
            // InternalOM2D.g:472:4: (lv_textfield_1_0= RULE_STRING )
            // InternalOM2D.g:473:5: lv_textfield_1_0= RULE_STRING
            {
            lv_textfield_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_textfield_1_0, grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDescriptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"textfield",
            						lv_textfield_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleModule"
    // InternalOM2D.g:493:1: entryRuleModule returns [EObject current=null] : iv_ruleModule= ruleModule EOF ;
    public final EObject entryRuleModule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModule = null;


        try {
            // InternalOM2D.g:493:47: (iv_ruleModule= ruleModule EOF )
            // InternalOM2D.g:494:2: iv_ruleModule= ruleModule EOF
            {
             newCompositeNode(grammarAccess.getModuleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModule=ruleModule();

            state._fsp--;

             current =iv_ruleModule; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModule"


    // $ANTLR start "ruleModule"
    // InternalOM2D.g:500:1: ruleModule returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_moduleimported_4_0= ruleModuleImport ) )? ( (lv_elements_5_0= ruleAbstractElement ) )* otherlv_6= '}' ) ;
    public final EObject ruleModule() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        EObject lv_description_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_moduleimported_4_0 = null;

        EObject lv_elements_5_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:506:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_moduleimported_4_0= ruleModuleImport ) )? ( (lv_elements_5_0= ruleAbstractElement ) )* otherlv_6= '}' ) )
            // InternalOM2D.g:507:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_moduleimported_4_0= ruleModuleImport ) )? ( (lv_elements_5_0= ruleAbstractElement ) )* otherlv_6= '}' )
            {
            // InternalOM2D.g:507:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_moduleimported_4_0= ruleModuleImport ) )? ( (lv_elements_5_0= ruleAbstractElement ) )* otherlv_6= '}' )
            // InternalOM2D.g:508:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'module' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' ( (lv_moduleimported_4_0= ruleModuleImport ) )? ( (lv_elements_5_0= ruleAbstractElement ) )* otherlv_6= '}'
            {
            // InternalOM2D.g:508:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==21) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalOM2D.g:509:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:509:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:510:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getModuleAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_12);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getModuleRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,22,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getModuleAccess().getModuleKeyword_1());
            		
            // InternalOM2D.g:531:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalOM2D.g:532:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalOM2D.g:532:4: (lv_name_2_0= ruleQualifiedName )
            // InternalOM2D.g:533:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getModuleAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModuleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_14); 

            			newLeafNode(otherlv_3, grammarAccess.getModuleAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalOM2D.g:554:3: ( (lv_moduleimported_4_0= ruleModuleImport ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==23) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalOM2D.g:555:4: (lv_moduleimported_4_0= ruleModuleImport )
                    {
                    // InternalOM2D.g:555:4: (lv_moduleimported_4_0= ruleModuleImport )
                    // InternalOM2D.g:556:5: lv_moduleimported_4_0= ruleModuleImport
                    {

                    					newCompositeNode(grammarAccess.getModuleAccess().getModuleimportedModuleImportParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_15);
                    lv_moduleimported_4_0=ruleModuleImport();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getModuleRule());
                    					}
                    					set(
                    						current,
                    						"moduleimported",
                    						lv_moduleimported_4_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.ModuleImport");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOM2D.g:573:3: ( (lv_elements_5_0= ruleAbstractElement ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=21 && LA5_0<=22)||LA5_0==25||LA5_0==27||LA5_0==34||LA5_0==36||(LA5_0>=38 && LA5_0<=40)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalOM2D.g:574:4: (lv_elements_5_0= ruleAbstractElement )
            	    {
            	    // InternalOM2D.g:574:4: (lv_elements_5_0= ruleAbstractElement )
            	    // InternalOM2D.g:575:5: lv_elements_5_0= ruleAbstractElement
            	    {

            	    					newCompositeNode(grammarAccess.getModuleAccess().getElementsAbstractElementParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_15);
            	    lv_elements_5_0=ruleAbstractElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModuleRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_5_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.AbstractElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_6=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getModuleAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModule"


    // $ANTLR start "entryRuleModuleImport"
    // InternalOM2D.g:600:1: entryRuleModuleImport returns [EObject current=null] : iv_ruleModuleImport= ruleModuleImport EOF ;
    public final EObject entryRuleModuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModuleImport = null;


        try {
            // InternalOM2D.g:600:53: (iv_ruleModuleImport= ruleModuleImport EOF )
            // InternalOM2D.g:601:2: iv_ruleModuleImport= ruleModuleImport EOF
            {
             newCompositeNode(grammarAccess.getModuleImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModuleImport=ruleModuleImport();

            state._fsp--;

             current =iv_ruleModuleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModuleImport"


    // $ANTLR start "ruleModuleImport"
    // InternalOM2D.g:607:1: ruleModuleImport returns [EObject current=null] : (otherlv_0= 'imported_name' otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) ) ;
    public final EObject ruleModuleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalOM2D.g:613:2: ( (otherlv_0= 'imported_name' otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) ) )
            // InternalOM2D.g:614:2: (otherlv_0= 'imported_name' otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) )
            {
            // InternalOM2D.g:614:2: (otherlv_0= 'imported_name' otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) ) )
            // InternalOM2D.g:615:3: otherlv_0= 'imported_name' otherlv_1= ':' ( (lv_name_2_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getModuleImportAccess().getImported_nameKeyword_0());
            		
            otherlv_1=(Token)match(input,24,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getModuleImportAccess().getColonKeyword_1());
            		
            // InternalOM2D.g:623:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOM2D.g:624:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOM2D.g:624:4: (lv_name_2_0= RULE_ID )
            // InternalOM2D.g:625:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_2_0, grammarAccess.getModuleImportAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getModuleImportRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModuleImport"


    // $ANTLR start "entryRuleAbstractElement"
    // InternalOM2D.g:645:1: entryRuleAbstractElement returns [EObject current=null] : iv_ruleAbstractElement= ruleAbstractElement EOF ;
    public final EObject entryRuleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractElement = null;


        try {
            // InternalOM2D.g:645:56: (iv_ruleAbstractElement= ruleAbstractElement EOF )
            // InternalOM2D.g:646:2: iv_ruleAbstractElement= ruleAbstractElement EOF
            {
             newCompositeNode(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractElement=ruleAbstractElement();

            state._fsp--;

             current =iv_ruleAbstractElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // InternalOM2D.g:652:1: ruleAbstractElement returns [EObject current=null] : (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_EnumX_2= ruleEnumX | this_DAO_3= ruleDAO | this_WebService_4= ruleWebService | this_Import_5= ruleImport | this_UseCase_6= ruleUseCase | this_Actor_7= ruleActor ) ;
    public final EObject ruleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject this_Module_0 = null;

        EObject this_Entity_1 = null;

        EObject this_EnumX_2 = null;

        EObject this_DAO_3 = null;

        EObject this_WebService_4 = null;

        EObject this_Import_5 = null;

        EObject this_UseCase_6 = null;

        EObject this_Actor_7 = null;



        	enterRule();

        try {
            // InternalOM2D.g:658:2: ( (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_EnumX_2= ruleEnumX | this_DAO_3= ruleDAO | this_WebService_4= ruleWebService | this_Import_5= ruleImport | this_UseCase_6= ruleUseCase | this_Actor_7= ruleActor ) )
            // InternalOM2D.g:659:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_EnumX_2= ruleEnumX | this_DAO_3= ruleDAO | this_WebService_4= ruleWebService | this_Import_5= ruleImport | this_UseCase_6= ruleUseCase | this_Actor_7= ruleActor )
            {
            // InternalOM2D.g:659:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_EnumX_2= ruleEnumX | this_DAO_3= ruleDAO | this_WebService_4= ruleWebService | this_Import_5= ruleImport | this_UseCase_6= ruleUseCase | this_Actor_7= ruleActor )
            int alt6=8;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // InternalOM2D.g:660:3: this_Module_0= ruleModule
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getModuleParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Module_0=ruleModule();

                    state._fsp--;


                    			current = this_Module_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalOM2D.g:669:3: this_Entity_1= ruleEntity
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getEntityParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Entity_1=ruleEntity();

                    state._fsp--;


                    			current = this_Entity_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalOM2D.g:678:3: this_EnumX_2= ruleEnumX
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getEnumXParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_EnumX_2=ruleEnumX();

                    state._fsp--;


                    			current = this_EnumX_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalOM2D.g:687:3: this_DAO_3= ruleDAO
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getDAOParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_DAO_3=ruleDAO();

                    state._fsp--;


                    			current = this_DAO_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalOM2D.g:696:3: this_WebService_4= ruleWebService
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getWebServiceParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_WebService_4=ruleWebService();

                    state._fsp--;


                    			current = this_WebService_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalOM2D.g:705:3: this_Import_5= ruleImport
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getImportParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Import_5=ruleImport();

                    state._fsp--;


                    			current = this_Import_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalOM2D.g:714:3: this_UseCase_6= ruleUseCase
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getUseCaseParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_UseCase_6=ruleUseCase();

                    state._fsp--;


                    			current = this_UseCase_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalOM2D.g:723:3: this_Actor_7= ruleActor
                    {

                    			newCompositeNode(grammarAccess.getAbstractElementAccess().getActorParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_Actor_7=ruleActor();

                    state._fsp--;


                    			current = this_Actor_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleActor"
    // InternalOM2D.g:735:1: entryRuleActor returns [EObject current=null] : iv_ruleActor= ruleActor EOF ;
    public final EObject entryRuleActor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActor = null;


        try {
            // InternalOM2D.g:735:46: (iv_ruleActor= ruleActor EOF )
            // InternalOM2D.g:736:2: iv_ruleActor= ruleActor EOF
            {
             newCompositeNode(grammarAccess.getActorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActor=ruleActor();

            state._fsp--;

             current =iv_ruleActor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActor"


    // $ANTLR start "ruleActor"
    // InternalOM2D.g:742:1: ruleActor returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'actor' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? ) ;
    public final EObject ruleActor() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_description_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:748:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'actor' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? ) )
            // InternalOM2D.g:749:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'actor' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? )
            {
            // InternalOM2D.g:749:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'actor' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )? )
            // InternalOM2D.g:750:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'actor' ( (lv_name_2_0= ruleQualifiedName ) ) (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )?
            {
            // InternalOM2D.g:750:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalOM2D.g:751:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:751:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:752:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getActorAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_17);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getActorRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,25,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getActorAccess().getActorKeyword_1());
            		
            // InternalOM2D.g:773:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalOM2D.g:774:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalOM2D.g:774:4: (lv_name_2_0= ruleQualifiedName )
            // InternalOM2D.g:775:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getActorAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_18);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActorRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOM2D.g:792:3: (otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==26) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalOM2D.g:793:4: otherlv_3= 'extends' ( (otherlv_4= RULE_ID ) )
                    {
                    otherlv_3=(Token)match(input,26,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getActorAccess().getExtendsKeyword_3_0());
                    			
                    // InternalOM2D.g:797:4: ( (otherlv_4= RULE_ID ) )
                    // InternalOM2D.g:798:5: (otherlv_4= RULE_ID )
                    {
                    // InternalOM2D.g:798:5: (otherlv_4= RULE_ID )
                    // InternalOM2D.g:799:6: otherlv_4= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getActorRule());
                    						}
                    					
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_4, grammarAccess.getActorAccess().getSuperTypeActorCrossReference_3_1_0());
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActor"


    // $ANTLR start "entryRuleUseCase"
    // InternalOM2D.g:815:1: entryRuleUseCase returns [EObject current=null] : iv_ruleUseCase= ruleUseCase EOF ;
    public final EObject entryRuleUseCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUseCase = null;


        try {
            // InternalOM2D.g:815:48: (iv_ruleUseCase= ruleUseCase EOF )
            // InternalOM2D.g:816:2: iv_ruleUseCase= ruleUseCase EOF
            {
             newCompositeNode(grammarAccess.getUseCaseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUseCase=ruleUseCase();

            state._fsp--;

             current =iv_ruleUseCase; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUseCase"


    // $ANTLR start "ruleUseCase"
    // InternalOM2D.g:822:1: ruleUseCase returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'usecase' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) otherlv_7= 'manage' otherlv_8= ':' ( (lv_manage_9_0= RULE_BOOLEAN_VALUE ) ) ( (lv_perfomedby_10_0= rulePerfomedActor ) )+ ( (lv_model_11_0= ruleModelUseCase ) )+ otherlv_12= '}' ) ;
    public final EObject ruleUseCase() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_fullName_6_0=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_manage_9_0=null;
        Token otherlv_12=null;
        EObject lv_description_0_0 = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_perfomedby_10_0 = null;

        EObject lv_model_11_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:828:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'usecase' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) otherlv_7= 'manage' otherlv_8= ':' ( (lv_manage_9_0= RULE_BOOLEAN_VALUE ) ) ( (lv_perfomedby_10_0= rulePerfomedActor ) )+ ( (lv_model_11_0= ruleModelUseCase ) )+ otherlv_12= '}' ) )
            // InternalOM2D.g:829:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'usecase' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) otherlv_7= 'manage' otherlv_8= ':' ( (lv_manage_9_0= RULE_BOOLEAN_VALUE ) ) ( (lv_perfomedby_10_0= rulePerfomedActor ) )+ ( (lv_model_11_0= ruleModelUseCase ) )+ otherlv_12= '}' )
            {
            // InternalOM2D.g:829:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'usecase' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) otherlv_7= 'manage' otherlv_8= ':' ( (lv_manage_9_0= RULE_BOOLEAN_VALUE ) ) ( (lv_perfomedby_10_0= rulePerfomedActor ) )+ ( (lv_model_11_0= ruleModelUseCase ) )+ otherlv_12= '}' )
            // InternalOM2D.g:830:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'usecase' ( (lv_name_2_0= ruleQualifiedName ) ) otherlv_3= '{' otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) otherlv_7= 'manage' otherlv_8= ':' ( (lv_manage_9_0= RULE_BOOLEAN_VALUE ) ) ( (lv_perfomedby_10_0= rulePerfomedActor ) )+ ( (lv_model_11_0= ruleModelUseCase ) )+ otherlv_12= '}'
            {
            // InternalOM2D.g:830:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==21) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalOM2D.g:831:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:831:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:832:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getUseCaseAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_19);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getUseCaseRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,27,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getUseCaseAccess().getUsecaseKeyword_1());
            		
            // InternalOM2D.g:853:3: ( (lv_name_2_0= ruleQualifiedName ) )
            // InternalOM2D.g:854:4: (lv_name_2_0= ruleQualifiedName )
            {
            // InternalOM2D.g:854:4: (lv_name_2_0= ruleQualifiedName )
            // InternalOM2D.g:855:5: lv_name_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getUseCaseAccess().getNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUseCaseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_20); 

            			newLeafNode(otherlv_3, grammarAccess.getUseCaseAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,28,FOLLOW_16); 

            			newLeafNode(otherlv_4, grammarAccess.getUseCaseAccess().getFullNameKeyword_4());
            		
            otherlv_5=(Token)match(input,24,FOLLOW_11); 

            			newLeafNode(otherlv_5, grammarAccess.getUseCaseAccess().getColonKeyword_5());
            		
            // InternalOM2D.g:884:3: ( (lv_fullName_6_0= RULE_STRING ) )
            // InternalOM2D.g:885:4: (lv_fullName_6_0= RULE_STRING )
            {
            // InternalOM2D.g:885:4: (lv_fullName_6_0= RULE_STRING )
            // InternalOM2D.g:886:5: lv_fullName_6_0= RULE_STRING
            {
            lv_fullName_6_0=(Token)match(input,RULE_STRING,FOLLOW_21); 

            					newLeafNode(lv_fullName_6_0, grammarAccess.getUseCaseAccess().getFullNameSTRINGTerminalRuleCall_6_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUseCaseRule());
            					}
            					setWithLastConsumed(
            						current,
            						"fullName",
            						lv_fullName_6_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_7=(Token)match(input,29,FOLLOW_16); 

            			newLeafNode(otherlv_7, grammarAccess.getUseCaseAccess().getManageKeyword_7());
            		
            otherlv_8=(Token)match(input,24,FOLLOW_22); 

            			newLeafNode(otherlv_8, grammarAccess.getUseCaseAccess().getColonKeyword_8());
            		
            // InternalOM2D.g:910:3: ( (lv_manage_9_0= RULE_BOOLEAN_VALUE ) )
            // InternalOM2D.g:911:4: (lv_manage_9_0= RULE_BOOLEAN_VALUE )
            {
            // InternalOM2D.g:911:4: (lv_manage_9_0= RULE_BOOLEAN_VALUE )
            // InternalOM2D.g:912:5: lv_manage_9_0= RULE_BOOLEAN_VALUE
            {
            lv_manage_9_0=(Token)match(input,RULE_BOOLEAN_VALUE,FOLLOW_23); 

            					newLeafNode(lv_manage_9_0, grammarAccess.getUseCaseAccess().getManageBOOLEAN_VALUETerminalRuleCall_9_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUseCaseRule());
            					}
            					setWithLastConsumed(
            						current,
            						"manage",
            						lv_manage_9_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.BOOLEAN_VALUE");
            				

            }


            }

            // InternalOM2D.g:928:3: ( (lv_perfomedby_10_0= rulePerfomedActor ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==30) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalOM2D.g:929:4: (lv_perfomedby_10_0= rulePerfomedActor )
            	    {
            	    // InternalOM2D.g:929:4: (lv_perfomedby_10_0= rulePerfomedActor )
            	    // InternalOM2D.g:930:5: lv_perfomedby_10_0= rulePerfomedActor
            	    {

            	    					newCompositeNode(grammarAccess.getUseCaseAccess().getPerfomedbyPerfomedActorParserRuleCall_10_0());
            	    				
            	    pushFollow(FOLLOW_24);
            	    lv_perfomedby_10_0=rulePerfomedActor();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getUseCaseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"perfomedby",
            	    						lv_perfomedby_10_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.PerfomedActor");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);

            // InternalOM2D.g:947:3: ( (lv_model_11_0= ruleModelUseCase ) )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==32) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalOM2D.g:948:4: (lv_model_11_0= ruleModelUseCase )
            	    {
            	    // InternalOM2D.g:948:4: (lv_model_11_0= ruleModelUseCase )
            	    // InternalOM2D.g:949:5: lv_model_11_0= ruleModelUseCase
            	    {

            	    					newCompositeNode(grammarAccess.getUseCaseAccess().getModelModelUseCaseParserRuleCall_11_0());
            	    				
            	    pushFollow(FOLLOW_25);
            	    lv_model_11_0=ruleModelUseCase();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getUseCaseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"model",
            	    						lv_model_11_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.ModelUseCase");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);

            otherlv_12=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getUseCaseAccess().getRightCurlyBracketKeyword_12());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUseCase"


    // $ANTLR start "entryRulePerfomedActor"
    // InternalOM2D.g:974:1: entryRulePerfomedActor returns [EObject current=null] : iv_rulePerfomedActor= rulePerfomedActor EOF ;
    public final EObject entryRulePerfomedActor() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePerfomedActor = null;


        try {
            // InternalOM2D.g:974:54: (iv_rulePerfomedActor= rulePerfomedActor EOF )
            // InternalOM2D.g:975:2: iv_rulePerfomedActor= rulePerfomedActor EOF
            {
             newCompositeNode(grammarAccess.getPerfomedActorRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePerfomedActor=rulePerfomedActor();

            state._fsp--;

             current =iv_rulePerfomedActor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePerfomedActor"


    // $ANTLR start "rulePerfomedActor"
    // InternalOM2D.g:981:1: rulePerfomedActor returns [EObject current=null] : (otherlv_0= 'perfomed' otherlv_1= 'by' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject rulePerfomedActor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalOM2D.g:987:2: ( (otherlv_0= 'perfomed' otherlv_1= 'by' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalOM2D.g:988:2: (otherlv_0= 'perfomed' otherlv_1= 'by' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalOM2D.g:988:2: (otherlv_0= 'perfomed' otherlv_1= 'by' ( (otherlv_2= RULE_ID ) ) )
            // InternalOM2D.g:989:3: otherlv_0= 'perfomed' otherlv_1= 'by' ( (otherlv_2= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,30,FOLLOW_26); 

            			newLeafNode(otherlv_0, grammarAccess.getPerfomedActorAccess().getPerfomedKeyword_0());
            		
            otherlv_1=(Token)match(input,31,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getPerfomedActorAccess().getByKeyword_1());
            		
            // InternalOM2D.g:997:3: ( (otherlv_2= RULE_ID ) )
            // InternalOM2D.g:998:4: (otherlv_2= RULE_ID )
            {
            // InternalOM2D.g:998:4: (otherlv_2= RULE_ID )
            // InternalOM2D.g:999:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPerfomedActorRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getPerfomedActorAccess().getTypeActorCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePerfomedActor"


    // $ANTLR start "entryRuleModelUseCase"
    // InternalOM2D.g:1014:1: entryRuleModelUseCase returns [EObject current=null] : iv_ruleModelUseCase= ruleModelUseCase EOF ;
    public final EObject entryRuleModelUseCase() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModelUseCase = null;


        try {
            // InternalOM2D.g:1014:53: (iv_ruleModelUseCase= ruleModelUseCase EOF )
            // InternalOM2D.g:1015:2: iv_ruleModelUseCase= ruleModelUseCase EOF
            {
             newCompositeNode(grammarAccess.getModelUseCaseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModelUseCase=ruleModelUseCase();

            state._fsp--;

             current =iv_ruleModelUseCase; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModelUseCase"


    // $ANTLR start "ruleModelUseCase"
    // InternalOM2D.g:1021:1: ruleModelUseCase returns [EObject current=null] : (otherlv_0= 'model' otherlv_1= ':' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleModelUseCase() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOM2D.g:1027:2: ( (otherlv_0= 'model' otherlv_1= ':' ( ( ruleQualifiedName ) ) ) )
            // InternalOM2D.g:1028:2: (otherlv_0= 'model' otherlv_1= ':' ( ( ruleQualifiedName ) ) )
            {
            // InternalOM2D.g:1028:2: (otherlv_0= 'model' otherlv_1= ':' ( ( ruleQualifiedName ) ) )
            // InternalOM2D.g:1029:3: otherlv_0= 'model' otherlv_1= ':' ( ( ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,32,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getModelUseCaseAccess().getModelKeyword_0());
            		
            otherlv_1=(Token)match(input,24,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getModelUseCaseAccess().getColonKeyword_1());
            		
            // InternalOM2D.g:1037:3: ( ( ruleQualifiedName ) )
            // InternalOM2D.g:1038:4: ( ruleQualifiedName )
            {
            // InternalOM2D.g:1038:4: ( ruleQualifiedName )
            // InternalOM2D.g:1039:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getModelUseCaseRule());
            					}
            				

            					newCompositeNode(grammarAccess.getModelUseCaseAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModelUseCase"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalOM2D.g:1057:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalOM2D.g:1057:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalOM2D.g:1058:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalOM2D.g:1064:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalOM2D.g:1070:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalOM2D.g:1071:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalOM2D.g:1071:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalOM2D.g:1072:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_27); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalOM2D.g:1079:3: (kw= '.' this_ID_2= RULE_ID )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==33) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalOM2D.g:1080:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,33,FOLLOW_13); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_27); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleImport"
    // InternalOM2D.g:1097:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalOM2D.g:1097:47: (iv_ruleImport= ruleImport EOF )
            // InternalOM2D.g:1098:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalOM2D.g:1104:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1110:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalOM2D.g:1111:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalOM2D.g:1111:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalOM2D.g:1112:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalOM2D.g:1116:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalOM2D.g:1117:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalOM2D.g:1117:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalOM2D.g:1118:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalOM2D.g:1139:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalOM2D.g:1139:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalOM2D.g:1140:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalOM2D.g:1146:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1152:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalOM2D.g:1153:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalOM2D.g:1153:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalOM2D.g:1154:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_28);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalOM2D.g:1164:3: (kw= '.*' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==35) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalOM2D.g:1165:4: kw= '.*'
                    {
                    kw=(Token)match(input,35,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleEnumX"
    // InternalOM2D.g:1175:1: entryRuleEnumX returns [EObject current=null] : iv_ruleEnumX= ruleEnumX EOF ;
    public final EObject entryRuleEnumX() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumX = null;


        try {
            // InternalOM2D.g:1175:46: (iv_ruleEnumX= ruleEnumX EOF )
            // InternalOM2D.g:1176:2: iv_ruleEnumX= ruleEnumX EOF
            {
             newCompositeNode(grammarAccess.getEnumXRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumX=ruleEnumX();

            state._fsp--;

             current =iv_ruleEnumX; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumX"


    // $ANTLR start "ruleEnumX"
    // InternalOM2D.g:1182:1: ruleEnumX returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_attributes_4_0= ruleAttributeEnum ) )* otherlv_5= '}' ) ;
    public final EObject ruleEnumX() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_description_0_0 = null;

        EObject lv_attributes_4_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1188:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_attributes_4_0= ruleAttributeEnum ) )* otherlv_5= '}' ) )
            // InternalOM2D.g:1189:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_attributes_4_0= ruleAttributeEnum ) )* otherlv_5= '}' )
            {
            // InternalOM2D.g:1189:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_attributes_4_0= ruleAttributeEnum ) )* otherlv_5= '}' )
            // InternalOM2D.g:1190:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_attributes_4_0= ruleAttributeEnum ) )* otherlv_5= '}'
            {
            // InternalOM2D.g:1190:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==21) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalOM2D.g:1191:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:1191:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:1192:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getEnumXAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_29);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEnumXRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,36,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getEnumXAccess().getEnumKeyword_1());
            		
            // InternalOM2D.g:1213:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOM2D.g:1214:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOM2D.g:1214:4: (lv_name_2_0= RULE_ID )
            // InternalOM2D.g:1215:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEnumXAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumXRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getEnumXAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalOM2D.g:1235:3: ( (lv_attributes_4_0= ruleAttributeEnum ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID||LA15_0==21) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalOM2D.g:1236:4: (lv_attributes_4_0= ruleAttributeEnum )
            	    {
            	    // InternalOM2D.g:1236:4: (lv_attributes_4_0= ruleAttributeEnum )
            	    // InternalOM2D.g:1237:5: lv_attributes_4_0= ruleAttributeEnum
            	    {

            	    					newCompositeNode(grammarAccess.getEnumXAccess().getAttributesAttributeEnumParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_attributes_4_0=ruleAttributeEnum();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEnumXRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_4_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.AttributeEnum");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getEnumXAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumX"


    // $ANTLR start "entryRuleAttributeEnum"
    // InternalOM2D.g:1262:1: entryRuleAttributeEnum returns [EObject current=null] : iv_ruleAttributeEnum= ruleAttributeEnum EOF ;
    public final EObject entryRuleAttributeEnum() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeEnum = null;


        try {
            // InternalOM2D.g:1262:54: (iv_ruleAttributeEnum= ruleAttributeEnum EOF )
            // InternalOM2D.g:1263:2: iv_ruleAttributeEnum= ruleAttributeEnum EOF
            {
             newCompositeNode(grammarAccess.getAttributeEnumRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeEnum=ruleAttributeEnum();

            state._fsp--;

             current =iv_ruleAttributeEnum; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeEnum"


    // $ANTLR start "ruleAttributeEnum"
    // InternalOM2D.g:1269:1: ruleAttributeEnum returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleAttributeEnum() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_fullName_5_0=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1275:2: ( ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) ) )? ) )
            // InternalOM2D.g:1276:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) ) )? )
            {
            // InternalOM2D.g:1276:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) ) )? )
            // InternalOM2D.g:1277:3: ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) ) )?
            {
            // InternalOM2D.g:1277:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==21) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalOM2D.g:1278:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:1278:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:1279:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getAttributeEnumAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAttributeEnumRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOM2D.g:1296:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalOM2D.g:1297:4: (lv_name_1_0= RULE_ID )
            {
            // InternalOM2D.g:1297:4: (lv_name_1_0= RULE_ID )
            // InternalOM2D.g:1298:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_31); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAttributeEnumAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeEnumRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalOM2D.g:1314:3: (otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==37) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalOM2D.g:1315:4: otherlv_2= ',' otherlv_3= 'fullName' otherlv_4= ':' ( (lv_fullName_5_0= RULE_STRING ) )
                    {
                    otherlv_2=(Token)match(input,37,FOLLOW_20); 

                    				newLeafNode(otherlv_2, grammarAccess.getAttributeEnumAccess().getCommaKeyword_2_0());
                    			
                    otherlv_3=(Token)match(input,28,FOLLOW_16); 

                    				newLeafNode(otherlv_3, grammarAccess.getAttributeEnumAccess().getFullNameKeyword_2_1());
                    			
                    otherlv_4=(Token)match(input,24,FOLLOW_11); 

                    				newLeafNode(otherlv_4, grammarAccess.getAttributeEnumAccess().getColonKeyword_2_2());
                    			
                    // InternalOM2D.g:1327:4: ( (lv_fullName_5_0= RULE_STRING ) )
                    // InternalOM2D.g:1328:5: (lv_fullName_5_0= RULE_STRING )
                    {
                    // InternalOM2D.g:1328:5: (lv_fullName_5_0= RULE_STRING )
                    // InternalOM2D.g:1329:6: lv_fullName_5_0= RULE_STRING
                    {
                    lv_fullName_5_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_fullName_5_0, grammarAccess.getAttributeEnumAccess().getFullNameSTRINGTerminalRuleCall_2_3_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeEnumRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"fullName",
                    							lv_fullName_5_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeEnum"


    // $ANTLR start "entryRuleDAO"
    // InternalOM2D.g:1350:1: entryRuleDAO returns [EObject current=null] : iv_ruleDAO= ruleDAO EOF ;
    public final EObject entryRuleDAO() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDAO = null;


        try {
            // InternalOM2D.g:1350:44: (iv_ruleDAO= ruleDAO EOF )
            // InternalOM2D.g:1351:2: iv_ruleDAO= ruleDAO EOF
            {
             newCompositeNode(grammarAccess.getDAORule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDAO=ruleDAO();

            state._fsp--;

             current =iv_ruleDAO; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDAO"


    // $ANTLR start "ruleDAO"
    // InternalOM2D.g:1357:1: ruleDAO returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'dao' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* otherlv_7= '}' ) ;
    public final EObject ruleDAO() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_description_0_0 = null;

        EObject lv_attributes_6_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1363:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'dao' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* otherlv_7= '}' ) )
            // InternalOM2D.g:1364:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'dao' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* otherlv_7= '}' )
            {
            // InternalOM2D.g:1364:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'dao' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* otherlv_7= '}' )
            // InternalOM2D.g:1365:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'dao' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* otherlv_7= '}'
            {
            // InternalOM2D.g:1365:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==21) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalOM2D.g:1366:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:1366:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:1367:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getDAOAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_32);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDAORule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,38,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getDAOAccess().getDaoKeyword_1());
            		
            // InternalOM2D.g:1388:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOM2D.g:1389:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOM2D.g:1389:4: (lv_name_2_0= RULE_ID )
            // InternalOM2D.g:1390:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_33); 

            					newLeafNode(lv_name_2_0, grammarAccess.getDAOAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDAORule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalOM2D.g:1406:3: (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==26) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalOM2D.g:1407:4: otherlv_3= 'extends' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,26,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getDAOAccess().getExtendsKeyword_3_0());
                    			
                    // InternalOM2D.g:1411:4: ( ( ruleQualifiedName ) )
                    // InternalOM2D.g:1412:5: ( ruleQualifiedName )
                    {
                    // InternalOM2D.g:1412:5: ( ruleQualifiedName )
                    // InternalOM2D.g:1413:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDAORule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDAOAccess().getSuperTypeEntityCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_4);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_30); 

            			newLeafNode(otherlv_5, grammarAccess.getDAOAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalOM2D.g:1432:3: ( (lv_attributes_6_0= ruleAttribute ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==RULE_ID||LA20_0==21) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalOM2D.g:1433:4: (lv_attributes_6_0= ruleAttribute )
            	    {
            	    // InternalOM2D.g:1433:4: (lv_attributes_6_0= ruleAttribute )
            	    // InternalOM2D.g:1434:5: lv_attributes_6_0= ruleAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getDAOAccess().getAttributesAttributeParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_attributes_6_0=ruleAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDAORule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_6_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Attribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

            otherlv_7=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getDAOAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDAO"


    // $ANTLR start "entryRuleEntity"
    // InternalOM2D.g:1459:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // InternalOM2D.g:1459:47: (iv_ruleEntity= ruleEntity EOF )
            // InternalOM2D.g:1460:2: iv_ruleEntity= ruleEntity EOF
            {
             newCompositeNode(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity=ruleEntity();

            state._fsp--;

             current =iv_ruleEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalOM2D.g:1466:1: ruleEntity returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_enumentityatributes_7_0= ruleEnumEntityAtribute ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_9=null;
        EObject lv_description_0_0 = null;

        EObject lv_attributes_6_0 = null;

        EObject lv_enumentityatributes_7_0 = null;

        EObject lv_relations_8_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1472:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_enumentityatributes_7_0= ruleEnumEntityAtribute ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' ) )
            // InternalOM2D.g:1473:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_enumentityatributes_7_0= ruleEnumEntityAtribute ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' )
            {
            // InternalOM2D.g:1473:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_enumentityatributes_7_0= ruleEnumEntityAtribute ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}' )
            // InternalOM2D.g:1474:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'entity' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )? otherlv_5= '{' ( (lv_attributes_6_0= ruleAttribute ) )* ( (lv_enumentityatributes_7_0= ruleEnumEntityAtribute ) )* ( (lv_relations_8_0= ruleRelation ) )* otherlv_9= '}'
            {
            // InternalOM2D.g:1474:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==21) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalOM2D.g:1475:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:1475:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:1476:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getEntityAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_34);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEntityRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,39,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getEntityAccess().getEntityKeyword_1());
            		
            // InternalOM2D.g:1497:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOM2D.g:1498:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOM2D.g:1498:4: (lv_name_2_0= RULE_ID )
            // InternalOM2D.g:1499:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_33); 

            					newLeafNode(lv_name_2_0, grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalOM2D.g:1515:3: (otherlv_3= 'extends' ( ( ruleQualifiedName ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==26) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalOM2D.g:1516:4: otherlv_3= 'extends' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,26,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getEntityAccess().getExtendsKeyword_3_0());
                    			
                    // InternalOM2D.g:1520:4: ( ( ruleQualifiedName ) )
                    // InternalOM2D.g:1521:5: ( ruleQualifiedName )
                    {
                    // InternalOM2D.g:1521:5: ( ruleQualifiedName )
                    // InternalOM2D.g:1522:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEntityRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getEntityAccess().getSuperTypeEntityCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_4);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_30); 

            			newLeafNode(otherlv_5, grammarAccess.getEntityAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalOM2D.g:1541:3: ( (lv_attributes_6_0= ruleAttribute ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==21) ) {
                    int LA23_1 = input.LA(2);

                    if ( (LA23_1==RULE_STRING) ) {
                        int LA23_4 = input.LA(3);

                        if ( (LA23_4==RULE_ID) ) {
                            int LA23_6 = input.LA(4);

                            if ( (LA23_6==24) ) {
                                alt23=1;
                            }


                        }


                    }


                }
                else if ( (LA23_0==RULE_ID) ) {
                    int LA23_2 = input.LA(2);

                    if ( (LA23_2==24) ) {
                        alt23=1;
                    }


                }


                switch (alt23) {
            	case 1 :
            	    // InternalOM2D.g:1542:4: (lv_attributes_6_0= ruleAttribute )
            	    {
            	    // InternalOM2D.g:1542:4: (lv_attributes_6_0= ruleAttribute )
            	    // InternalOM2D.g:1543:5: lv_attributes_6_0= ruleAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getAttributesAttributeParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_attributes_6_0=ruleAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_6_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Attribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            // InternalOM2D.g:1560:3: ( (lv_enumentityatributes_7_0= ruleEnumEntityAtribute ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_ID) ) {
                    int LA24_1 = input.LA(2);

                    if ( (LA24_1==49) ) {
                        alt24=1;
                    }


                }
                else if ( (LA24_0==21) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalOM2D.g:1561:4: (lv_enumentityatributes_7_0= ruleEnumEntityAtribute )
            	    {
            	    // InternalOM2D.g:1561:4: (lv_enumentityatributes_7_0= ruleEnumEntityAtribute )
            	    // InternalOM2D.g:1562:5: lv_enumentityatributes_7_0= ruleEnumEntityAtribute
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getEnumentityatributesEnumEntityAtributeParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_enumentityatributes_7_0=ruleEnumEntityAtribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"enumentityatributes",
            	    						lv_enumentityatributes_7_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.EnumEntityAtribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            // InternalOM2D.g:1579:3: ( (lv_relations_8_0= ruleRelation ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==RULE_ID) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalOM2D.g:1580:4: (lv_relations_8_0= ruleRelation )
            	    {
            	    // InternalOM2D.g:1580:4: (lv_relations_8_0= ruleRelation )
            	    // InternalOM2D.g:1581:5: lv_relations_8_0= ruleRelation
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getRelationsRelationParserRuleCall_7_0());
            	    				
            	    pushFollow(FOLLOW_35);
            	    lv_relations_8_0=ruleRelation();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"relations",
            	    						lv_relations_8_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Relation");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_9=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getEntityAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleWebService"
    // InternalOM2D.g:1606:1: entryRuleWebService returns [EObject current=null] : iv_ruleWebService= ruleWebService EOF ;
    public final EObject entryRuleWebService() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWebService = null;


        try {
            // InternalOM2D.g:1606:51: (iv_ruleWebService= ruleWebService EOF )
            // InternalOM2D.g:1607:2: iv_ruleWebService= ruleWebService EOF
            {
             newCompositeNode(grammarAccess.getWebServiceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWebService=ruleWebService();

            state._fsp--;

             current =iv_ruleWebService; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWebService"


    // $ANTLR start "ruleWebService"
    // InternalOM2D.g:1613:1: ruleWebService returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'webservice' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_services_4_0= ruleService ) )* otherlv_5= '}' ) ;
    public final EObject ruleWebService() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_description_0_0 = null;

        EObject lv_services_4_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1619:2: ( ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'webservice' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_services_4_0= ruleService ) )* otherlv_5= '}' ) )
            // InternalOM2D.g:1620:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'webservice' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_services_4_0= ruleService ) )* otherlv_5= '}' )
            {
            // InternalOM2D.g:1620:2: ( ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'webservice' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_services_4_0= ruleService ) )* otherlv_5= '}' )
            // InternalOM2D.g:1621:3: ( (lv_description_0_0= ruleDescription ) )? otherlv_1= 'webservice' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_services_4_0= ruleService ) )* otherlv_5= '}'
            {
            // InternalOM2D.g:1621:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==21) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalOM2D.g:1622:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:1622:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:1623:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getWebServiceAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_36);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getWebServiceRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,40,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getWebServiceAccess().getWebserviceKeyword_1());
            		
            // InternalOM2D.g:1644:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOM2D.g:1645:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOM2D.g:1645:4: (lv_name_2_0= RULE_ID )
            // InternalOM2D.g:1646:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_2_0, grammarAccess.getWebServiceAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWebServiceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_37); 

            			newLeafNode(otherlv_3, grammarAccess.getWebServiceAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalOM2D.g:1666:3: ( (lv_services_4_0= ruleService ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( ((LA27_0>=41 && LA27_0<=44)) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalOM2D.g:1667:4: (lv_services_4_0= ruleService )
            	    {
            	    // InternalOM2D.g:1667:4: (lv_services_4_0= ruleService )
            	    // InternalOM2D.g:1668:5: lv_services_4_0= ruleService
            	    {

            	    					newCompositeNode(grammarAccess.getWebServiceAccess().getServicesServiceParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_37);
            	    lv_services_4_0=ruleService();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getWebServiceRule());
            	    					}
            	    					add(
            	    						current,
            	    						"services",
            	    						lv_services_4_0,
            	    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Service");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getWebServiceAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWebService"


    // $ANTLR start "entryRuleService"
    // InternalOM2D.g:1693:1: entryRuleService returns [EObject current=null] : iv_ruleService= ruleService EOF ;
    public final EObject entryRuleService() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleService = null;


        try {
            // InternalOM2D.g:1693:48: (iv_ruleService= ruleService EOF )
            // InternalOM2D.g:1694:2: iv_ruleService= ruleService EOF
            {
             newCompositeNode(grammarAccess.getServiceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleService=ruleService();

            state._fsp--;

             current =iv_ruleService; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleService"


    // $ANTLR start "ruleService"
    // InternalOM2D.g:1700:1: ruleService returns [EObject current=null] : ( ( ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) ) ) otherlv_1= 'service' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= ruleParams ) ) ) ;
    public final EObject ruleService() throws RecognitionException {
        EObject current = null;

        Token lv_verb_0_1=null;
        Token lv_verb_0_2=null;
        Token lv_verb_0_3=null;
        Token lv_verb_0_4=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_params_4_0 = null;

        EObject lv_params_6_0 = null;

        EObject lv_type_9_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1706:2: ( ( ( ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) ) ) otherlv_1= 'service' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= ruleParams ) ) ) )
            // InternalOM2D.g:1707:2: ( ( ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) ) ) otherlv_1= 'service' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= ruleParams ) ) )
            {
            // InternalOM2D.g:1707:2: ( ( ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) ) ) otherlv_1= 'service' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= ruleParams ) ) )
            // InternalOM2D.g:1708:3: ( ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) ) ) otherlv_1= 'service' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )* )? otherlv_7= ')' otherlv_8= ':' ( (lv_type_9_0= ruleParams ) )
            {
            // InternalOM2D.g:1708:3: ( ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) ) )
            // InternalOM2D.g:1709:4: ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) )
            {
            // InternalOM2D.g:1709:4: ( (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' ) )
            // InternalOM2D.g:1710:5: (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' )
            {
            // InternalOM2D.g:1710:5: (lv_verb_0_1= 'Get' | lv_verb_0_2= 'Post' | lv_verb_0_3= 'Delete' | lv_verb_0_4= 'Put' )
            int alt28=4;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt28=1;
                }
                break;
            case 42:
                {
                alt28=2;
                }
                break;
            case 43:
                {
                alt28=3;
                }
                break;
            case 44:
                {
                alt28=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }

            switch (alt28) {
                case 1 :
                    // InternalOM2D.g:1711:6: lv_verb_0_1= 'Get'
                    {
                    lv_verb_0_1=(Token)match(input,41,FOLLOW_38); 

                    						newLeafNode(lv_verb_0_1, grammarAccess.getServiceAccess().getVerbGetKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getServiceRule());
                    						}
                    						setWithLastConsumed(current, "verb", lv_verb_0_1, null);
                    					

                    }
                    break;
                case 2 :
                    // InternalOM2D.g:1722:6: lv_verb_0_2= 'Post'
                    {
                    lv_verb_0_2=(Token)match(input,42,FOLLOW_38); 

                    						newLeafNode(lv_verb_0_2, grammarAccess.getServiceAccess().getVerbPostKeyword_0_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getServiceRule());
                    						}
                    						setWithLastConsumed(current, "verb", lv_verb_0_2, null);
                    					

                    }
                    break;
                case 3 :
                    // InternalOM2D.g:1733:6: lv_verb_0_3= 'Delete'
                    {
                    lv_verb_0_3=(Token)match(input,43,FOLLOW_38); 

                    						newLeafNode(lv_verb_0_3, grammarAccess.getServiceAccess().getVerbDeleteKeyword_0_0_2());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getServiceRule());
                    						}
                    						setWithLastConsumed(current, "verb", lv_verb_0_3, null);
                    					

                    }
                    break;
                case 4 :
                    // InternalOM2D.g:1744:6: lv_verb_0_4= 'Put'
                    {
                    lv_verb_0_4=(Token)match(input,44,FOLLOW_38); 

                    						newLeafNode(lv_verb_0_4, grammarAccess.getServiceAccess().getVerbPutKeyword_0_0_3());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getServiceRule());
                    						}
                    						setWithLastConsumed(current, "verb", lv_verb_0_4, null);
                    					

                    }
                    break;

            }


            }


            }

            otherlv_1=(Token)match(input,45,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getServiceAccess().getServiceKeyword_1());
            		
            // InternalOM2D.g:1761:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalOM2D.g:1762:4: (lv_name_2_0= RULE_ID )
            {
            // InternalOM2D.g:1762:4: (lv_name_2_0= RULE_ID )
            // InternalOM2D.g:1763:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_39); 

            					newLeafNode(lv_name_2_0, grammarAccess.getServiceAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getServiceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,46,FOLLOW_40); 

            			newLeafNode(otherlv_3, grammarAccess.getServiceAccess().getLeftParenthesisKeyword_3());
            		
            // InternalOM2D.g:1783:3: ( ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )* )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==21||(LA30_0>=38 && LA30_0<=39)) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalOM2D.g:1784:4: ( (lv_params_4_0= ruleParams ) ) (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )*
                    {
                    // InternalOM2D.g:1784:4: ( (lv_params_4_0= ruleParams ) )
                    // InternalOM2D.g:1785:5: (lv_params_4_0= ruleParams )
                    {
                    // InternalOM2D.g:1785:5: (lv_params_4_0= ruleParams )
                    // InternalOM2D.g:1786:6: lv_params_4_0= ruleParams
                    {

                    						newCompositeNode(grammarAccess.getServiceAccess().getParamsParamsParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_41);
                    lv_params_4_0=ruleParams();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getServiceRule());
                    						}
                    						add(
                    							current,
                    							"params",
                    							lv_params_4_0,
                    							"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Params");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalOM2D.g:1803:4: (otherlv_5= ',' ( (lv_params_6_0= ruleParams ) ) )*
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0==37) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // InternalOM2D.g:1804:5: otherlv_5= ',' ( (lv_params_6_0= ruleParams ) )
                    	    {
                    	    otherlv_5=(Token)match(input,37,FOLLOW_42); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getServiceAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalOM2D.g:1808:5: ( (lv_params_6_0= ruleParams ) )
                    	    // InternalOM2D.g:1809:6: (lv_params_6_0= ruleParams )
                    	    {
                    	    // InternalOM2D.g:1809:6: (lv_params_6_0= ruleParams )
                    	    // InternalOM2D.g:1810:7: lv_params_6_0= ruleParams
                    	    {

                    	    							newCompositeNode(grammarAccess.getServiceAccess().getParamsParamsParserRuleCall_4_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_41);
                    	    lv_params_6_0=ruleParams();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getServiceRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"params",
                    	    								lv_params_6_0,
                    	    								"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Params");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop29;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,47,FOLLOW_16); 

            			newLeafNode(otherlv_7, grammarAccess.getServiceAccess().getRightParenthesisKeyword_5());
            		
            otherlv_8=(Token)match(input,24,FOLLOW_42); 

            			newLeafNode(otherlv_8, grammarAccess.getServiceAccess().getColonKeyword_6());
            		
            // InternalOM2D.g:1837:3: ( (lv_type_9_0= ruleParams ) )
            // InternalOM2D.g:1838:4: (lv_type_9_0= ruleParams )
            {
            // InternalOM2D.g:1838:4: (lv_type_9_0= ruleParams )
            // InternalOM2D.g:1839:5: lv_type_9_0= ruleParams
            {

            					newCompositeNode(grammarAccess.getServiceAccess().getTypeParamsParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_2);
            lv_type_9_0=ruleParams();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getServiceRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_9_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Params");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleService"


    // $ANTLR start "entryRuleParams"
    // InternalOM2D.g:1860:1: entryRuleParams returns [EObject current=null] : iv_ruleParams= ruleParams EOF ;
    public final EObject entryRuleParams() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParams = null;


        try {
            // InternalOM2D.g:1860:47: (iv_ruleParams= ruleParams EOF )
            // InternalOM2D.g:1861:2: iv_ruleParams= ruleParams EOF
            {
             newCompositeNode(grammarAccess.getParamsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParams=ruleParams();

            state._fsp--;

             current =iv_ruleParams; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParams"


    // $ANTLR start "ruleParams"
    // InternalOM2D.g:1867:1: ruleParams returns [EObject current=null] : (this_Entity_0= ruleEntity | this_DAO_1= ruleDAO ) ;
    public final EObject ruleParams() throws RecognitionException {
        EObject current = null;

        EObject this_Entity_0 = null;

        EObject this_DAO_1 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1873:2: ( (this_Entity_0= ruleEntity | this_DAO_1= ruleDAO ) )
            // InternalOM2D.g:1874:2: (this_Entity_0= ruleEntity | this_DAO_1= ruleDAO )
            {
            // InternalOM2D.g:1874:2: (this_Entity_0= ruleEntity | this_DAO_1= ruleDAO )
            int alt31=2;
            switch ( input.LA(1) ) {
            case 21:
                {
                int LA31_1 = input.LA(2);

                if ( (LA31_1==RULE_STRING) ) {
                    int LA31_4 = input.LA(3);

                    if ( (LA31_4==39) ) {
                        alt31=1;
                    }
                    else if ( (LA31_4==38) ) {
                        alt31=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 31, 4, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 31, 1, input);

                    throw nvae;
                }
                }
                break;
            case 39:
                {
                alt31=1;
                }
                break;
            case 38:
                {
                alt31=2;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalOM2D.g:1875:3: this_Entity_0= ruleEntity
                    {

                    			newCompositeNode(grammarAccess.getParamsAccess().getEntityParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Entity_0=ruleEntity();

                    state._fsp--;


                    			current = this_Entity_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalOM2D.g:1884:3: this_DAO_1= ruleDAO
                    {

                    			newCompositeNode(grammarAccess.getParamsAccess().getDAOParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_DAO_1=ruleDAO();

                    state._fsp--;


                    			current = this_DAO_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParams"


    // $ANTLR start "entryRuleAttribute"
    // InternalOM2D.g:1896:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalOM2D.g:1896:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalOM2D.g:1897:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalOM2D.g:1903:1: ruleAttribute returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_DATATYPE ) ) (otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) )? (otherlv_7= 'NotRequired' )? ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_type_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_fullName_6_0=null;
        Token otherlv_7=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:1909:2: ( ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_DATATYPE ) ) (otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) )? (otherlv_7= 'NotRequired' )? ) )
            // InternalOM2D.g:1910:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_DATATYPE ) ) (otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) )? (otherlv_7= 'NotRequired' )? )
            {
            // InternalOM2D.g:1910:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_DATATYPE ) ) (otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) )? (otherlv_7= 'NotRequired' )? )
            // InternalOM2D.g:1911:3: ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_type_3_0= RULE_DATATYPE ) ) (otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) )? (otherlv_7= 'NotRequired' )?
            {
            // InternalOM2D.g:1911:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==21) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalOM2D.g:1912:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:1912:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:1913:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getAttributeAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAttributeRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOM2D.g:1930:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalOM2D.g:1931:4: (lv_name_1_0= RULE_ID )
            {
            // InternalOM2D.g:1931:4: (lv_name_1_0= RULE_ID )
            // InternalOM2D.g:1932:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAttributeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_43); 

            			newLeafNode(otherlv_2, grammarAccess.getAttributeAccess().getColonKeyword_2());
            		
            // InternalOM2D.g:1952:3: ( (lv_type_3_0= RULE_DATATYPE ) )
            // InternalOM2D.g:1953:4: (lv_type_3_0= RULE_DATATYPE )
            {
            // InternalOM2D.g:1953:4: (lv_type_3_0= RULE_DATATYPE )
            // InternalOM2D.g:1954:5: lv_type_3_0= RULE_DATATYPE
            {
            lv_type_3_0=(Token)match(input,RULE_DATATYPE,FOLLOW_44); 

            					newLeafNode(lv_type_3_0, grammarAccess.getAttributeAccess().getTypeDATATYPETerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_3_0,
            						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.DATATYPE");
            				

            }


            }

            // InternalOM2D.g:1970:3: (otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==28) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalOM2D.g:1971:4: otherlv_4= 'fullName' otherlv_5= ':' ( (lv_fullName_6_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,28,FOLLOW_16); 

                    				newLeafNode(otherlv_4, grammarAccess.getAttributeAccess().getFullNameKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,24,FOLLOW_11); 

                    				newLeafNode(otherlv_5, grammarAccess.getAttributeAccess().getColonKeyword_4_1());
                    			
                    // InternalOM2D.g:1979:4: ( (lv_fullName_6_0= RULE_STRING ) )
                    // InternalOM2D.g:1980:5: (lv_fullName_6_0= RULE_STRING )
                    {
                    // InternalOM2D.g:1980:5: (lv_fullName_6_0= RULE_STRING )
                    // InternalOM2D.g:1981:6: lv_fullName_6_0= RULE_STRING
                    {
                    lv_fullName_6_0=(Token)match(input,RULE_STRING,FOLLOW_45); 

                    						newLeafNode(lv_fullName_6_0, grammarAccess.getAttributeAccess().getFullNameSTRINGTerminalRuleCall_4_2_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"fullName",
                    							lv_fullName_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalOM2D.g:1998:3: (otherlv_7= 'NotRequired' )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==48) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalOM2D.g:1999:4: otherlv_7= 'NotRequired'
                    {
                    otherlv_7=(Token)match(input,48,FOLLOW_2); 

                    				newLeafNode(otherlv_7, grammarAccess.getAttributeAccess().getNotRequiredKeyword_5());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleEnumEntityAtribute"
    // InternalOM2D.g:2008:1: entryRuleEnumEntityAtribute returns [EObject current=null] : iv_ruleEnumEntityAtribute= ruleEnumEntityAtribute EOF ;
    public final EObject entryRuleEnumEntityAtribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumEntityAtribute = null;


        try {
            // InternalOM2D.g:2008:59: (iv_ruleEnumEntityAtribute= ruleEnumEntityAtribute EOF )
            // InternalOM2D.g:2009:2: iv_ruleEnumEntityAtribute= ruleEnumEntityAtribute EOF
            {
             newCompositeNode(grammarAccess.getEnumEntityAtributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumEntityAtribute=ruleEnumEntityAtribute();

            state._fsp--;

             current =iv_ruleEnumEntityAtribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumEntityAtribute"


    // $ANTLR start "ruleEnumEntityAtribute"
    // InternalOM2D.g:2015:1: ruleEnumEntityAtribute returns [EObject current=null] : ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'uses' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleEnumEntityAtribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_description_0_0 = null;



        	enterRule();

        try {
            // InternalOM2D.g:2021:2: ( ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'uses' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalOM2D.g:2022:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'uses' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalOM2D.g:2022:2: ( ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'uses' ( (otherlv_3= RULE_ID ) ) )
            // InternalOM2D.g:2023:3: ( (lv_description_0_0= ruleDescription ) )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'uses' ( (otherlv_3= RULE_ID ) )
            {
            // InternalOM2D.g:2023:3: ( (lv_description_0_0= ruleDescription ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==21) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalOM2D.g:2024:4: (lv_description_0_0= ruleDescription )
                    {
                    // InternalOM2D.g:2024:4: (lv_description_0_0= ruleDescription )
                    // InternalOM2D.g:2025:5: lv_description_0_0= ruleDescription
                    {

                    					newCompositeNode(grammarAccess.getEnumEntityAtributeAccess().getDescriptionDescriptionParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_13);
                    lv_description_0_0=ruleDescription();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEnumEntityAtributeRule());
                    					}
                    					set(
                    						current,
                    						"description",
                    						lv_description_0_0,
                    						"br.ufes.nemo.seon.generator.ontologymodel.to.django.OM2D.Description");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalOM2D.g:2042:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalOM2D.g:2043:4: (lv_name_1_0= RULE_ID )
            {
            // InternalOM2D.g:2043:4: (lv_name_1_0= RULE_ID )
            // InternalOM2D.g:2044:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_46); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEnumEntityAtributeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumEntityAtributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,49,FOLLOW_13); 

            			newLeafNode(otherlv_2, grammarAccess.getEnumEntityAtributeAccess().getUsesKeyword_2());
            		
            // InternalOM2D.g:2064:3: ( (otherlv_3= RULE_ID ) )
            // InternalOM2D.g:2065:4: (otherlv_3= RULE_ID )
            {
            // InternalOM2D.g:2065:4: (otherlv_3= RULE_ID )
            // InternalOM2D.g:2066:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEnumEntityAtributeRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_3, grammarAccess.getEnumEntityAtributeAccess().getTypeEnumXCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumEntityAtribute"


    // $ANTLR start "entryRuleRelation"
    // InternalOM2D.g:2081:1: entryRuleRelation returns [EObject current=null] : iv_ruleRelation= ruleRelation EOF ;
    public final EObject entryRuleRelation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelation = null;


        try {
            // InternalOM2D.g:2081:49: (iv_ruleRelation= ruleRelation EOF )
            // InternalOM2D.g:2082:2: iv_ruleRelation= ruleRelation EOF
            {
             newCompositeNode(grammarAccess.getRelationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelation=ruleRelation();

            state._fsp--;

             current =iv_ruleRelation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelation"


    // $ANTLR start "ruleRelation"
    // InternalOM2D.g:2088:1: ruleRelation returns [EObject current=null] : (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany | this_ManyToOne_3= ruleManyToOne ) ;
    public final EObject ruleRelation() throws RecognitionException {
        EObject current = null;

        EObject this_OneToOne_0 = null;

        EObject this_ManyToMany_1 = null;

        EObject this_OneToMany_2 = null;

        EObject this_ManyToOne_3 = null;



        	enterRule();

        try {
            // InternalOM2D.g:2094:2: ( (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany | this_ManyToOne_3= ruleManyToOne ) )
            // InternalOM2D.g:2095:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany | this_ManyToOne_3= ruleManyToOne )
            {
            // InternalOM2D.g:2095:2: (this_OneToOne_0= ruleOneToOne | this_ManyToMany_1= ruleManyToMany | this_OneToMany_2= ruleOneToMany | this_ManyToOne_3= ruleManyToOne )
            int alt36=4;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case 53:
                    {
                    alt36=4;
                    }
                    break;
                case 51:
                    {
                    alt36=2;
                    }
                    break;
                case 52:
                    {
                    alt36=3;
                    }
                    break;
                case 50:
                    {
                    alt36=1;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }
            switch (alt36) {
                case 1 :
                    // InternalOM2D.g:2096:3: this_OneToOne_0= ruleOneToOne
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToOneParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToOne_0=ruleOneToOne();

                    state._fsp--;


                    			current = this_OneToOne_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalOM2D.g:2105:3: this_ManyToMany_1= ruleManyToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getManyToManyParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyToMany_1=ruleManyToMany();

                    state._fsp--;


                    			current = this_ManyToMany_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalOM2D.g:2114:3: this_OneToMany_2= ruleOneToMany
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getOneToManyParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_OneToMany_2=ruleOneToMany();

                    state._fsp--;


                    			current = this_OneToMany_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalOM2D.g:2123:3: this_ManyToOne_3= ruleManyToOne
                    {

                    			newCompositeNode(grammarAccess.getRelationAccess().getManyToOneParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyToOne_3=ruleManyToOne();

                    state._fsp--;


                    			current = this_ManyToOne_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelation"


    // $ANTLR start "entryRuleOneToOne"
    // InternalOM2D.g:2135:1: entryRuleOneToOne returns [EObject current=null] : iv_ruleOneToOne= ruleOneToOne EOF ;
    public final EObject entryRuleOneToOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToOne = null;


        try {
            // InternalOM2D.g:2135:49: (iv_ruleOneToOne= ruleOneToOne EOF )
            // InternalOM2D.g:2136:2: iv_ruleOneToOne= ruleOneToOne EOF
            {
             newCompositeNode(grammarAccess.getOneToOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToOne=ruleOneToOne();

            state._fsp--;

             current =iv_ruleOneToOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToOne"


    // $ANTLR start "ruleOneToOne"
    // InternalOM2D.g:2142:1: ruleOneToOne returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleOneToOne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOM2D.g:2148:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) ) )
            // InternalOM2D.g:2149:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) )
            {
            // InternalOM2D.g:2149:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) ) )
            // InternalOM2D.g:2150:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToOne' ( ( ruleQualifiedName ) )
            {
            // InternalOM2D.g:2150:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOM2D.g:2151:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOM2D.g:2151:4: (lv_name_0_0= RULE_ID )
            // InternalOM2D.g:2152:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_47); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,50,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToOneAccess().getOneToOneKeyword_1());
            		
            // InternalOM2D.g:2172:3: ( ( ruleQualifiedName ) )
            // InternalOM2D.g:2173:4: ( ruleQualifiedName )
            {
            // InternalOM2D.g:2173:4: ( ruleQualifiedName )
            // InternalOM2D.g:2174:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToOneRule());
            					}
            				

            					newCompositeNode(grammarAccess.getOneToOneAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToOne"


    // $ANTLR start "entryRuleManyToMany"
    // InternalOM2D.g:2192:1: entryRuleManyToMany returns [EObject current=null] : iv_ruleManyToMany= ruleManyToMany EOF ;
    public final EObject entryRuleManyToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyToMany = null;


        try {
            // InternalOM2D.g:2192:51: (iv_ruleManyToMany= ruleManyToMany EOF )
            // InternalOM2D.g:2193:2: iv_ruleManyToMany= ruleManyToMany EOF
            {
             newCompositeNode(grammarAccess.getManyToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyToMany=ruleManyToMany();

            state._fsp--;

             current =iv_ruleManyToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyToMany"


    // $ANTLR start "ruleManyToMany"
    // InternalOM2D.g:2199:1: ruleManyToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) (otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) ) )? ) ;
    public final EObject ruleManyToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalOM2D.g:2205:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) (otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) ) )? ) )
            // InternalOM2D.g:2206:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) (otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) ) )? )
            {
            // InternalOM2D.g:2206:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) (otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) ) )? )
            // InternalOM2D.g:2207:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToMany' ( ( ruleQualifiedName ) ) (otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) ) )?
            {
            // InternalOM2D.g:2207:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOM2D.g:2208:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOM2D.g:2208:4: (lv_name_0_0= RULE_ID )
            // InternalOM2D.g:2209:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_48); 

            					newLeafNode(lv_name_0_0, grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,51,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getManyToManyAccess().getManyToManyKeyword_1());
            		
            // InternalOM2D.g:2229:3: ( ( ruleQualifiedName ) )
            // InternalOM2D.g:2230:4: ( ruleQualifiedName )
            {
            // InternalOM2D.g:2230:4: ( ruleQualifiedName )
            // InternalOM2D.g:2231:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToManyRule());
            					}
            				

            					newCompositeNode(grammarAccess.getManyToManyAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_31);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalOM2D.g:2245:3: (otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==37) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalOM2D.g:2246:4: otherlv_3= ',' otherlv_4= 'by' otherlv_5= ':' ( ( ruleQualifiedName ) )
                    {
                    otherlv_3=(Token)match(input,37,FOLLOW_26); 

                    				newLeafNode(otherlv_3, grammarAccess.getManyToManyAccess().getCommaKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,31,FOLLOW_16); 

                    				newLeafNode(otherlv_4, grammarAccess.getManyToManyAccess().getByKeyword_3_1());
                    			
                    otherlv_5=(Token)match(input,24,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getManyToManyAccess().getColonKeyword_3_2());
                    			
                    // InternalOM2D.g:2258:4: ( ( ruleQualifiedName ) )
                    // InternalOM2D.g:2259:5: ( ruleQualifiedName )
                    {
                    // InternalOM2D.g:2259:5: ( ruleQualifiedName )
                    // InternalOM2D.g:2260:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getManyToManyRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getManyToManyAccess().getByEntityCrossReference_3_3_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyToMany"


    // $ANTLR start "entryRuleOneToMany"
    // InternalOM2D.g:2279:1: entryRuleOneToMany returns [EObject current=null] : iv_ruleOneToMany= ruleOneToMany EOF ;
    public final EObject entryRuleOneToMany() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneToMany = null;


        try {
            // InternalOM2D.g:2279:50: (iv_ruleOneToMany= ruleOneToMany EOF )
            // InternalOM2D.g:2280:2: iv_ruleOneToMany= ruleOneToMany EOF
            {
             newCompositeNode(grammarAccess.getOneToManyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneToMany=ruleOneToMany();

            state._fsp--;

             current =iv_ruleOneToMany; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneToMany"


    // $ANTLR start "ruleOneToMany"
    // InternalOM2D.g:2286:1: ruleOneToMany returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleOneToMany() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOM2D.g:2292:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) ) )
            // InternalOM2D.g:2293:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) )
            {
            // InternalOM2D.g:2293:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) ) )
            // InternalOM2D.g:2294:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'OneToMany' ( ( ruleQualifiedName ) )
            {
            // InternalOM2D.g:2294:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOM2D.g:2295:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOM2D.g:2295:4: (lv_name_0_0= RULE_ID )
            // InternalOM2D.g:2296:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_49); 

            					newLeafNode(lv_name_0_0, grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,52,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getOneToManyAccess().getOneToManyKeyword_1());
            		
            // InternalOM2D.g:2316:3: ( ( ruleQualifiedName ) )
            // InternalOM2D.g:2317:4: ( ruleQualifiedName )
            {
            // InternalOM2D.g:2317:4: ( ruleQualifiedName )
            // InternalOM2D.g:2318:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOneToManyRule());
            					}
            				

            					newCompositeNode(grammarAccess.getOneToManyAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneToMany"


    // $ANTLR start "entryRuleManyToOne"
    // InternalOM2D.g:2336:1: entryRuleManyToOne returns [EObject current=null] : iv_ruleManyToOne= ruleManyToOne EOF ;
    public final EObject entryRuleManyToOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyToOne = null;


        try {
            // InternalOM2D.g:2336:50: (iv_ruleManyToOne= ruleManyToOne EOF )
            // InternalOM2D.g:2337:2: iv_ruleManyToOne= ruleManyToOne EOF
            {
             newCompositeNode(grammarAccess.getManyToOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyToOne=ruleManyToOne();

            state._fsp--;

             current =iv_ruleManyToOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyToOne"


    // $ANTLR start "ruleManyToOne"
    // InternalOM2D.g:2343:1: ruleManyToOne returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToOne' ( ( ruleQualifiedName ) ) ) ;
    public final EObject ruleManyToOne() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalOM2D.g:2349:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToOne' ( ( ruleQualifiedName ) ) ) )
            // InternalOM2D.g:2350:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToOne' ( ( ruleQualifiedName ) ) )
            {
            // InternalOM2D.g:2350:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToOne' ( ( ruleQualifiedName ) ) )
            // InternalOM2D.g:2351:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'ManyToOne' ( ( ruleQualifiedName ) )
            {
            // InternalOM2D.g:2351:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalOM2D.g:2352:4: (lv_name_0_0= RULE_ID )
            {
            // InternalOM2D.g:2352:4: (lv_name_0_0= RULE_ID )
            // InternalOM2D.g:2353:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_50); 

            					newLeafNode(lv_name_0_0, grammarAccess.getManyToOneAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToOneRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,53,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getManyToOneAccess().getManyToOneKeyword_1());
            		
            // InternalOM2D.g:2373:3: ( ( ruleQualifiedName ) )
            // InternalOM2D.g:2374:4: ( ruleQualifiedName )
            {
            // InternalOM2D.g:2374:4: ( ruleQualifiedName )
            // InternalOM2D.g:2375:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getManyToOneRule());
            					}
            				

            					newCompositeNode(grammarAccess.getManyToOneAccess().getTypeEntityCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyToOne"

    // Delegated rules


    protected DFA6 dfa6 = new DFA6(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\25\1\4\10\uffff\1\26";
    static final String dfa_3s = "\1\50\1\4\10\uffff\1\50";
    static final String dfa_4s = "\2\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\uffff";
    static final String dfa_5s = "\13\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\1\2\2\uffff\1\11\1\uffff\1\10\6\uffff\1\7\1\uffff\1\4\1\uffff\1\5\1\3\1\6",
            "\1\12",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\2\2\uffff\1\11\1\uffff\1\10\10\uffff\1\4\1\uffff\1\5\1\3\1\6"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "659:2: (this_Module_0= ruleModule | this_Entity_1= ruleEntity | this_EnumX_2= ruleEnumX | this_DAO_3= ruleDAO | this_WebService_4= ruleWebService | this_Import_5= ruleImport | this_UseCase_6= ruleUseCase | this_Actor_7= ruleActor )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000001D40A600002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000001D40AE08000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000001D40A608000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000140000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000100008000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000208020L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000004004000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x00001E0000008000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x000080C000200000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000802000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x000000C000200000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0001000010000002L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0020000000000000L});

}
/**
 * generated by Xtext 2.27.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Description;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ModelUseCase;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.PerfomedActor;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.UseCase;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl.UseCaseImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl.UseCaseImpl#getName <em>Name</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl.UseCaseImpl#getFullName <em>Full Name</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl.UseCaseImpl#getManage <em>Manage</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl.UseCaseImpl#getPerfomedby <em>Perfomedby</em>}</li>
 *   <li>{@link br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl.UseCaseImpl#getModel <em>Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseCaseImpl extends AbstractElementImpl implements UseCase
{
  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected Description description;

  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getFullName() <em>Full Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFullName()
   * @generated
   * @ordered
   */
  protected static final String FULL_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFullName() <em>Full Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFullName()
   * @generated
   * @ordered
   */
  protected String fullName = FULL_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getManage() <em>Manage</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getManage()
   * @generated
   * @ordered
   */
  protected static final String MANAGE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getManage() <em>Manage</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getManage()
   * @generated
   * @ordered
   */
  protected String manage = MANAGE_EDEFAULT;

  /**
   * The cached value of the '{@link #getPerfomedby() <em>Perfomedby</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPerfomedby()
   * @generated
   * @ordered
   */
  protected EList<PerfomedActor> perfomedby;

  /**
   * The cached value of the '{@link #getModel() <em>Model</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModel()
   * @generated
   * @ordered
   */
  protected EList<ModelUseCase> model;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UseCaseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OM2DPackage.Literals.USE_CASE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Description getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDescription(Description newDescription, NotificationChain msgs)
  {
    Description oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OM2DPackage.USE_CASE__DESCRIPTION, oldDescription, newDescription);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDescription(Description newDescription)
  {
    if (newDescription != description)
    {
      NotificationChain msgs = null;
      if (description != null)
        msgs = ((InternalEObject)description).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OM2DPackage.USE_CASE__DESCRIPTION, null, msgs);
      if (newDescription != null)
        msgs = ((InternalEObject)newDescription).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OM2DPackage.USE_CASE__DESCRIPTION, null, msgs);
      msgs = basicSetDescription(newDescription, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OM2DPackage.USE_CASE__DESCRIPTION, newDescription, newDescription));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OM2DPackage.USE_CASE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getFullName()
  {
    return fullName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setFullName(String newFullName)
  {
    String oldFullName = fullName;
    fullName = newFullName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OM2DPackage.USE_CASE__FULL_NAME, oldFullName, fullName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getManage()
  {
    return manage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setManage(String newManage)
  {
    String oldManage = manage;
    manage = newManage;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, OM2DPackage.USE_CASE__MANAGE, oldManage, manage));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<PerfomedActor> getPerfomedby()
  {
    if (perfomedby == null)
    {
      perfomedby = new EObjectContainmentEList<PerfomedActor>(PerfomedActor.class, this, OM2DPackage.USE_CASE__PERFOMEDBY);
    }
    return perfomedby;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ModelUseCase> getModel()
  {
    if (model == null)
    {
      model = new EObjectContainmentEList<ModelUseCase>(ModelUseCase.class, this, OM2DPackage.USE_CASE__MODEL);
    }
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case OM2DPackage.USE_CASE__DESCRIPTION:
        return basicSetDescription(null, msgs);
      case OM2DPackage.USE_CASE__PERFOMEDBY:
        return ((InternalEList<?>)getPerfomedby()).basicRemove(otherEnd, msgs);
      case OM2DPackage.USE_CASE__MODEL:
        return ((InternalEList<?>)getModel()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case OM2DPackage.USE_CASE__DESCRIPTION:
        return getDescription();
      case OM2DPackage.USE_CASE__NAME:
        return getName();
      case OM2DPackage.USE_CASE__FULL_NAME:
        return getFullName();
      case OM2DPackage.USE_CASE__MANAGE:
        return getManage();
      case OM2DPackage.USE_CASE__PERFOMEDBY:
        return getPerfomedby();
      case OM2DPackage.USE_CASE__MODEL:
        return getModel();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case OM2DPackage.USE_CASE__DESCRIPTION:
        setDescription((Description)newValue);
        return;
      case OM2DPackage.USE_CASE__NAME:
        setName((String)newValue);
        return;
      case OM2DPackage.USE_CASE__FULL_NAME:
        setFullName((String)newValue);
        return;
      case OM2DPackage.USE_CASE__MANAGE:
        setManage((String)newValue);
        return;
      case OM2DPackage.USE_CASE__PERFOMEDBY:
        getPerfomedby().clear();
        getPerfomedby().addAll((Collection<? extends PerfomedActor>)newValue);
        return;
      case OM2DPackage.USE_CASE__MODEL:
        getModel().clear();
        getModel().addAll((Collection<? extends ModelUseCase>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case OM2DPackage.USE_CASE__DESCRIPTION:
        setDescription((Description)null);
        return;
      case OM2DPackage.USE_CASE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case OM2DPackage.USE_CASE__FULL_NAME:
        setFullName(FULL_NAME_EDEFAULT);
        return;
      case OM2DPackage.USE_CASE__MANAGE:
        setManage(MANAGE_EDEFAULT);
        return;
      case OM2DPackage.USE_CASE__PERFOMEDBY:
        getPerfomedby().clear();
        return;
      case OM2DPackage.USE_CASE__MODEL:
        getModel().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case OM2DPackage.USE_CASE__DESCRIPTION:
        return description != null;
      case OM2DPackage.USE_CASE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case OM2DPackage.USE_CASE__FULL_NAME:
        return FULL_NAME_EDEFAULT == null ? fullName != null : !FULL_NAME_EDEFAULT.equals(fullName);
      case OM2DPackage.USE_CASE__MANAGE:
        return MANAGE_EDEFAULT == null ? manage != null : !MANAGE_EDEFAULT.equals(manage);
      case OM2DPackage.USE_CASE__PERFOMEDBY:
        return perfomedby != null && !perfomedby.isEmpty();
      case OM2DPackage.USE_CASE__MODEL:
        return model != null && !model.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", fullName: ");
    result.append(fullName);
    result.append(", manage: ");
    result.append(manage);
    result.append(')');
    return result.toString();
  }

} //UseCaseImpl

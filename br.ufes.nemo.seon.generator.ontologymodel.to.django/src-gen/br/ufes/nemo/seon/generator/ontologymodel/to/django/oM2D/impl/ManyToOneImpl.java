/**
 * generated by Xtext 2.27.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.impl;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ManyToOne;
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Many To One</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ManyToOneImpl extends RelationImpl implements ManyToOne
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ManyToOneImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return OM2DPackage.Literals.MANY_TO_ONE;
  }

} //ManyToOneImpl

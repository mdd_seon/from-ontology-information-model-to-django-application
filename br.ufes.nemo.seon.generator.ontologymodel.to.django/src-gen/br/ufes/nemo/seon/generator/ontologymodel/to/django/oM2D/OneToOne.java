/**
 * generated by Xtext 2.27.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>One To One</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OM2DPackage#getOneToOne()
 * @model
 * @generated
 */
public interface OneToOne extends Relation
{
} // OneToOne

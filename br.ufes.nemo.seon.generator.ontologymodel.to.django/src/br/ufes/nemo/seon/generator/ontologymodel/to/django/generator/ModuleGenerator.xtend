package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator

import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.AdminGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.DAOGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.FormGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.ModelGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.SerializerGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.TemplateGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.URLGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.UTILGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components.ViewGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module
import com.google.inject.Inject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class ModuleGenerator extends AbstractGenerator {

	@Inject
	ModelGenerator modelGenerator;

	@Inject
	AdminGenerator adminGenerator;

	@Inject
	URLGenerator urlGenerator;

	@Inject
	ViewGenerator viewGenerator;

	@Inject
	FormGenerator formGenerator;

	@Inject
	TemplateGenerator templateGenerator;

	@Inject
	SerializerGenerator serializerGenerator;

	@Inject
	DAOGenerator daoGenerator;
	
	@Inject
	UTILGenerator utilGenerator;

	var PATH_LIB_ENTITY = "solution/apps/"

	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		// Criando os models, service e applications
		for (m : resource.allContents.toIterable.filter(Module)) {
			
			if (m.moduleimported == null){
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/__init__.py", m.create_init)

				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/migrations/__init__.py", "")
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/admin.py", adminGenerator.create(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/templates/" + m.name.toLowerCase + "/__init__.py",
					"")
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/models.py", modelGenerator.create(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/factory.py", m.createFactory)
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/apps.py", m.createApps)
	
				templateGenerator.create(m, fsa, PATH_LIB_ENTITY);
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/web_urls.py", urlGenerator.createURLWeb(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/api_urls.py", urlGenerator.createURLAPI(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/dao.py", daoGenerator.create(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/web_urls.py", urlGenerator.createURLWeb(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/api_urls.py", urlGenerator.createURLAPI(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/web_views.py", viewGenerator.create(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/api_views.py", viewGenerator.createAPIView(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/forms.py", formGenerator.create(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/validators.py", '')
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/messages.py", '')
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/exception.py", '')
				
				for (e: m.elements){
					if (e.eClass.instanceClass == Entity) {
						val Entity entity = e as Entity;
						fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/test/unit/"+entity.name.toLowerCase+"_tests.py", entity.createclasstest(m))
						
						
					}
				}
		
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/services.py", m.createServices)
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/signals.py", m.createSignals)
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/serializers.py", serializerGenerator.create(m))
	
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/utils.py", utilGenerator.createUtil())
				
				fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/pagination.py", pagination())
				
					
			}

						
					}

	}
	
	def pagination()'''
	from rest_framework import pagination
	from rest_framework.response import Response
	
	class CustomPagination(pagination.PageNumberPagination):
	    page_size = 10
	    page_size_query_param = 'page_size'
	    max_page_size = 1000
	
	    def get_paginated_response(self, data):
	        return Response({
	            'meta': {
	                'current_page': self.page.number,
	                'per_page': self.page.paginator.per_page,
	                'max_per_page': self.max_page_size,
	                'total': self.page.paginator.count
	            },
	            'data': data
	        })
	'''
	
	def create_init(Module module)'''
	default_app_config = 'apps.«module.name.toLowerCase».apps.«module.name.toFirstUpper»Config'
	''' 
	def createApps(Module module) '''
		from django.apps import AppConfig
		from django.utils.translation import ugettext_lazy as _
		
		class «module.name.toFirstUpper»Config(AppConfig):
			name  = 'apps.«module.name.toLowerCase»'
			label = 'apps_«module.name.toLowerCase»'
			
			def ready(self):
				import apps.«module.name.toLowerCase»
		
	'''
	def createFactory(Module module)'''
		«FOR e : module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				
				class «entity.name»Factory(factory.django.DjangoModelFactory):
					class Meta:
						model = «module.name.toLowerCase».«entity.name»
				
			«ENDIF»
		«ENDFOR»
	
	'''
	def createattributeJsontest(Entity entity)'''
	«FOR attribute : entity.attributes»				
		'«attribute.name»' : «attribute.createAtrributeValuesTest»,
	«ENDFOR»
	'''
	
	def createattributetest(Entity entity)'''
	«FOR attribute : entity.attributes»				
		«attribute.name» = «attribute.createAtrributeValuesTest»,
	«ENDFOR»
	'''
	
	def createAtrributeValuesTest(Attribute attribute)'''
		«IF attribute.type.toLowerCase == 'string'» self.faker.first_name()
		«ELSEIF attribute.type.toLowerCase == 'decimal'» random.uniform(0.00, 100.5)
		«ELSEIF attribute.type.toLowerCase == 'cnpj'» self.faker.cnpj()
		«ELSEIF attribute.type.toLowerCase == 'cpf'» self.faker.cpf()
		«ELSEIF attribute.type.toLowerCase == 'telefone'» self.faker.phone_number()
		«ELSEIF attribute.type.toLowerCase == 'celular'» self.faker.phone_number()
		«ELSEIF attribute.type.toLowerCase == 'datetime'» self.faker.date()
		«ELSEIF attribute.type.toLowerCase == 'url'» 'http://'+ self.fake.domain_name()
		«ELSEIF attribute.type.toLowerCase == 'date'» self.faker.date()
		«ELSEIF attribute.type.toLowerCase == 'email'» self.faker.ascii_company_email()
		«ELSEIF attribute.type.toLowerCase == 'integer'» random.uniform(0, 100)
		«ELSE» 'TO DO'		
		«ENDIF»'''
	
	
	def createclasstest(Entity entity, Module module)'''
		import json
		from rest_framework import status
		from django.test import TestCase, Client
		from django.urls import reverse
		from «module.name.toLowerCase».models import «entity.name» 
		from «module.name.toLowerCase».serializers import «entity.name»Serializer
		from faker import Faker
		import random
		
		class «entity.name»Tests(TestCase):
			
			def setUp(self):
				self.faker = Faker('pt_BR')
				self.client = Client()
				
				self.«entity.name.toLowerCase»_1 = «entity.name».objects.create(«entity.createattributetest»)
				self.«entity.name.toLowerCase»_2 = «entity.name».objects.create(«entity.createattributetest»)
				self.«entity.name.toLowerCase»_3 = «entity.name».objects.create(«entity.createattributetest»)
				
				self.valid_payload = {
					«entity.createattributeJsontest»
				}
				self.invalid_payload = {
					«entity.createattributeJsontest»    
				}
			def test_valid_create(self):
				response = self.client.post(reverse('«entity.name.toLowerCase»-api-list'), 
				data=json.dumps(self.valid_payload),
				content_type='application/json')
				self.assertEqual(response.status_code, status.HTTP_201_CREATED)		
			
			def test_invalid_create(self):
				response = self.client.post(
			            reverse('«entity.name.toLowerCase»-api-list'),
			            data=json.dumps(self.invalid_payload),
			            content_type='application/json'
			        )
				self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)		
			
			def test_valid_upload(self):
				response = self.client.put(reverse('«entity.name.toLowerCase»-detail',kwargs={'pk': self.«entity.name.toLowerCase»_1.id}),
			            data=json.dumps(self.valid_payload), content_type='application/json')
			
				self.assertEqual(response.status_code, status.HTTP_200_OK)
			
			def test_invalid_upload(self):
				response = self.client.put(reverse('«entity.name.toLowerCase»-detail',kwargs={'pk': self.«entity.name.toLowerCase»_1.id}),
			     data=json.dumps(self.invalid_payload), content_type='application/json')
			
				self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
			
			# retornando todos os elementos	
			def test_retrieve_all(self):
				response = self.client.get(reverse('«entity.name.toLowerCase»-api-list'))
			
				data = «entity.name».objects.all()
					
				serializer = «entity.name»Serializer(data, context={'request': None}, many=True)
				#Aqui deve comparar todos os compos do objeto com serialização
				self.assertEqual(response.data, serializer.data)
					
				self.assertIsNotNone(response.data)
				self.assertEqual(response.status_code, status.HTTP_200_OK)
				
			# retornando um elemento
			def test_valid_get_element(self):
				response = self.client.get(reverse('«entity.name.toLowerCase»-detail',kwargs={'pk': self.«entity.name.toLowerCase»_1.id}))
				data = «entity.name».objects.get(pk=self.condicao_1.id)
				#Aqui deve comparar todos os compos do objeto com serialização
				self.assertEqual(str(data.uuid),response.data['uuid'])
				self.assertIsNotNone(response.data)
				self.assertEqual(response.status_code, status.HTTP_200_OK)
				
			# erro ao retornar um elemento invalido
			def test_invalid_get_element(self):
				response = self.client.get(reverse('«entity.name.toLowerCase»-detail',kwargs={'pk': 666}))
				self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
				
			# Delete um elemento valido
			def test_valid_delete(self):
				response = self.client.delete(reverse('«entity.name.toLowerCase»-detail',kwargs={'pk': self.«entity.name.toLowerCase»_1.id}))
				self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
				
			# Delete um elemento valido
			def test_invalid_delete(self):
				response = self.client.delete(reverse('«entity.name.toLowerCase»-detail',kwargs={'pk': 666}))
				self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
	
	'''

	
	def createServices(Module module) '''
		from .models import *
		«FOR e : module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«entity.createService»
			«ENDIF»
		«ENDFOR»
	'''

	def createService(Entity entity) '''
		class «entity.name»Service():
			""" Service of «entity.name»"""
			
	'''

	def createSignals(Module module) '''
		from .models import *
		from django.db.models.signals import *
		from django.dispatch import receiver
		from .services import *
		
		«FOR e : module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				## signals from «entity.name» 
				«entity.createsignal»
				#######
				
			«ENDIF»
		«ENDFOR»
		
	'''
	def createsignal(Entity entity)'''
		@receiver(pre_init, sender=«entity.name»)
		def pre_init_«entity.name.toLowerCase»(sender, *args, **kwargs):
			pass
			
		@receiver(post_init, sender=«entity.name»)
		def post_init_«entity.name.toLowerCase»(sender, instance):
			pass
		
		@receiver(pre_save, sender=«entity.name»)
		def pre_save_«entity.name.toLowerCase»(sender, instance, raw, using, update_fields):
			pass
		
		@receiver(post_save, sender=«entity.name»)
		def post_save_«entity.name.toLowerCase»(sender, instance, created, raw, using, update_fields):
			pass
			
		@receiver(pre_delete, sender=«entity.name»)
		def pre_delete_«entity.name.toLowerCase»(sender, instance, using):
			pass
			
		@receiver(post_delete, sender=«entity.name»)
		def post_delete_«entity.name.toLowerCase»(sender, instance, using):
			pass
			
		@receiver(m2m_changed, sender=«entity.name»)
		def m2m_changed_«entity.name.toLowerCase»(sender, instance, action, reverse, model, pk_set, using):
			pass
	'''

}

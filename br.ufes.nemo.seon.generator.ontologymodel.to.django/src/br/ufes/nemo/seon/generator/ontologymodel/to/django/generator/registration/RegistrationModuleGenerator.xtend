package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.registration;

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class RegistrationModuleGenerator extends AbstractGenerator {

	var PATH_LIB_ENTITY = "solution/apps/registration/"

	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		// Criando os models, service e applications
		fsa.generateFile(PATH_LIB_ENTITY + "__init__.py", "")

		fsa.generateFile(PATH_LIB_ENTITY + "/migrations/__init__.py", "")

		fsa.generateFile(PATH_LIB_ENTITY + "admin.py", createAdmin())

		fsa.generateFile(PATH_LIB_ENTITY + "/models.py", createModel())

		fsa.generateFile(PATH_LIB_ENTITY + "/urls.py", createURL())

		fsa.generateFile(PATH_LIB_ENTITY + "/views.py", createView())

		fsa.generateFile(PATH_LIB_ENTITY + "/forms.py", createForm())

		fsa.generateFile(PATH_LIB_ENTITY + "/app.py", createConfig())

		fsa.generateFile(PATH_LIB_ENTITY + "/tests.py", createTest())

	}
	
	def createConfig()'''
	
	from django.apps import AppConfig
	
	
	class AuthConfig(AppConfig):
	    name = 'apps.auth'
	    label = 'apps_auth'
	
	'''
	
	def createTest()'''
	from django.test import TestCase
	'''
	
	
	def createView()'''
	# Create your views here.
	from django.shortcuts import render, redirect
	from django.contrib.auth import authenticate, login
	from .forms import LoginForm, SignUpForm
	
	
	def login_view(request):
	    form = LoginForm(request.POST or None)
	
	    msg = None
	
	    if request.method == "POST":
	
	        if form.is_valid():
	            username = form.cleaned_data.get("username")
	            password = form.cleaned_data.get("password")
	            user = authenticate(username=username, password=password)
	            if user is not None:
	                login(request, user)
	                return redirect("/")
	            else:
	                msg = 'Invalid credentials'
	        else:
	            msg = 'Error validating the form'
	
	    return render(request, "accounts/login.html", {"form": form, "msg": msg})
	
	
	def register_user(request):
	    msg = None
	    success = False
	
	    if request.method == "POST":
	        form = SignUpForm(request.POST)
	        if form.is_valid():
	            form.save()
	            username = form.cleaned_data.get("username")
	            raw_password = form.cleaned_data.get("password1")
	            user = authenticate(username=username, password=raw_password)
	
	            msg = 'User created - please <a href="/login">login</a>.'
	            success = True
	
	            # return redirect("/login/")
	
	        else:
	            msg = 'Form is not valid'
	    else:
	        form = SignUpForm()
	
	    return render(request, "accounts/register.html", {"form": form, "msg": msg, "success": success})
	
	'''
	
	def createURL()'''
	from django.urls import path
	from .views import login_view, register_user
	from django.contrib.auth.views import LogoutView
	
	urlpatterns = [
	    path('login/', login_view, name="login"),
	    path('register/', register_user, name="register"),
	    path("logout/", LogoutView.as_view(), name="logout")
	]
	'''
	
	def createAdmin()'''
	from django.contrib import admin	
	
	'''

	def createModel()'''
	from django.db import models
	
	# Create your models here.
	'''

	def createForm()'''
	from django import forms
	from django.contrib.auth.forms import UserCreationForm
	from django.contrib.auth.models import User
	
	class LoginForm(forms.Form):
	    username = forms.CharField(
	        widget=forms.TextInput(
	            attrs={
	                "placeholder": "Username",
	                "class": "form-control"
	            }
	        ))
	    password = forms.CharField(
	        widget=forms.PasswordInput(
	            attrs={
	                "placeholder": "Password",
	                "class": "form-control"
	            }
	        ))
	
	
	class SignUpForm(UserCreationForm):
	    username = forms.CharField(
	        widget=forms.TextInput(
	            attrs={
	                "placeholder": "Username",
	                "class": "form-control"
	            }
	        ))
	    email = forms.EmailField(
	        widget=forms.EmailInput(
	            attrs={
	                "placeholder": "Email",
	                "class": "form-control"
	            }
	        ))
	    password1 = forms.CharField(
	        widget=forms.PasswordInput(
	            attrs={
	                "placeholder": "Password",
	                "class": "form-control"
	            }
	        ))
	    password2 = forms.CharField(
	        widget=forms.PasswordInput(
	            attrs={
	                "placeholder": "Password check",
	                "class": "form-control"
	            }
	        ))
	
	    class Meta:
	        model = User
	        fields = ('username', 'email', 'password1', 'password2')
	
	'''
}

package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator

import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class TestDGenerator extends AbstractGenerator {
	
	var PATH_LIB_ENTITY = "solution/apps/"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
			// Criando os models, service e applications
		for (m : resource.allContents.toIterable.filter(Module)) {
			
			fsa.generateFile(PATH_LIB_ENTITY + m.name.toLowerCase + "/tests.py",m.createTests)
			
		}		
		
	}
	
	def createTests(Module module)'''
	from django.test import TestCase
	'''
		
	
}
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class BDDGenerator extends AbstractGenerator {
	
	var PATH = "solution/"

	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) 
	{
		fsa.generateFile(PATH + "features/steps/README.md", "");
		
		for (entity : resource.allContents.toIterable.filter(Entity)) {
			
			fsa.generateFile(PATH + "features/"+entity.name+".feature", entity.create_feature_file);
		}
		
	}	
	
	private def create_feature_file(Entity entity)'''
	Feature: Manager to «entity.name»
	
	Scenarios to manager an instance of «entity.name»
	
	Background:
		Give I am Authenticated User
	
	Scenario Outline: Manager an instance of «entity.name» 
	
		When I want to "<action>" a «entity.name»
		Then the system respoonde "<response>" with "<type"
	
		Examples:
		|action |type   |response|
		|Create |success|201     |
		|Create |fail   |400     |
		|Delete |success|204     |
		|Delete |fail   |404     |
		|ReadOne|success|200     |
		|ReadOne|fail   |404     |
		|ReadAll|success|200     |
		|ReadAll|fail   |400     |
		|Update |success|200     |
		|Update |fail   |400     |
	'''
	
	 
}
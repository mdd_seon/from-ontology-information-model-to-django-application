/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator

import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.application.SettingsGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.registration.RegistrationModuleGenerator
import br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.templates.TemplateComponentGenerator
import com.google.inject.Inject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class OM2DGenerator extends AbstractGenerator {
	
	@Inject
	ModuleGenerator moduleGenerator;
	
	@Inject
	RegistrationModuleGenerator registrationModuleGenerator;
	
	@Inject
	SettingsGenerator settingsGenerator;
	
	@Inject
	TemplateComponentGenerator templateComponentGenerator;
	
	@Inject
	DocumentationGenerator documentationGenerator;
	
	@Inject
	HelperGenerator helperGenerator;
	
	@Inject
	ScriptGenerator scriptGenerator;
	
	@Inject
	BDDGenerator bddGenerator;
	
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
			
		moduleGenerator.doGenerate(resource,fsa, context);
		
		helperGenerator.doGenerate(resource,fsa, context);
		
		scriptGenerator.doGenerate(resource,fsa, context);
		
		registrationModuleGenerator.doGenerate(resource,fsa, context);
		
		settingsGenerator.doGenerate(resource,fsa, context);
		
		templateComponentGenerator.doGenerate(resource,fsa, context);
		
		documentationGenerator.doGenerate(resource,fsa, context);
		
		bddGenerator.doGenerate(resource,fsa, context);
		
		
		
	}
}

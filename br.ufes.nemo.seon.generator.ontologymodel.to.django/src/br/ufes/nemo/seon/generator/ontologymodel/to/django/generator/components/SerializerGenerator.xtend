package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class SerializerGenerator {
	
	
	def create(Module module)'''
	from rest_framework import serializers
	from .models import («FOR e: module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
					«entity.name»,
			«ENDIF»
		«ENDFOR»)
	
	«FOR e: module.elements»
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«entity.createSerializer»
		«ENDIF»
	«ENDFOR»
	'''
	
	def createSerializer(Entity entity)'''
		class «entity.name»WriteSerializer (serializers.ModelSerializer):
		
			class Meta:
				model = «entity.name» 
				fields = '__all__'
				exclude = ("polymorphic_ctype",)
						
		
		class «entity.name»ReadSerializer (serializers.ModelSerializer):
		
			class Meta:
				model = «entity.name» 
				fields = '__all__'
				depth = 1
				exclude = ("polymorphic_ctype",)
				
	'''
	
}
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class SignalsGenerator {
	
	// Criando o aquivo Admin
	def create(Module module)'''
	from django.db.models.signals import pre_save, pre_init, post_init, post_save,m2m_changed,pre_delete, post_delete
	from django.dispatch import receiver
	from .models import *
		
	«FOR e: module.elements»
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«entity.createSignal»
		«ENDIF»
	«ENDFOR»
	'''
	//Criando o conteudo de cada classe do Admin
	def createSignal(Entity entity)'''
	
	@receiver(pre_init, sender=«entity.name»)
	def pre_init_«entity.name.toLowerCase()»(sender, *args, **kwargs):
		pass
	
	@receiver(post_init, sender=«entity.name»)
	def post_init_«entity.name.toLowerCase()»(sender, instance):
		pass
		
	@receiver(pre_save, sender=«entity.name»)
	def pre_save_«entity.name.toLowerCase()»(sender, instance,raw, using, update_fields):
		pass
	
	@receiver(post_save, sender=«entity.name»)
	def post_save_«entity.name.toLowerCase()»(sender, instance, created, raw, using, update_fields):
		pass
	
	@receiver(m2m_changed, sender=«entity.name»)
	def m2m_changed_«entity.name.toLowerCase()»(sender, instance, action, reverse, model, pk_set, using):
		pass	
	
	@receiver(pre_delete, sender=«entity.name»)
	def delete_pre_«entity.name.toLowerCase()»(sender, instance, using):
		pass
	
	@receiver(post_delete, sender=«entity.name»)
	def delete_post_«entity.name.toLowerCase()»(sender, instance, using):
		pass
		
	'''	
	
}
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.templates

import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import com.google.inject.Inject
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration

class TemplateComponentGenerator extends AbstractGenerator{
	
	var PATH_LIB_ENTITY = "solution/templates"
	
	@Inject
	TemplateComponent templateGenerator;
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		for (configuration : resource.allContents.toIterable.filter(Configuration)) {
			
			fsa.generateFile(PATH_LIB_ENTITY + "/base.html", templateGenerator.createBase)
			fsa.generateFile(PATH_LIB_ENTITY + "/components/menu/menu_lateral.html", templateGenerator.create_menu_lateral(resource))
			fsa.generateFile(PATH_LIB_ENTITY + "/components/menu/menu_topo.html", templateGenerator.create_menu_top)
			//fsa.generateFile(PATH_LIB_ENTITY + "/components/content.html", templateGenerator.con)
		}
		
		
	}
	
}
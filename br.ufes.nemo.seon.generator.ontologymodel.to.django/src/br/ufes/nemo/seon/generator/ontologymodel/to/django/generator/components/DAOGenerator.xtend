package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.DAO
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class DAOGenerator {
	
	
	def create(Module module) '''
		# -*- coding: utf-8 -*-
		«FOR e : module.elements»
			«IF e.eClass.instanceClass == DAO»
				«val DAO dao = e as DAO»
				«dao.create»
			«ENDIF»
		«ENDFOR»
	'''
	def create(DAO dao) '''
		class «dao.name»():
			"""«IF dao.description !== null»«dao.description.textfield»«ENDIF»"""
			def __init__():
			«FOR attribute : dao.attributes»
				self.«attribute.name» = None
			«ENDFOR»
			
	'''
}
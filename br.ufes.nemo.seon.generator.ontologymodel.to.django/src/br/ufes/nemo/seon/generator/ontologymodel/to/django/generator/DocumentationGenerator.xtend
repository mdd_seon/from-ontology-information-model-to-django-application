package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumX
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ManyToMany
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OneToMany
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.OneToOne
import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.naming.IQualifiedNameProvider

class DocumentationGenerator extends AbstractGenerator {

	var PATH_LIB_DOCUMENTATION = "/solution/"

	@Inject extension IQualifiedNameProvider;	

	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		for (c : resource.allContents.toIterable.filter(Configuration)) {

			fsa.generateFile(PATH_LIB_DOCUMENTATION + "README.md", c.compile)

		}
		// criando o readme geral
		var Iterable<Module> modules = resource.allContents.toIterable.filter(Module);
		fsa.generateFile(PATH_LIB_DOCUMENTATION + "docs/README.md", modules.compile)

		modules = resource.allContents.toIterable.filter(Module);
		fsa.generateFile(PATH_LIB_DOCUMENTATION + "docs/packagediagram.puml", modules.createPackageDiagram)

		for (m : resource.allContents.toIterable.filter(Module)) {
			fsa.generateFile(PATH_LIB_DOCUMENTATION + "docs/" + m.name.toLowerCase + "/README.md", m.compile)
			fsa.generateFile(PATH_LIB_DOCUMENTATION + "docs/" + m.name.toLowerCase + "/classdiagram.puml",
				m.createClassDiagram)

		}
	}

	def createPackageDiagram(Iterable<Module> modules) '''
		@startuml
		«FOR m : modules»
			namespace «m.name»{
				
			}
		«ENDFOR»
		@enduml
	'''

	def compile(Iterable<Module> modules) '''
		# Documentation
		## Conceptual Model
		![Domain Diagram](packagediagram.png)	
								
		## Modules
		«FOR m : modules»
			«IF m.description !== null»
				* **[«m.name»](./«m.name.toLowerCase»/)** : «m.description.textfield» 
				
			«ELSE»
				* **[«m.name»](./«m.name»/)** : - 
				
			«ENDIF»
		«ENDFOR»
		
	'''

	def createClassDiagram(Module m) {
		val List<Entity> entities = new ArrayList()
		val List<EnumX> enuns = new ArrayList()
		for (e : m.elements) {
			if (e.eClass.instanceClass == Entity) {
				val Entity entity = e as Entity
				entities.add(entity)
			}
			else{
				if (e.eClass.instanceClass == EnumX){
					val EnumX enumx = e as EnumX
					enuns.add(enumx)
				}	
			}
		}
		'''
			«entities.createClassDiagram(m, enuns)»
		'''

	}

	def createClassDiagram(List<Entity> entities, Module m,List<EnumX> enuns) '''
		@startuml
		
		«IF enuns.size()>0»
			«FOR e: enuns»
			enum «e.name»{
				«FOR a: e.attributes»
					«a.name»
				«ENDFOR»
			}
			«ENDFOR»
			
		«ENDIF»
		
		«IF entities.size()>0»
			«FOR e: entities»
				class «e.name»{
					«FOR a: e.attributes»
						«a.type»:«a.name»
					«ENDFOR»
					
					«FOR r: e.relations»
						«IF r.eClass.instanceClass != ManyToMany»
						 	«r.type.name» : «r.name.toLowerCase» 
						«ENDIF»	
					«ENDFOR»
					
				}
				«IF e.superType !== null»«e.superType.name» <|-- «e.name»«ENDIF»
				
				«FOR a: e.enumentityatributes»
					«e.name» "1" -- "1" «a.type.name» : «a.name.toLowerCase» >
				«ENDFOR»
				
				«FOR r: e.relations»
					«IF r.eClass.instanceClass == OneToOne»
					
						«IF r.type.eContainer.fullyQualifiedName.toString.toLowerCase == m.name.toString.toLowerCase»	
							«e.name» "1" -- "1" «r.type.name» : «r.name.toLowerCase» >
						«ELSE»
							«e.name» "1" -- "1" «r.type.eContainer.fullyQualifiedName.toString».«r.type.name» : «r.name.toLowerCase» >
						«ENDIF»		
					«ENDIF»
					
					«IF r.eClass.instanceClass == ManyToMany»
						«IF r.type.eContainer.fullyQualifiedName.toString.toLowerCase == m.name.toString.toLowerCase»	
							«e.name» "0..*" -- "0..*" «r.type.name» : «r.name.toLowerCase» >
						«ELSE»
							«e.name» "0..*" -- "0..*" «r.type.eContainer.fullyQualifiedName.toString».«r.type.name» : «r.name.toLowerCase» >
						«ENDIF»
						
					«ENDIF»
					
					«IF r.eClass.instanceClass == OneToMany»
						«IF r.type.eContainer.fullyQualifiedName.toString.toLowerCase == m.name.toString.toLowerCase»	
							«e.name» "1" -- "0..*" «r.type.name» : «r.name.toLowerCase» >
						«ELSE»
							«e.name» "1" -- "0..*" «r.type.eContainer.fullyQualifiedName.toString».«r.type.name» : «r.name.toLowerCase» >
						«ENDIF»
					«ENDIF»
				«ENDFOR»
				
			«ENDFOR»
		«ENDIF»
		
		@enduml
	'''

	def compile(Module m) '''
		# Documentation: «m.name»
		«IF (m.description !== null) » 
			«m.description.textfield»
		«ENDIF»
		## Application Conceptual Data Model
		![Domain Diagram](classdiagram.png)	
						
		## Entities
		«FOR e : m.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«entity.descriptionX»
			«ENDIF»
		«ENDFOR»
		
			
	'''

	def descriptionX(Entity e) '''
		«IF e.description !== null»
			* **«e.name»** : «e.description.textfield»«ELSE»* **«e.name»** : -
		«ENDIF»	
	'''

	def compile(Configuration e) '''
		# «e.software.name.toFirstUpper»
		
		## General Information
		* **Software**:«e.software.name»
		* **Author**:«e.author.name»
		* **Author's e-mail**:«e.author_email.name»
		* **Source Repository**: [«e.repository.name»](«e.repository.name»)  
		
		## Goal
		«e.about.name»
		
		## Documentation
		
		The Documentation can be found in this [link](./docs/README.md)
			
		## Instalation
		
		
		
			
	'''

}

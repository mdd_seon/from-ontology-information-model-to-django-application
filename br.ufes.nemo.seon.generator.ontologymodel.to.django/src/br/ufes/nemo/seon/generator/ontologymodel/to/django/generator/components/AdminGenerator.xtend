package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class AdminGenerator {
		
	// Criando o aquivo Admin
	def create(Module module)'''
	from django.contrib import admin
	from .models import *
		
	«FOR e: module.elements»
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«entity.createAdmin»
		«ENDIF»
	«ENDFOR»
	'''
	//Criando o conteudo de cada classe do Admin
	def createAdmin(Entity entity)'''
	admin.site.register(«entity.name»)
	'''
	
	
}
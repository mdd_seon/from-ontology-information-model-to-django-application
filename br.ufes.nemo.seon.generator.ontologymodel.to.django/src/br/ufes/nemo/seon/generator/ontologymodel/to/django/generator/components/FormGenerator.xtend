package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class FormGenerator {
	
	def create(Module module, Entity entity)'''
	from django import forms
	from .models import *
	from django.utils.translation import ugettext_lazy as _
	
	«entity.createForm»
	'''
	
	
	def create(Module module)'''
		from django import forms
		from .models import *
		from django.utils.translation import ugettext_lazy as _	
		
		«FOR e: module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«entity.createForm»
			«ENDIF»
		«ENDFOR»
	'''
	
	def createForm(Entity e)'''
	class «e.name»Form(forms.ModelForm):
		class Meta:
			model =«e.name»
			fields = [
			«FOR attribute: e.attributes»
				'«attribute.name»',	
			«ENDFOR»
			] 
	
			labels = {
			«FOR attribute: e.attributes»
				'«attribute.name»': _('«IF attribute.fullName !== null»«attribute.fullName»«ELSE»«attribute.name.toFirstLower»«ENDIF»'),	
			«ENDFOR»
			}
			widgets = {}
	
	'''
	
}



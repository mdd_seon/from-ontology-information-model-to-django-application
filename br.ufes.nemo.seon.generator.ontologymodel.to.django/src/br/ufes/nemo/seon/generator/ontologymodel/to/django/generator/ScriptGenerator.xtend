package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class ScriptGenerator extends AbstractGenerator	{

	var PATH_LIB = "/solution/"

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {

		for (configuration : resource.allContents.toIterable.filter(Configuration)) {
			
			val softwareName = configuration.software.name

			fsa.generateFile(PATH_LIB +"/script_deploy/gunicorn/"+configuration.software.name.toLowerCase+".socket", softwareName.createApplicationSocket)
			fsa.generateFile(PATH_LIB + "/script_deploy/gunicorn/"+configuration.software.name.toLowerCase+".service", softwareName.createApplicationService)
			
			fsa.generateFile(PATH_LIB + "/script_deploy/nginx/"+softwareName.toLowerCase, softwareName.createNginixfile)
			
			fsa.generateFile(PATH_LIB + "/script_deploy/db/sqlmigrate.py", createSQLmigratePython())
			
			fsa.generateFile(PATH_LIB + "/sqlmigrate.sh", createSQLMigrateSH())
			fsa.generateFile(PATH_LIB +  "/run-linux.sh", softwareName.createRunLinuxSH)
		}

	}
	
	def createApplicationSocket(String softwareName)'''
		[Unit]
		Description=«softwareName» socket
		
		[Socket]
		ListenStream=/run/«softwareName.toLowerCase».socket
		
		[Install]
		WantedBy=sockets.target
	'''
	
	def createApplicationService(String softwareName)'''
		[Unit]
		Description=«softwareName» service
		Requires=«softwareName.toLowerCase».socket
		After=network.target
		
		[Service]
		User=root
		Group=www-data
		WorkingDirectory=/xxxxx/«softwareName.toLowerCase»
		ExecStart=/xxxxx/«softwareName.toLowerCase»/.venv/bin/gunicorn \
		          --access-logfile - \
		          --workers 3 \
		          --bind unix:/run/factor.sock \
		          «softwareName.toLowerCase».wsgi:application
		          
		[Install]
		WantedBy=multi-user.target
	'''
	
	def createNginixfile(String softwareName)'''
	server {
	    listen xxxxxx;
	    server_name xxxxx;
	
	    location = /favicon.ico { access_log off; log_not_found off; }
	    location /staticfiles/ {
	        root /xxxx/xxx/«softwareName.toLowerCase»/«softwareName.toLowerCase»;
	    }
	
	    location / {
	        include proxy_params;
	        proxy_pass http://unix:/run/«softwareName.toLowerCase».sock;
	    }
	}
	'''
	
	def createSQLmigratePython()'''
	import string
	f = open("migrationsplan.txt", "r")
	count  = 0
	commands = []
	for x in f:
	  #separando o nome do app com do arquivo  
	  
	  apps = x.rsplit( ".", 1 )[ 0 ]
	  #Pegando apenas o nome do app
	
	  apps = apps.split()[2]
	  apps = apps.translate({ord(c): None for c in string.whitespace}) 
	  
	  #pegando o nome do migrate
	  migrate = x.rsplit( ".", 1 )[ 1 ]
	  migrate = migrate.strip()
	  
	  migrate = migrate.translate({ord(c): None for c in string.whitespace})
	  command = ""
	  command = "python manage.py sqlmigrate "+apps+" "+migrate+' > ./script_deploy/db/sql/'+str(count)+'_'+apps+'_'+migrate+'.sql' 
	  commands.append (command)
	  count = count +1
	
	file = open('sql_command_migrates.sh','w')
	for command in commands:
	    file.write(command+'\n')
	file.close()
	'''
	
	def createSQLMigrateSH()'''
	python manage.py makemigrations
	python manage.py showmigrations --plan > migrationsplan.txt
	python script_deploy/db/sqlmigrate.py 
	'''
	
	def createRunLinuxSH(String softwareName)'''
	gunicorn «softwareName.toLowerCase».wsgi:application --bind 0.0.0.0:8000 \
	    --error-logfile error.log \
	    --access-logfile access.log
	'''
}

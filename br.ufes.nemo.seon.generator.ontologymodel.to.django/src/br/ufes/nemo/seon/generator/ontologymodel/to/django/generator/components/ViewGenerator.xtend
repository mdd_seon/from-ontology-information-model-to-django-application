package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class ViewGenerator {
	
	def createAux(Module module, Entity entity)'''
		class «entity.name»ListView(LoginRequiredMixin,ListView):
			model = «entity.name»
			template_name  = '«module.name.toLowerCase»/«entity.name.toLowerCase»/«entity.name.toLowerCase»_list.html'
					
			class «entity.name»CreateView(LoginRequiredMixin,SuccessMessageMixin,CreateView):
				model = «entity.name»
				form_class = «entity.name»Form
				template_name  = '«module.name.toLowerCase»/«entity.name.toLowerCase»/«entity.name.toLowerCase»_form.html'
				success_url = reverse_lazy('«entity.name.toLowerCase»-web-list')
				success_message = 'Cadastro realizado com sucesso!'
					
			class «entity.name»UpdateView(LoginRequiredMixin,SuccessMessageMixin,UpdateView):
				model = «entity.name»
				form_class = «entity.name»Form
				template_name  = '«module.name.toLowerCase»/«entity.name.toLowerCase»/«entity.name.toLowerCase»_form.html'
				success_url = reverse_lazy('«entity.name.toLowerCase»-web-list')
				success_message = 'Dados atualizados com sucesso!'
			
			class «entity.name»DeleteView(LoginRequiredMixin,SuccessMessageMixin,DeleteView):
				model = «entity.name»
				form_class = «entity.name»Form
				template_name  = '«module.name.toLowerCase»/«entity.name.toLowerCase»/«entity.name.toLowerCase»_confirm_delete.html'
				success_url = reverse_lazy('«entity.name.toLowerCase»-web-list')
				
				def delete(self, request, *args, **kwargs):
					response = super().delete(request, *args, **kwargs)
					messages.success(self.request, 'Exclusão realizada com sucesso!')
					return response
	'''
	
	def create(Module module, Entity entity)'''
	from django.shortcuts import render
	from django.urls import reverse_lazy
	from django.views.generic.list import ListView
	from django.views.generic.edit import CreateView,UpdateView, DeleteView
	from django.contrib.auth.mixins import LoginRequiredMixin
	from django.contrib.messages.views import SuccessMessageMixin
	from django.contrib import messages
		
	from .models import *
	from .forms import *	
	
	«createAux(module, entity)»
			
	'''
	
	
	def create(Module module)'''
	from django.shortcuts import render
	from django.urls import reverse_lazy
	from django.views.generic.list import ListView
	from django.views.generic.edit import CreateView,UpdateView, DeleteView
	from django.contrib.auth.mixins import LoginRequiredMixin
	from django.contrib.messages.views import SuccessMessageMixin
	from django.contrib import messages
	
	from .models import *
	from .forms import *	
	
	«FOR e: module.elements»	
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«createAux(module, entity)»
			«ENDIF»
		«ENDFOR»
		
	
	'''
	
	def searchfields (Entity entity )'''
	«FOR a: entity.attributes»«IF a.type != 'file'»'«a.name»',«ENDIF»«ENDFOR»
	'''
	
	def createAPIViewAux(Module Module, Entity entity)'''
		class «entity.name»ViewSet(ModelViewSet):
			queryset = «entity.name».objects.all()
			pagination_class = CustomPagination
			authentication_classes = [OAuth2Authentication, SessionAuthentication]
			permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
			filter_backends = (filters.SearchFilter, 
								filters.OrderingFilter, 
								django_filters.rest_framework.DjangoFilterBackend)
			filterset_fields = '__all__'	
			search_fields = [«entity.searchfields»]
			ordering_fields = '__all__'	
			
			def get_serializer_class(self):
				if self.request.method in ['GET']:
			    	return «entity.name»ReadSerializer
			    return «entity.name»WriteSerializer
		
	'''
	
	/*
	def createAPIViewAux(Module Module, Entity entity)'''
		class «entity.name»APIList(generics.ListCreateAPIView):
			queryset = «entity.name».objects.all()
			serializer_class = «entity.name»Serializer
			pagination_class = CustomPagination
			authentication_classes = [OAuth2Authentication, SessionAuthentication]
			permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
			filter_backends = (filters.SearchFilter, filters.OrderingFilter, django_filters.rest_framework.DjangoFilterBackend)
			filterset_fields = '__all__'	
			search_fields = [«entity.searchfields»]
			ordering_fields = '__all__'	
		
		class «entity.name»APIDetail(generics.RetrieveUpdateDestroyAPIView):
			queryset = «entity.name».objects.all()
			serializer_class = «entity.name»Serializer
			pagination_class = CustomPagination
			authentication_classes = [OAuth2Authentication, SessionAuthentication]
			permission_classes = [Or(IsAdminUser, TokenHasReadWriteScope)]
			filter_backends = (filters.SearchFilter, filters.OrderingFilter, django_filters.rest_framework.DjangoFilterBackend)
			filterset_fields = '__all__'
			search_fields = [«entity.searchfields»]
			ordering_fields = '__all__'
	'''
	 */
	def createAPIView(Module module, Entity entity)'''
	from rest_framework import generics
	from .models import *
	from .serializers import *
		
	from django_filters import rest_framework as filters
	from rest_framework.permissions import IsAdminUser
	from rest_condition import Or
	from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication
	from rest_framework.authentication import SessionAuthentication
	from .pagination import CustomPagination
	from rest_framework import generics
	from rest_framework import filters
	import django_filters.rest_framework
	
	
	«createAPIViewAux(module, entity)»
	'''
	
	
	def createImport(Module module)'''
	«FOR e: module.elements»	
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«entity.name»,
		«ENDIF»
	«ENDFOR»'''
	
	def createSerializar(Module module)'''
	«FOR e: module.elements»	
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«entity.name»ReadSerializer,«entity.name»WriteSerializer,
		«ENDIF»
	«ENDFOR»'''
	
	def createAPIView(Module module)'''
	from .models import («module.createImport»)
	from .serializers import («module.createSerializar»)
	from rest_framework.viewsets import ModelViewSet
	from django_filters import rest_framework as filters
	from rest_framework.permissions import IsAdminUser
	from rest_condition import Or
	from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication
	from rest_framework.authentication import SessionAuthentication
	from .pagination import CustomPagination
	from rest_framework import generics
	from rest_framework import filters
	import django_filters.rest_framework
	
	
	«FOR e: module.elements»	
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«createAPIViewAux(module, entity)»
					
		«ENDIF»
	«ENDFOR»
	
	'''
	
}
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module

class URLGenerator {
	
	def createURLWeb(Entity entity, Module module)'''
	from django.urls import path, register_converter
	from .web_views import *
	from .utils import *
	register_converter(HashIdConverter, "hashid")
	
	urlpatterns = [
		«createURLWebAux(entity, module)»
	]
	'''
	
	def createURLWebAux(Entity entity, Module module)'''
			#Web
			path('«module.name.toLowerCase»/«entity.name.toLowerCase»/',«entity.name»ListView.as_view(), name='«entity.name.toLowerCase»-web-list'),
			path('«module.name.toLowerCase»/«entity.name.toLowerCase»/add',«entity.name»CreateView.as_view(), name='«entity.name.toLowerCase»-web-create'),
			path('«module.name.toLowerCase»/«entity.name.toLowerCase»/<hashid:pk>',«entity.name»UpdateView.as_view(), name='«entity.name.toLowerCase»-web-detail'),
			path('«module.name.toLowerCase»/«entity.name.toLowerCase»/<hashid:pk>/delete',«entity.name»DeleteView.as_view(), name='«entity.name.toLowerCase»-web-delete'),			
				
	'''
	
	def createURLWeb(Module module)'''
	from django.urls import path, register_converter
	from .web_views import *
	from .utils import *
	register_converter(HashIdConverter, "hashid")
	
	urlpatterns = [
	«FOR e: module.elements»
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
			«createURLWebAux(entity, module)»	
		«ENDIF»
	«ENDFOR»
	]
	'''
	
	def createURLAPI(Entity entity, Module module)'''
	from django.urls import path, register_converter
	from .api_views import *
	from .utils import *
	register_converter(HashIdConverter, "hashid")
	
	urlpatterns = [
		«createURLAPIAux(entity,module)»
	]
	'''
	
	def createURLAPIAux(Entity entity, Module module)'''
		router.register(r'«entity.name.toLowerCase»', «entity.name»ViewSet, basename='«entity.name.toLowerCase»')
	'''


	def createModelImport(Module module)'''
	«FOR e: module.elements»
		«IF e.eClass.instanceClass == Entity»
			«val Entity entity = e as Entity»
				«entity.name»ViewSet,
			«ENDIF»
		«ENDFOR»
	'''

	def createURLAPI(Module module)'''
		
		from rest_framework import routers
		from django.urls import include, path
		from .api_views import («module.createModelImport»)
		
		router = routers.DefaultRouter()
		
		«FOR e: module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«createURLAPIAux(entity,module)»	
			«ENDIF»
		«ENDFOR»
		
		urlpatterns = [
		    path('«module.name.toLowerCase»/', include(router.urls)),
		]
		'''
	
}
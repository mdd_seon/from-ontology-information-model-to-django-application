package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module
import org.eclipse.xtext.generator.IFileSystemAccess2

class TemplateGenerator {
	
	def create(Module module, IFileSystemAccess2 fsa, String path){
		
		for (element: module.elements) {
			if (element.eClass.instanceClass == Entity){
				
				val Entity entity = element as Entity;
				
				val String file_path = path + module.name.toLowerCase + "/templates/"+ module.name.toLowerCase 
					+"/"+entity.name.toLowerCase+"/"+entity.name.toLowerCase;
				
				fsa.generateFile(file_path+"_form.html",entity.createFormHTML)	
				fsa.generateFile(file_path+"_list.html",entity.createListHTML)	
				fsa.generateFile(file_path+"_update.html",entity.createUpdateHTML)	
				fsa.generateFile(file_path+"_confirm_delete.html",entity.createDeleteHTML)	
			}
			
		}
			
	}
	
	def createFormHTML(Entity entity)'''
	{% extends 'base.html' %}
	
	{% load static %}
	
	{% block menu_lateral %}
	{% include 'components/menu/menu_lateral.html' %}
	{% endblock %}
	
	{% block menu_topo %}
	{% include 'components/menu/menu_topo.html' %}
	{% endblock %}
	
	{% block content %}
	
	<div class="card mt-4">
	    <div class="card-header bg-white d-flex justify-content-between ">
	       <h3>«entity.name.toUpperCase»</h3>    
	    </div>
	    <div class="card-body mt-3">
	        <form method="post" autocomplete="off" id="«entity.name.toLowerCase»-form">
	            {% csrf_token %}
	            {% load crispy_forms_tags %}
	            {{ form | crispy }}
	            
	            <div class="btn-actions">
	                <button type="submit" class="btn btn-primary">Cadastrar</button>                
	            </div>	
	        </form>	
	
	    </div>
	
	</div>
	
	
	{% endblock %}
	
	
	'''	
	
	def createListHTML(Entity entity)'''
	{% extends 'base.html' %}
	
	{% load static %}
	
	{% block menu %}
	
	{% endblock %}
	
	{% block content %}
	
	<div class="card mt-4">
	    <div class="card-header bg-white d-flex justify-content-between ">
	      
	       <h3>«entity.name.toUpperCase»</h3>
	    </div>
	    <div class="card-body mt-3">
	        <p><a class="btn btn-primary" href="{% url '«entity.name.toLowerCase»-web-create' %}" role="button">Novo Cadastro</a></p>
	        <br>
	        <table class="table">
	  <thead>
	    <tr>
	      «FOR attribute: entity.attributes»
	      <th scope="col">«attribute.name»</th>	      	
	      «ENDFOR»
	    </tr>
	  </thead>
	  <tbody>
	    {% for object in object_list %}
	    <tr>
	      «FOR attribute: entity.attributes»
	      <td>{{ object.«attribute.name»}}</td>
	      «ENDFOR»
	      <td> <a href="{% url '«entity.name.toLowerCase»-web-detail' object.id %}">Editar </a>| <a href="{% url '«entity.name.toLowerCase»-web-delete' object.id %}">Excluir</a></td>
	    </tr>
	    {% empty %}
	    <tr>
	      <td>Vazio</td>
	      <td>----</td>
	      <td>----</td>
	      <td>----</td>               
	    </tr>
	    {% endfor %}    
	  </tbody>
	</table>
	
	    </div>
	
	</div>
	
	{% endblock %}
	
	{% block scripts %}
	
	{{ block.super }}
	
	
	{% endblock %}
		
	
	'''
	
	def createUpdateHTML(Entity entity)'''	
	{% extends 'base.html' %}
	
	{% load static %}
	
	{% block menu %}
	
	{% endblock %}
	
	{% block content %}
	
	<div class="card mt-4">
	    <div class="card-header bg-white d-flex justify-content-between ">
	        <h3>«entity.name.toUpperCase»</h3>        
	    </div>
	    <div class="card-body mt-3">
	        <form method="post" autocomplete="off" id="«entity.name.toLowerCase»-form">
	            {% csrf_token %}
	            {% load crispy_forms_tags %}
	            {{ form | crispy }}
	            
	            <div class="btn-actions">
	                <button type="submit" class="btn btn-primary">Atualizar</button>                
	            </div>            
	        </form>
	    </div>
	
	</div>
	
	
	{% endblock %}
	
	
	{% block scripts %}
	
	{{ block.super }}
	
	{% endblock %}
	
	'''
	
	def createDeleteHTML(Entity entity)'''
	{% extends 'base.html' %}
	
	{% load static %}
	
	{% block menu %}
	{% endblock %}
	
	<!-- Bloco de conteúdo da nossa página -->
	{% block content %}
	    <div class="card mt-4">
	        <div class="card-header bg-white d-flex justify-content-between ">
	            <h3>Exclusão do «entity.name.toUpperCase»</h3>        
	        </div> 
	
	        <div class="card-body mt-3">
	            Você tem certeza que quer excluir?
	            <br>
	            
	
	            <form method="post">
	                {% csrf_token %}
	
	                <hr />
	                <div class="text-right">
	                    <a href="{% url '«entity.name.toLowerCase»-web-list' %}" class="btn btn-outline-danger">
	                        Cancelar
	                    </a>
	                    <button class="btn btn-danger">Excluir</button>
	                </div>
	            </form>
	        </div>
	     </div>
	{% endblock %}	
	
	'''
			
	
}
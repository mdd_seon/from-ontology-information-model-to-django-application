package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.application

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Configuration
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class SettingsGenerator extends AbstractGenerator {
	
	var PATH_LIB_ENTITY = "solution/"
	var solution_name = ""
	
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		for (configuration : resource.allContents.toIterable.filter(Configuration)) {
			
			solution_name = configuration.software.name.toLowerCase
			
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/__init__.py", "")
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/media/__init__.py", "")
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/settings/__init__.py", "")
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/settings/base.py", createBaseSetting(resource))
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/settings/local.py", createLocalSetting())
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/settings/production.py", createProctionSetting())
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/settings/test.py", createTestSetting())
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/staticfiles/__init__.py", "")
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/asgi.py", createASGI(configuration.software.name.toLowerCase))
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/urls.py", createURL(configuration.software.name,configuration.about.name,resource))
			fsa.generateFile(PATH_LIB_ENTITY + configuration.software.name.toLowerCase +"/wsgi.py", createWSGI(configuration.software.name.toLowerCase))
			fsa.generateFile(PATH_LIB_ENTITY + "/manage.py", createManage(configuration.software.name.toLowerCase))
			fsa.generateFile(PATH_LIB_ENTITY + "/logs/.gitkeep", "")
			fsa.generateFile(PATH_LIB_ENTITY + ".env", configuration.software.name.createEnv())
			
			fsa.generateFile(PATH_LIB_ENTITY + "/static/__init__.py", "")			
			fsa.generateFile(PATH_LIB_ENTITY + "Procfile", createProcfile(configuration.software.name.toLowerCase))
			fsa.generateFile(PATH_LIB_ENTITY + "/requirements.txt", createRequirements)
				
		}	
		
	}
	
	def createEnv(String softwareName)'''
	ALLOWED_HOSTS=*
	
	DB_ENGINE_LOCAL=django.db.backends.sqlite3
	DB_HOST_LOCAL=
	DB_NAME_LOCAL=sqlite3.db
	DB_USER_LOCAL=
	DB_PASSWORD_LOCAL=
	DB_PORT_LOCAL=
	
	
	DB_ENGINE_TEST=django.db.backends.sqlite3
	DB_HOST_TEST=''
	DB_NAME_TEST='sqlite3.db'
	DB_PASSWORD_TEST=''
	DB_PORT_TEST=''
	DB_USER_TEST=''
	
	DB_ENGINE_PRODUCTION=django.db.backends.postgres
	DB_HOST_PRODUCTION=
	DB_NAME_PRODUCTION=
	DB_PASSWORD_PRODUCTION=
	DB_PORT_PRODUCTION=
	DB_USER_PRODUCTION= 
	
	DEBUG=True
	SECRET_KEY=3izb^ryglj(bvrjb2_y1fZvcnbky#358_l6-nn#i8fkug4mmz!
	
	DEFAULT_FROM_EMAIL=Crefisa xxxx <xxx@xxxx.com>
	EMAIL_HOST=xxxx
	EMAIL_HOST_PASSWORD=xxxx
	EMAIL_HOST_USER=xxxx
	EMAIL_PORT=587
	EMAIL_USE_TLS=True
	
	HASHIDS_SALT=hA8(scA@!fg3*sc&xaGh&6%-l<._&xCf
	
	DJANGO_SETTINGS_MODULE = «softwareName.toLowerCase».settings.local
	
	URL_VALIDATION=
	URL=
	
	'''
	def createProcfile(String softwareName)'''
	web: gunicorn «softwareName.toLowerCase».wsgi --log-file=- 
	'''
	
	def createRequirements()'''
	asgiref==3.4.1
	beautifulsoup4==4.10.0
	behave==1.2.6
	behave-django==1.4.0
	certifi==2021.10.8
	cffi==1.15.0
	charset-normalizer==2.0.7
	coreapi==2.3.3
	coreschema==0.0.4
	cryptography==35.0.0
	Deprecated==1.2.13
	dj-database-url==0.5.0
	Django==3.1.13
	django-easy-audit==1.3.0
	django-filter==21.1
	django-heroku==0.3.1
	django-oauth-toolkit==1.5.0
	djangorestframework==3.12.4
	drf-yasg==1.20.0
	idna==3.3
	inflection==0.5.1
	itypes==1.2.0
	Jinja2==3.0.3
	jwcrypto==1.0
	Markdown==3.3.4
	MarkupSafe==2.0.1
	oauthlib==3.1.1
	packaging==21.2
	parse==1.19.0
	parse-type==0.5.2
	psycopg2==2.9.2
	pycparser==2.21
	pyparsing==2.4.7
	python-decouple==3.5
	pytz==2021.3
	requests==2.26.0
	rest-condition==1.0.3
	ruamel.yaml==0.17.17
	ruamel.yaml.clib==0.2.6
	six==1.16.0
	soupsieve==2.3.1
	sqlparse==0.4.2
	Unipath==1.1
	uritemplate==4.1.1
	urllib3==1.26.7
	whitenoise==5.3.0
	wrapt==1.13.3
	django-cpf-cnpj
	asgiref==3.4.1
	autopep8==1.5.7
	dj-database-url==0.5.0
	gunicorn==20.1.0
	pycodestyle==2.7.0
	sqlparse==0.4.2
	toml==0.10.2
	Unipath==1.1
	
	'''
	
	
	
	def createManage(String softwareName)'''
	#!/usr/bin/env python
	"""Django's command-line utility for administrative tasks."""
	import os
	import sys
	
	
	def main():
	    os.environ.setdefault('DJANGO_SETTINGS_MODULE', '«softwareName.toLowerCase».settings.local')
	    try:
	        from django.core.management import execute_from_command_line
	    except ImportError as exc:
	        raise ImportError(
	            "Couldn't import Django. Are you sure it's installed and "
	            "available on your PYTHONPATH environment variable? Did you "
	            "forget to activate a virtual environment?"
	        ) from exc
	    execute_from_command_line(sys.argv)
	
	
	if __name__ == '__main__':
	    main()
	
	
	'''
	
	def createTestSetting()'''
	from .base import *
	from decouple import config
	import dj_database_url
		
	
	DEBUG = True
	
	SECRET_KEY = config('SECRET_KEY')
	
	
	# Database
	# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
	
	DATABASES = {
			'default': {
					'ENGINE': config('DB_ENGINE_TEST'),
					'NAME': config('DB_NAME_TEST'),
					'USER': config('DB_USER_TEST'),
					'PASSWORD': config('DB_PASSWORD_TEST'),
					'HOST': config('DB_HOST_TEST'),
					'PORT': config('DB_PORT_TEST'),
			}
	}
	
	'''
	
	def createProctionSetting()'''
	from .base import *
	from decouple import config
	import dj_database_url
	
	DEBUG = False
	
	SECRET_KEY = config('SECRET_KEY')
	
	
	DATABASES = {
		    'default': {
	        	'ENGINE': config('DB_ENGINE_PRODUCTION'),
	        	'NAME': config('DB_NAME_PRODUCTION'),
	        	'USER': config('DB_USER_PRODUCTION'),
	        	'PASSWORD': config('DB_PASSWORD_PRODUCTION'),
	        	'HOST': config('DB_HOST_PRODUCTION'),
	        	'PORT': config('DB_PORT_PRODUCTION'),
	    }
	}
	
	
	'''
	def createLocalSetting()'''
	from .base import *
	from decouple import config
	import dj_database_url
	
	DEBUG = config('DEBUG', default=True, cast=bool)
	
	SECRET_KEY = config('SECRET_KEY')
	
	# Database
	# https://	docs.djangoproject.com/en/3.0/ref/settings/#databases
	
	DATABASES = {
			'default': {
					'ENGINE': config('DB_ENGINE_LOCAL'),
					'NAME': config('DB_NAME_LOCAL'),
					'USER': config('DB_USER_LOCAL'),
					'PASSWORD': config('DB_PASSWORD_LOCAL'),
					'HOST': config('DB_HOST_LOCAL'),
					'PORT': config('DB_PORT_LOCAL'),
			},
	}

	
	
	'''
	
	def createBaseSetting( Resource resource)'''
	"""
	Django settings for gs project.
	
	Generated by 'django-admin startproject' using Django 3.0.7.
	
	For more information on this file, see
	https://docs.djangoproject.com/en/3.0/topics/settings/
	
	For the full list of settings and their values, see
	https://docs.djangoproject.com/en/3.0/ref/settings/
	"""
	
	import os
	import django_heroku
	
	
	from decouple import config
	from unipath import Path
	
	from django.contrib.messages import constants as messages
	
	import mimetypes
	
	
	# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	
	# Quick-start development settings - unsuitable for production
	# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/
	
	# SECURITY WARNING: don't run with debug turned on in production!
	DEBUG = True
	
	# load production server from .env
	ALLOWED_HOSTS = ['localhost', '127.0.0.1']

	
	# Application definition
	
	INSTALLED_APPS = [
	    'django.contrib.admin',
	    'django.contrib.auth',
	    'django.contrib.contenttypes',
	    'django.contrib.sessions',
	    'django.contrib.messages',
	    'django.contrib.staticfiles',
	    'django.contrib.admindocs',
	    'easyaudit',
	    'django_cpf_cnpj',
	    'rest_framework', 
	    'rest_framework.authtoken',
	    'oauth2_provider',
	    'drf_yasg',	    
	    'behave_django',
	    «FOR module: resource.allContents.toIterable.filter(Module)»
	     'apps.«module.name.toLowerCase»',
	    «ENDFOR»	    
	]
	
	MIDDLEWARE = [
	    'django.middleware.security.SecurityMiddleware',
	    'whitenoise.middleware.WhiteNoiseMiddleware',
	    'django.contrib.sessions.middleware.SessionMiddleware',
	    'django.middleware.common.CommonMiddleware',
	    'django.middleware.csrf.CsrfViewMiddleware',
	    'django.contrib.auth.middleware.AuthenticationMiddleware',
	    'django.contrib.messages.middleware.MessageMiddleware',
	    'django.middleware.clickjacking.XFrameOptionsMiddleware',
	    'easyaudit.middleware.easyaudit.EasyAuditMiddleware',	    
	]
	
	ROOT_URLCONF = '«solution_name».urls'
	
	CRISPY_TEMPLATE_PACK = 'bootstrap4'
	
	TEMPLATES = [
	    {
	        'BACKEND': 'django.template.backends.django.DjangoTemplates',
	        'DIRS': [os.path.join('templates')],
	        'APP_DIRS': True,
	        'OPTIONS': {
	            'context_processors': [
	                'django.template.context_processors.debug',
	                'django.template.context_processors.request',
	                'django.contrib.auth.context_processors.auth',
	                'django.contrib.messages.context_processors.messages',
	            ],
	        },
	    },
	]
	
	# Database
	# https://docs.djangoproject.com/en/3.2/ref/settings/#databases
	
	WSGI_APPLICATION = '«solution_name».wsgi.application'
	
	
	# Password validation
	# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators
	
	AUTH_PASSWORD_VALIDATORS = [
	    {
	        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	    },
	    {
	        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
	    },
	    {
	        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	    },
	    {
	        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	    },
	]
	
	
	LANGUAGE_CODE = 'pt-br'
	
	TIME_ZONE = 'America/Sao_Paulo'
	
	USE_I18N = True
	
	USE_L10N = True
	
	USE_TZ = True
	
	STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
	
	STATIC_URL = '/static/'
	
	STATICFILES_DIRS = (os.path.join('static'), )
	
	DATE_INPUT_FORMATS = ['%d/%m/%Y']
	
	# Heroku settings
	django_heroku.settings(locals())
	
	LOGIN_URL = "/accounts/login/"
	
	LOGIN_REDIRECT_URL = '/'
	
	LOGOUT_REDIRECT_URL = '/accounts/login'
	
	STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
		
	MESSAGE_LEVEL = 10
		
	MESSAGE_TAGS = {
	    messages.DEBUG: 'alert-info',
	    messages.INFO: 'alert-info',
	    messages.SUCCESS: 'alert-success',
	    messages.WARNING: 'alert-warning',
	    messages.ERROR: 'alert-danger',
	}
	
	ERRORS_MESSAGES = {
	    "unique":"Erro! Chave unica não pode se repetir"
	}
		
	MEDIA_URL = '/media/'
		
	MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
	
	REST_FRAMEWORK = {
	    'DATETIME_FORMAT': '%d/%m/%Y',
	    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
	    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend'],
	    'PAGE_SIZE': 10,
	    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',    
	    'DEFAULT_AUTHENTICATION_CLASSES': ('oauth2_provider.contrib.rest_framework.OAuth2Authentication',),
	    'DEFAULT_PARSER_CLASSES': [
	        'rest_framework.parsers.JSONParser',
	    ]
	}
	
	
	OAUTH2_PROVIDER = {	
	    'SCOPES': {'read': 'Read scope', 'write': 'Write scope', 'groups': 'Access to your groups'},
	    'ACCESS_TOKEN_EXPIRE_SECONDS': 36000,
	}
	
	EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
	
	if DEBUG == True:
	    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend' 
	
	EMAIL_HOST = config('EMAIL_HOST')
	EMAIL_PORT = config('EMAIL_PORT')
	EMAIL_HOST_USER = config('EMAIL_HOST_USER')
	EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')
	EMAIL_USE_TLS = config('EMAIL_USE_TLS')
	DEFAULT_FROM_EMAIL = config('DEFAULT_FROM_EMAIL')
	
	URL_VALIDATION_SUCCESS = 'password_reset'
	URL_VALIDATION_ERRO = 'validation_erro'
	
	URL = config('URL')
	URL_VALIDATION = config('URL_VALIDATION')
	
	HASHIDS_SALT = config('HASHIDS_SALT')
	
	LOGGING = {
	    "version": 1,
	    "disable_existing_loggers": False,
	    "root": {"level": "INFO", "handlers": ["file"]},
	    "handlers": {
	        "file": {
	            "level": "INFO",
	            "class": "logging.FileHandler",
	            "filename": "./logs/django.log",
	            "formatter": "app",
	        },
	    },
	    "loggers": {
	        "django": {
	            "handlers": ["file"],
	            "level": "INFO",
	            "propagate": True
	        },
	    },
	    "formatters": {
	        "app": {
	            "format": (
	                u"%(asctime)s [%(levelname)-8s] "
	                "(%(module)s.%(funcName)s) %(message)s"
	            ),
	            "datefmt": "%Y-%m-%d %H:%M:%S",
	        },
	    },
	}
	
	'''
	
	def createURL(String softwareName, String description,Resource resource) '''
	from django.contrib import admin
	from django.urls import path, include
	
	from django.conf import settings
	from django.conf.urls.static import static
	from rest_framework import permissions
	from drf_yasg.views import get_schema_view
	from drf_yasg import openapi 
	
	schema_view = get_schema_view(
	   openapi.Info(
	      title="«softwareName.toUpperCase»",
	      default_version='v1',
	      description="«description»",
	   ),
	   public=True,
	   permission_classes=[permissions.AllowAny],
	)
	
	
	urlpatterns = [
	    path('admin/doc/', include('django.contrib.admindocs.urls')),
	    path('admin/', admin.site.urls),
	    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')), 
	    path('api/v1/doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
	    path('accounts/', include('django.contrib.auth.urls')),
	«FOR module: resource.allContents.toIterable.filter(Module)»
		 path('', include('apps.«module.name.toLowerCase».api_urls')),
	«ENDFOR»
	] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	
	if settings.DEBUG:
	    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
	'''
	
	
	def createWSGI(String softwareName)'''
	"""
	WSGI config for gs project.
	
	It exposes the WSGI callable as a module-level variable named ``application``.
	
	For more information on this file, see
	https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
	"""
	
	import os
	
	from django.core.wsgi import get_wsgi_application
	
	os.environ.setdefault('DJANGO_SETTINGS_MODULE', '«softwareName.toLowerCase».settings.local')
	
	application = get_wsgi_application()
	'''
	
	def createASGI(String softwareName)'''
	"""
	ASGI config for gs project.
	
	It exposes the ASGI callable as a module-level variable named ``application``.
	
	For more information on this file, see
	https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
	"""
	
	import os
	
	from django.core.asgi import get_asgi_application
	
	os.environ.setdefault('DJANGO_SETTINGS_MODULE', '.«softwareName.toLowerCase».settings.base')
	
	application = get_asgi_application()
	
	'''
	
}
package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.templates

import org.eclipse.emf.ecore.resource.Resource
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity

class TemplateComponent {
	
	
	def createBase()'''
	
	<html>
	{% load static %}
	
	<link href="/static/styles/base.css" rel="stylesheet" />
	<style>
	 body {
	    overflow-y: auto;
	}
	</style>
	
	<body>	
	<div class="wrapper">
	  {% block menu_lateral %} {% endblock menu_lateral %}  
	  
	  <div class="main">
	    
	    {% block menu_topo %} {% endblock menu_topo %}  
	    
	    <main class="content">
	      {% block content %} {% endblock content %}        
	    </main>
	   
	  </div>
	</div>
	
	
	{% block scripts %}
	  <script src="/static/assets/vuejs/3.2.22/vuejs.min.js"></script>
	  <script src="/static/scripts/base.js"></script>
	{% endblock %}
	</body>
	
	</html>
	
	'''
	
	def create_menu_lateral (Resource resource)'''
	<nav id="sidebar" class="sidebar js-sidebar">
	  <div class="sidebar-content js-simplebar">
	    <a class="sidebar-brand" href="{% url 'home' %}">
	      <span class="align-middle">
	        <i
	          class="align-middle"
	          data-feather="codesandbox"
	          style="position: relative; top: -3px; left: -5px"
	        ></i>
	        Factor Admin
	      </span>
	    </a>
	    «FOR entity : resource.allContents.toIterable.filter(Entity)»
	    	«entity.create_entity_menu»
	    «ENDFOR»
	    
	   </ul>
	  </div>
	</nav>
	'''
	
	
	def create_entity_menu(Entity e )'''
	<li class="sidebar-item">
		   <a class="sidebar-link" href="{% url '«e.name.toLowerCase»-web-list' %}">
		   <i class="align-middle" data-feather="file-text"></i>
		   <span class="align-middle">«e.name.toFirstUpper»</span>
		  </a>
	</li>
		
	
	'''

	
	
	def create_menu_top ()'''
	<nav class="navbar navbar-expand navbar-light navbar-bg">
	  <a class="sidebar-toggle js-sidebar-toggle">
	    <i class="hamburger align-self-center"></i>
	  </a>
	
	  <div class="navbar-collapse collapse">
	    <ul class="navbar-nav navbar-align">
	
	      <li class="nav-item dropdown">
	        <a
	          class="nav-icon dropdown-toggle d-inline-block d-sm-none"
	          href="#"
	          data-bs-toggle="dropdown"
	        >
	          <i class="align-middle" data-feather="settings"></i>
	        </a>
	
	        <a
	          class="nav-link dropdown-toggle d-none d-sm-inline-block"
	          href="#"
	          data-bs-toggle="dropdown"
	        >
	          <i class="align-middle me-1" data-feather="user"></i>
	          <!--<img src="img/avatars/avatar.jpg" class="avatar img-fluid rounded me-1" alt="Charles Hall" /> <span class="text-dark">Charles Hall</span>-->
	        </a>
	        <div class="dropdown-menu dropdown-menu-end">
	          <a class="dropdown-item" href="pages-profile.html"
	            ><i class="align-middle me-1" data-feather="user"></i>
	            Profile</a
	          >          
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="#">Log out</a>
	        </div>
	      </li>
	    </ul>
	  </div>
	</nav>	
	
	'''
}
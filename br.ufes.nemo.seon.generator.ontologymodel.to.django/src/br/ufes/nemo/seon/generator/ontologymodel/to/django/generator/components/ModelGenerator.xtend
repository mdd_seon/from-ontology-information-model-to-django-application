package br.ufes.nemo.seon.generator.ontologymodel.to.django.generator.components

import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Attribute
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Entity
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumEntityAtribute
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.EnumX
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.ManyToMany
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Module
import br.ufes.nemo.seon.generator.ontologymodel.to.django.oM2D.Relation
import com.google.inject.Inject
import java.util.HashSet
import java.util.Set
import org.eclipse.xtext.naming.IQualifiedNameProvider

class ModelGenerator {

	@Inject extension IQualifiedNameProvider

	def create(Module module) '''
		# -*- coding: utf-8 -*-
		from django.db import models	
		from django.utils.translation import gettext_lazy as _	
		«module.createImport»		
		«FOR e : module.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«entity.createModel»
			«ENDIF»
			
			«IF e.eClass.instanceClass == EnumX»
				«val EnumX enumX = e as EnumX»
				«enumX.createEnum»
			«ENDIF»
		«ENDFOR»
	'''

	def createImport(Module module) {
		val Set<String> imports = new HashSet<String>;
		for (e : module.elements){
			if (e.eClass.instanceClass == Entity){
				val Entity entity = e as Entity
				for (relation : entity.relations){
					if (relation.type.eContainer.fullyQualifiedName.toString.toLowerCase != module.name.toString.toLowerCase){
						imports.add ('''from apps.«relation.type.eContainer.fullyQualifiedName.toLowerCase».models import «relation.type.name»''')
					}
											
				}
				for (attribute: entity.attributes){
						if (attribute.type.toLowerCase == 'cpf'){
							imports.add ('''from django_cpf_cnpj.fields import CPFField''')
						}
						if (attribute.type.toLowerCase == 'cnpj'){
							imports.add ('''from django_cpf_cnpj.fields import CNPJField''')
						}
						if (attribute.type.toLowerCase == 'user'){
							imports.add ('''from django.contrib.auth.models import User''')
						}					
					
				}				 				
			}
		}
		'''
		«FOR element : imports»
		«element»
		«ENDFOR»
		'''
	}
	
	

	def createEnum(EnumX enumX) '''
		class «enumX.name»(models.TextChoices):
			"""«IF enumX.description !== null»«enumX.description.textfield»«ENDIF»"""
			«FOR attribute : enumX.attributes»
			«attribute.name» = '«attribute.name»', _('«attribute.fullName»')
			«ENDFOR»
			
	'''

	def createModel(Entity entity) '''
		
		class «entity.name» («IF entity.superType !== null»«entity.superType.name»«ELSE»models.Model«ENDIF»): 
			"""«IF entity.description !== null»«entity.description.textfield»«ENDIF»"""
			
			«FOR attribute : entity.attributes»				
				«attribute.create»				
			«ENDFOR»
			
			«FOR enumAttribute : entity.enumentityatributes»				
				«enumAttribute.create»				
			«ENDFOR»
			
			«FOR relation : entity.relations»
				«relation.create»
			«ENDFOR»
			
			class Meta:
				db_table = '«entity.name.toLowerCase»'
		
	'''

	def create(EnumEntityAtribute enumEntityAtribute){
		var String valor = ""
		var int tamanho = 0
		for (attribute: enumEntityAtribute.type.attributes){
			if (valor == "")
				valor = attribute.name;
			if (attribute.name.length > tamanho){
				tamanho = attribute.name.length
			}
		}
		'''
	 	«enumEntityAtribute.name» = models.CharField(max_length=«tamanho»,choices=«enumEntityAtribute.type.name».choices, default=«enumEntityAtribute.type.name».«valor»)
		'''
	} 
	
	
	def create(Attribute attribute) '''
		
		«IF attribute.type.toLowerCase == 'string'»
			«attribute.name» = models.CharField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» max_length=300,null=True, blank=True)
		«ELSEIF attribute.type.toLowerCase == 'decimal'»
			«attribute.name» = models.«attribute.type.toFirstUpper»Field(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» max_digits=14, decimal_places=2,null=True, blank=True)
		«ELSEIF attribute.type.toLowerCase == 'cnpj'»
			«attribute.name» = CNPJField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» null=True, blank=True)
		«ELSEIF attribute.type.toLowerCase == 'cpf'»
			«attribute.name» = CPFField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» null=True, blank=True)
		«ELSEIF attribute.type.toLowerCase == 'telefone'»
			«attribute.name» = models.CharField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» max_length=27,null=True, blank=True)
		«ELSEIF attribute.type.toLowerCase == 'celular'»
			«attribute.name» = models.CharField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» max_length=11,null=True, blank=True)
		«ELSEIF attribute.type.toLowerCase == 'user'»
			«attribute.name» = models.ForeignKey(User,blank=True,null=True, on_delete=models.CASCADE)
		«ELSEIF attribute.type.toLowerCase == 'datetime'»
			«attribute.name» = models.DateTimeField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF»blank=True)
		«ELSEIF attribute.type.toLowerCase == 'url'»
					«attribute.name» = models.URLField(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF»blank=True)
		«ELSE»
			«attribute.name» = models.«attribute.type.toFirstUpper»Field(«IF attribute.fullName !== null»"«attribute.fullName»",«ENDIF» null=True, blank=True)
		«ENDIF»
	'''

	def create(Relation relation) '''
		«IF relation.eClass.instanceClass == ManyToMany»
		«val ManyToMany m2m = relation as ManyToMany»
		«relation.name» = models.ManyToManyField(«relation.type.name» «IF m2m.by !== null»,through='«m2m.by.name»'«ENDIF»)
		«ELSE»
		«relation.name» = models.ForeignKey(«relation.type.name»,blank=True,null=True, on_delete=models.CASCADE, related_name=«relation.name»_«relation.type.name»)
		«ENDIF»	
		
	'''

}

/*
 * generated by Xtext 2.27.0
 */
package br.ufes.nemo.seon.generator.ontologymodel.to.django.ui;

import br.ufes.nemo.seon.generator.ontologymodel.to.django.ui.internal.DjangoActivator;
import com.google.inject.Injector;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class OM2DExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return FrameworkUtil.getBundle(DjangoActivator.class);
	}
	
	@Override
	protected Injector getInjector() {
		DjangoActivator activator = DjangoActivator.getInstance();
		return activator != null ? activator.getInjector(DjangoActivator.BR_UFES_NEMO_SEON_GENERATOR_ONTOLOGYMODEL_TO_DJANGO_OM2D) : null;
	}

}

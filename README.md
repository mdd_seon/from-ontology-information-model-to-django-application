# From Ontology Information Model to Django Application

This plugin is responsible for creating a Django application from an Ontology Information Model

## Install

1. Install Ecplise IDE for Java and DSL Developers
	
	1.1 Access this link https://www.eclipse.org/downloads/packages/release/2022-03/r/eclipse-ide-java-and-dsl-developers download this version
	
	1.2 Or for the latest version, go to https://www.eclipse.org/downloads/packages/ click on the option "Eclipse IDE for Java and DSL Developers"
	
	1.2.1 Download the version presented on this page

2. Copy all .jar files from the **lib** folder of this repository.
3. Go to the folder where you downloaded the eclipse file and look for the plugins folder (~folder_where_you_downloaded_eclipses/eclipse/plugins) and paste the content copied in the previous step

## Usage
1. Create a Java Project
2. Create a file with .om2d extension

	2.1 Right click on the src folder

	2.2 Click New > File

	2.3 Write a name for the file (example: teste.om2d)

3. A window will pop up with the message "Do you want to convert 'your_java_project' to an Xtext project?" click Yes.

	3.1 A folder named src-gen will be created.

	3.2 If these windows do not appear, there was an error in the installation, perform the installation steps again.

4. Write the Information Model in the created .om2d file

	4.1 You can copy the code below as an example

5. Save the .om2d file you just edited

    5.1 In the src-gen folder python code will be generated based on the Information Model written in the .om2d file

## Example of a .om2d

[Link of Example](xxxxxxx)

```bash

Configuration {
	about: "Rent a Car"
	software: "rentcar"
	author: "Paulo Sérgio dos Santos Júnior"
	author_email:"paulossjunior@gmail.com"
	repository: "Your path on GitHub or GitLab"	
}

module Rent {
	
	entity Car {
		
		name: string, fullName: "Car Name"
		
	}
	
	entity Person {
		
		usuario: user, fullName: "A Person"
		identification: cpf

		car OneToOne Car
	}
	
	entity Organization {
		usuario: user, fullName: "An Organization"
		identification: cnpj
				
		employees OneToMany Person
	}

	
}

```
## Using the example
```python

python manage.py runserver
```


## Grammar